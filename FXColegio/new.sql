CREATE DATABASE FXColegio
GO
USE FXColegio
GO

CREATE TABLE Profesor(
	idProfesor INT IDENTITY(1,1) NOT NULL,
	dpi VARCHAR(20) NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	telefono VARCHAR(50) NOT NULL,
	correo VARCHAR(50) NOT NULL,
	direccion VARCHAR(255) NOT NULL,
	PRIMARY KEY(idProfesor)
);

CREATE TABLE Bimestre(
	idBimestre INT IDENTITY(1,1) NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	PRIMARY KEY(idBimestre)
);

CREATE TABLE Salon(
	idSalon INT IDENTITY(1,1) NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	PRIMARY KEY(idSalon)
);

CREATE TABLE Materia(
	idMateria INT IDENTITY(1,1) NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	PRIMARY KEY(idMateria)

);

CREATE TABLE Seccion(
	idSeccion INT IDENTITY(1,1) NOT NULL,
	grado VARCHAR(50) NOT NULL,
	nombre VARCHAR(50) NOT NULL,
	jornada VARCHAR(20) NOT NULL,
	codigo VARCHAR(50) NOT NULL,
	PRIMARY KEY(idSeccion)
	
);

CREATE TABLE Alumno(
	idAlumno INT IDENTITY(1,1) NOT NULL,
	idSeccion INT NOT NUll,
	nombre VARCHAR(255) NOT NULL,
	carnet VARCHAR(50) NOT NULL,
	direccion VARCHAR(255) NOT NULL,
	telefono VARCHAR(50) NOT NULL,
	FOREIGN KEY (idSeccion) REFERENCES Seccion(idSeccion) ON DELETE CASCADE,
	PRIMARY KEY(idAlumno)
);

CREATE TABLE Usuario(
	idUsuario INT IDENTITY(1,1) NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	contrasenia VARCHAR(255) NOT NULL,
	PRIMARY KEY(idUsuario)
);

CREATE TABLE Asignacion(
	idAsignacion INT IDENTITY(1,1) NOT NULL,
	idSeccion INT NOT NULL,
	idMateria INT NOT NULL,
	idProfesor INT NOT NULL,
	idSalon INT NOT NULL,
	FOREIGN KEY (idProfesor) REFERENCES Profesor(idProfesor) ON DELETE CASCADE,
	FOREIGN KEY (idMateria) REFERENCES Materia(idMateria) ON DELETE CASCADE,
	FOREIGN KEY (idSeccion) REFERENCES Seccion(idSeccion) ON DELETE CASCADE,
	FOREIGN KEY (idSalon) REFERENCES Salon(idSalon) ON DELETE CASCADE,
	PRIMARY KEY(idAsignacion)
);

CREATE TABLE Actividad(
	idActividad INT IDENTITY(1,1) NOT NULL,
	idAsignacion INT NOT NULL,
	idBimestre INT NOT NULL,
	descripcion VARCHAR(255) NOT NULL,
	valorNeto INT NOT NULL,
	FOREIGN KEY (idBimestre) REFERENCES Bimestre(idBimestre) ON DELETE CASCADE,
	FOREIGN KEY (idAsignacion) REFERENCES Asignacion(idAsignacion) ON DELETE CASCADE,
 	PRIMARY KEY(idActividad)
);

CREATE TABLE ActividadAlumno(
	idActividadAlumno INT IDENTITY(1,1) NOT NULL,
	idActividad INT NOT NULL,
	idAlumno INT NOT NULL,
	nota INT NOT NULL DEFAULT 0,
	valorNeto INT NOT NULL,
	FOREIGN KEY (idActividad) REFERENCES Actividad(idActividad) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (idAlumno) REFERENCES Alumno(idAlumno) ON DELETE NO ACTION,
	PRIMARY KEY(idActividadAlumno)
);
	
CREATE TABLE Notas(
	idNotas INT IDENTITY(1,1) NOT NULL,
	idAlumno INT NOT NULL,
	idMateria INT NOT NULL,
	bimestre1 Int NOT NULL DEFAULT 0,
	bimestre2 Int NOT NULL DEFAULT 0,
	bimestre3 Int NOT NULL DEFAULT 0,
	bimestre4 Int NOT NULL DEFAULT 0,
	FOREIGN KEY (idAlumno) REFERENCES Alumno(idAlumno) ON DELETE CASCADE,
	FOREIGN KEY (idMateria) REFERENCES Materia(idMateria) ON DELETE CASCADE,
	PRIMARY KEY(idNotas)
);
GO

------------- MATERIA -----------------

--Insertar
CREATE PROCEDURE SP_InsertarMateria
	@nombre VARCHAR(50)
		AS
		BEGIN
			INSERT INTO Materia(nombre)
			VALUES(@nombre)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarMateria
	@idMateria INT, @nombre VARCHAR(50)
	AS
	BEGIN
		UPDATE Materia SET nombre = @nombre
		WHERE idMateria = @idMateria
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarMateria
	@idMateria INT
	AS
	BEGIN 
		DELETE FROM Materia WHERE idMateria = @idMateria;
	END
GO

------------- Bimestre -----------------

--Insertar
CREATE PROCEDURE SP_InsertarBimestre
	@nombre VARCHAR(50)
		AS
		BEGIN
			INSERT INTO Bimestre(nombre)
			VALUES(@nombre)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarBimestre
	@idBimestre INT, @nombre VARCHAR(50)
	AS
	BEGIN
		UPDATE Bimestre SET nombre = @nombre
		WHERE idBimestre = @idBimestre
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarBimestre
	@idBimestre INT
	AS
	BEGIN 
		DELETE FROM Bimestre WHERE idBimestre = @idBimestre;
	END
GO

------------- Usuario -----------------

--Insertar
CREATE PROCEDURE SP_InsertarUsuario
	@nombre VARCHAR(255), @contrasenia VARCHAR(255)
		AS
		BEGIN
			INSERT INTO Usuario(nombre, contrasenia)
			VALUES(@nombre, @contrasenia)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarUsuario
	@idUsuario INT, @nombre VARCHAR(255), @contrasenia VARCHAR(255)
	AS
	BEGIN
		UPDATE Usuario SET nombre = @nombre, contrasenia = @contrasenia
		WHERE idUsuario = @idUsuario
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarUsuario
	@idUsuario INT
	AS
	BEGIN 
		DELETE FROM Usuario WHERE idUsuario = @idUsuario;
	END
GO

------------- Salon -----------------

--Insertar
CREATE PROCEDURE SP_InsertarSalon
	@nombre VARCHAR(50)
		AS
		BEGIN
			INSERT INTO Salon(nombre)
			VALUES(@nombre)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarSalon
	@idSalon INT, @nombre VARCHAR(50)
	AS
	BEGIN
		UPDATE Salon SET nombre = @nombre
		WHERE idSalon = @idSalon
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarSalon
	@idSalon INT
	AS
	BEGIN 
		DELETE FROM Salon WHERE idSalon = @idSalon;
	END
GO

------------- Grado -----------------

--Insertar
CREATE PROCEDURE SP_InsertarGrado
	@grado VARCHAR(50) , @nombre VARCHAR(50), 
	@jornada VARCHAR(20), @codigo VARCHAR(50)
		AS
		BEGIN
			INSERT INTO Seccion(grado, nombre, jornada, codigo)
			VALUES(@grado, @nombre, @jornada, @codigo)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarGrado
	@idSeccion INT, @grado VARCHAR(50),
	@nombre VARCHAR(50), @jornada VARCHAR(20), @codigo VARCHAR(50)
	AS
	BEGIN
		UPDATE Seccion SET grado = @grado,
		nombre = @nombre, jornada = @jornada, codigo = @codigo
		WHERE idSeccion = @idSeccion
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarGrado
	@idSeccion INT
	AS
	BEGIN 
		DELETE FROM Seccion WHERE idSeccion = @idSeccion;
	END
GO

------------- Profesor -----------------

--Insertar
CREATE PROCEDURE SP_InsertarProfesor
	@dpi VARCHAR(20), @nombre VARCHAR(255), 
	@direccion VARCHAR(255), @telefono VARCHAR(50),
	@correo VARCHAR(50)
	
		AS
		BEGIN
			INSERT INTO Profesor(dpi, nombre, direccion, telefono, correo)
			VALUES(@dpi, @nombre, @direccion, @telefono, @correo)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarProfesor
	@idProfesor INT, @dpi VARCHAR(20),
	@nombre VARCHAR(255), @direccion VARCHAR(255),
	@telefono VARCHAR(50), @correo VARCHAR(50)
	AS
	BEGIN
		UPDATE Profesor SET dpi = @dpi, nombre = @nombre, 
		direccion = @direccion, telefono = @telefono,
		correo = @correo
		WHERE idProfesor = @idProfesor
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarProfesor
	@idProfesor INT
	AS
	BEGIN 
		DELETE FROM Profesor WHERE idProfesor = @idProfesor;
	END
GO

------------- Alumno -----------------

--Insertar
CREATE PROCEDURE SP_InsertarAlumno
	@idGrado INT, @nombre VARCHAR(255), @carnet VARCHAR(50),
	@direccion VARCHAR(255), @telefono VARCHAR(50)
	
		AS
		BEGIN
			INSERT INTO Alumno(idSeccion, nombre, carnet, direccion, telefono)
			VALUES(@idGrado, @nombre, @carnet, @direccion, @telefono)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarAlumno
	@idAlumno INT, @idGrado INT, @nombre VARCHAR(255), @carnet VARCHAR(50),
	@direccion VARCHAR(255), @telefono VARCHAR(50)
	AS
	BEGIN
		UPDATE Alumno SET idSeccion = @idGrado, nombre = @nombre,
		carnet = @carnet, direccion = @direccion, telefono = @telefono
		WHERE idAlumno = @idAlumno
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarAlumno
	@idAlumno INT
	AS
	BEGIN 
		DELETE FROM Alumno WHERE idAlumno = @idAlumno;
	END
GO

------------- Asignacion -----------------

--Insertar
CREATE PROCEDURE SP_InsertarAsignacion
	@idSeccion INT, @idMateria INT, @idProfesor INT,
	@idSalon INT
		AS
		BEGIN
			INSERT INTO Asignacion(idSeccion, idMateria, idProfesor, idSalon)
			VALUES(@idSeccion, @idMateria, @idProfesor, @idSalon)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarAsignacion
	@idAsignacion INT, @idSeccion INT, @idMateria INT, @idProfesor INT,
	@idSalon INT
	AS
	BEGIN
		UPDATE Asignacion SET idSeccion = @idSeccion, idMateria = @idMateria,
		 idProfesor = @idProfesor, idSalon = @idSalon
		WHERE idAsignacion = @idAsignacion
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarAsignacion
	@idAsignacion INT
	AS
	BEGIN 
		DELETE FROM Asignacion WHERE idAsignacion = @idAsignacion;
	END
GO

------------- Notas -----------------

CREATE PROCEDURE SP_InsertarNotas
	@idAlumno INT, @idMateria INT
	
		AS
		BEGIN
			INSERT INTO Notas(idAlumno, idMateria)
			VALUES(@idAlumno, @idMateria)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarNotas
	@idNotas INT, @idAlumno INT, @idMateria INT, @bimestreI INT, @bimestreII INT,
	@bimestreIII INT, @bimestreIV INT
	AS
	BEGIN
		UPDATE Notas SET idAlumno = @idAlumno, idMateria = @idMateria, bimestre1 = @bimestreI,
		bimestre2 = @bimestreII, bimestre3 = @bimestreIII, bimestre4 = @bimestreIV
		WHERE idNotas = @idNotas
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarNotas
	@idNotas INT
	AS
	BEGIN 
		DELETE FROM Notas WHERE idNotas = @idNotas;
	END
GO

------------- Actividad -----------------

--Insertar
CREATE PROCEDURE SP_InsertarActividad
	@idAsignacion INT, @idBimestre INT, @nombre VARCHAR(255), @valorNeto INT
		AS
		BEGIN
			INSERT INTO Actividad(idAsignacion, idBimestre, descripcion, valorNeto)
			VALUES(@idAsignacion, @idBimestre, @nombre, @valorNeto)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarActividad
	@idActividad INT, @idBimestre INT, @idAsignacion INT, @nombre VARCHAR(255), @valorNeto INT
	AS
	BEGIN
		UPDATE Actividad SET idAsignacion = @idAsignacion, idBimestre = @idBimestre, descripcion = @nombre, valorNeto = @valorNeto
		WHERE idActividad = @idActividad
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarActividad
	@idActividad INT
	AS
	BEGIN 
		DELETE FROM Actividad WHERE idActividad = @idActividad;
	END
GO

------------- ActividadAlumno -----------------

--Insertar
CREATE PROCEDURE SP_InsertarActividadAlumno
	@idActividad INT, @idAlumno INT, @valorNeto INT, @nota INT
		AS
		BEGIN
			INSERT INTO ActividadAlumno(idActividad, idAlumno, valorNeto, nota)
			VALUES(@idActividad, @idAlumno, @valorNeto, @nota)
			
		END
GO
--Actualizar
CREATE PROCEDURE SP_ActualizarActividadAlumno
	@idActividadAlumno INT, @idActividad INT, @idAlumno INT, @valorNeto INT, @nota INT
	AS
	BEGIN
		UPDATE ActividadAlumno SET idActividad = @idActividad, idAlumno = @idAlumno,
		 valorNeto = @valorNeto, nota = @nota 
		WHERE idActividadAlumno = @idActividadAlumno
	END
GO
--Borrar
CREATE PROCEDURE SP_BorrarActividadAlumno
	@idActividad INT
	AS
	BEGIN 
		DELETE FROM ActividadAlumno WHERE idActividadAlumno = @idActividad;
	END
GO