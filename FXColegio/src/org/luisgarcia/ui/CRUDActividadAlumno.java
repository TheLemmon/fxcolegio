/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.luisgarcia.bean.ActividadAlumno;
import org.luisgarcia.bean.Actividad;
import org.luisgarcia.bean.Alumno;
import org.luisgarcia.bean.Bimestre;
import org.luisgarcia.bean.Materia;
import org.luisgarcia.bean.Notas;
import org.luisgarcia.bean.Profesor;
import org.luisgarcia.bean.Seccion;
import org.luisgarcia.controller.ControllerActividad;
import org.luisgarcia.controller.ControllerActividadAlumno;
import org.luisgarcia.controller.ControllerAlumno;
import org.luisgarcia.controller.ControllerAsignacion;
import org.luisgarcia.controller.ControllerBimestre;
import org.luisgarcia.controller.ControllerNotas;
import org.luisgarcia.controller.ControllerSeccion;
/**
 *
 * @author Fernando
 */
public class CRUDActividadAlumno {
    private static final CRUDActividadAlumno CRUD_ActividadAlumno = new CRUDActividadAlumno();
    private HBox hBox;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonCalificar;
    private ComboBox<Seccion> comboBoxSeccion;
    private ComboBox<Materia> comboBoxMateria;
    private ComboBox<Profesor> comboBoxProfesor;
    private ComboBox<Bimestre> comboBoxBimestre;
    private ComboBox<Alumno> comboBoxAlumno;
    private int valor = 1;
    private SpinnerValueFactory svf;
    private  Spinner sp;
    private Label Valor;
    private int valor2 = 1;
    private SpinnerValueFactory svf2;
    private  Spinner sp2;
    private Label Valor2;
    private int notaBimestre;
    
    private TableColumn<ActividadAlumno, Integer> tableColumnIdActividadAlumno;
    private TableColumn<ActividadAlumno, Actividad> tableColumnActividades;
    private TableColumn<ActividadAlumno, Alumno>  tableColumnAlumno;
    private TableColumn<ActividadAlumno, Integer> tableColumnNota;
    private TableColumn<ActividadAlumno, Integer> tableColumnValorNeto;
    private TableView<ActividadAlumno> tableView;
    private ObservableList<ActividadAlumno> observableList;
    private ObservableList<Seccion> observableListSeccion;
    private ObservableList<Materia> observableListMateria;
    private ObservableList<Profesor> observableListProfesor;
    private ObservableList<Alumno> observableListAlumno;
    private ObservableList<Bimestre> observableListBimestre;

    private CRUDActividadAlumno() {
        
    }

    public static CRUDActividadAlumno getCRUD_ActividadAlumno() {
        return CRUD_ActividadAlumno;
    }
    
public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        gridPane.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
                    tableView.getSelectionModel().clearSelection();
                    Valor.setVisible(false);
                    sp.setVisible(false);
                    Valor2.setVisible(false);
                    sp2.setVisible(false);
                    buttonCalificar.setVisible(false);
                }
            }
        });
        updateObservableListOne();
        Label labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 1);
        comboBoxSeccion = new ComboBox<>(observableListSeccion);
        comboBoxSeccion.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateObservableListOne();
                comboBoxMateria.getSelectionModel().clearSelection();
                comboBoxProfesor.getSelectionModel().clearSelection();
                try {
                updateObservableListMateria(comboBoxSeccion.getSelectionModel()
                        .getSelectedItem().getCodigo());
                }catch (NullPointerException ex){
                        
                }
            }
        });
        gridPane.add(comboBoxSeccion, 1, 1);
        
        comboBoxSeccion.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                updateOne();
            }
        });
        
        Label labelMateria = new Label("Materia:");
        gridPane.add(labelMateria, 0, 2);
        comboBoxMateria = new ComboBox<>(observableListMateria);
        comboBoxSeccion.setMinSize(140, 20);
        comboBoxMateria.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(comboBoxSeccion.getSelectionModel().getSelectedItem() != null &&
                   comboBoxMateria.getSelectionModel().getSelectedItem() != null){
                    updateObservableListProfesor(comboBoxMateria.getSelectionModel()
                        .getSelectedItem().getNombre(), comboBoxSeccion.getSelectionModel()
                                .getSelectedItem().getCodigo());
                }
                    
                
            }
        });
        comboBoxMateria.setMinSize(140, 20);
        gridPane.add(comboBoxMateria, 1, 2);
        
        
        Label labelBimestre = new Label("Bimestre:");
        gridPane.add(labelBimestre, 0, 5);
        comboBoxBimestre = new ComboBox<>(observableListBimestre);
        comboBoxBimestre.setMinSize(140, 20);
        gridPane.add(comboBoxBimestre, 1, 5);
        
        Label labelAlumno = new Label("Alumno:");
        gridPane.add(labelAlumno, 0, 4);
        comboBoxAlumno = new ComboBox<>(observableListAlumno);
        comboBoxAlumno.setMinSize(140, 20);
        gridPane.add(comboBoxAlumno, 1, 4);
        
       
        Label labelProfesor = new Label("Profesor:");
        gridPane.add(labelProfesor, 0, 3);
        comboBoxProfesor = new ComboBox<>(observableListProfesor);
        comboBoxProfesor.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
                    if(comboBoxSeccion.getSelectionModel().getSelectedItem() != null){
                        updateObservableListAlumno(comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo());
                    }
                
                }
            }
        });
        comboBoxProfesor.setMinSize(140, 20);
        gridPane.add(comboBoxProfesor, 1, 3);
       
        Button aceptar = new Button("Buscar");
        aceptar.setMinSize(120, 30);
        aceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateTableViewItemsbusqueda(ControllerActividad.getCONTROLLER_Actividad().searchOne(
                        comboBoxSeccion.getSelectionModel().getSelectedItem().getIdSeccion(),
                        comboBoxProfesor.getSelectionModel().getSelectedItem().getIdProfesor(),
                        comboBoxMateria.getSelectionModel().getSelectedItem().getIdMateria(), 
                        comboBoxBimestre.getSelectionModel().getSelectedItem().getIdBimestre()),
                        comboBoxAlumno.getSelectionModel().getSelectedItem().getIdAlumno());
            }
        });
        gridPane.add(aceptar, 1, 6, 1, 2);
        tableColumnIdActividadAlumno = new TableColumn<>("ID");
        tableColumnIdActividadAlumno.setCellValueFactory(new PropertyValueFactory<>("idActividadAlumno"));
        tableColumnIdActividadAlumno.setMinWidth(80);
        
        tableColumnActividades = new TableColumn<>("Actividad");
        tableColumnActividades.setCellValueFactory(new PropertyValueFactory<>("Actividad"));
        tableColumnActividades.setMinWidth(180);
        
        tableColumnAlumno = new TableColumn<>("Alumno");
        tableColumnAlumno.setCellValueFactory(new PropertyValueFactory<>("Alumno"));
        tableColumnAlumno.setMinWidth(160);
        
        tableColumnValorNeto = new TableColumn<>("Valor");
        tableColumnValorNeto.setCellValueFactory(new PropertyValueFactory<>("valorNeto"));
        tableColumnValorNeto.setMinWidth(80);
        
        tableColumnNota = new TableColumn<>("Nota");
        tableColumnNota.setCellValueFactory(new PropertyValueFactory<>("nota"));
        tableColumnNota.setMinWidth(80);
                
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdActividadAlumno,
                tableColumnActividades, tableColumnAlumno,tableColumnValorNeto, tableColumnNota);
        
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
                    if(tableView.getSelectionModel().getSelectedItem() != null ||
                        comboBoxMateria.getSelectionModel().getSelectedItem() == null ||
                        comboBoxProfesor.getSelectionModel().getSelectedItem() == null ||
                        comboBoxBimestre.getSelectionModel().getSelectedItem() == null){
                    Valor.setVisible(true);
                    sp.setVisible(true);
                    valor = tableView.getSelectionModel().getSelectedItem().getValorNeto();
                    svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(valor, 200);
                    sp.setValueFactory(svf);
                    Valor2.setVisible(true);
                    sp2.setVisible(true);
                    svf2 = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, valor);
                    sp2.getEditor().setText("0");
                    sp2.setValueFactory(svf2);
                    buttonCalificar.setVisible(true);                        
                    }

                }
            }
        });
        tableView.setMinSize(580, 400);  
        gridPane.add(tableView, 2, 1, 2, 9);
          
        Valor = new Label("Valor");
        Valor.setVisible(false);
        gridPane.add(Valor, 4, 1);
        
        sp = new Spinner();
        sp.setValueFactory(svf);
        sp.setMinSize(140, 20);
        sp.setPrefWidth(80);
        sp.setVisible(false);
        sp.setEditable(true);
        sp.setMouseTransparent(true);
        gridPane.add(sp, 5, 1);
        
        Valor2 = new Label("Nota Obtenida");
        Valor2.setVisible(false);
        gridPane.add(Valor2, 4, 2);
        
        sp2 = new Spinner();
        sp2.setValueFactory(svf2);
        sp2.setMinSize(140, 20);
        sp2.setPrefWidth(80);
        sp2.setVisible(false);
        sp2.setEditable(true);
        gridPane.add(sp2, 5, 2);
        
        buttonCalificar = new Button("Calificar");
        buttonCalificar.setMinSize(120, 30);
        
        buttonCalificar.setVisible(false);
        buttonCalificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                notaBimestre = 0;
                if(Integer.parseInt(sp2.getEditor().getText()) > valor || 
                        comboBoxSeccion.getSelectionModel().getSelectedItem() == null ||
                        comboBoxMateria.getSelectionModel().getSelectedItem() == null ||
                        comboBoxProfesor.getSelectionModel().getSelectedItem() == null ||
                        comboBoxBimestre.getSelectionModel().getSelectedItem() == null){
                    
                } else {
                    ControllerActividadAlumno.getCONTROLLER_ActividadAlumno().modify(
                            tableView.getSelectionModel().getSelectedItem().getIdActividadAlumno(),
                    ControllerActividad.getCONTROLLER_Actividad().getInt(
                        tableView.getSelectionModel().getSelectedItem().getActividad()
                                .getAsignacion().getSeccion().getIdSeccion(),
                        tableView.getSelectionModel().getSelectedItem().getActividad()
                                .getAsignacion().getMateria().getIdMateria(),
                        tableView.getSelectionModel().getSelectedItem().getActividad()
                                .getAsignacion().getProfesor().getIdProfesor()),
                        tableView.getSelectionModel().getSelectedItem().getAlumno().getIdAlumno(),
                        tableView.getSelectionModel().getSelectedItem().getValorNeto(),
                        Integer.parseInt(sp2.getEditor().getText()));
                try{
                    updateTableViewItemsbusqueda(ControllerActividad.getCONTROLLER_Actividad().searchOne(
                        comboBoxSeccion.getSelectionModel().getSelectedItem().getIdSeccion(),
                        comboBoxProfesor.getSelectionModel().getSelectedItem().getIdProfesor(),
                        comboBoxMateria.getSelectionModel().getSelectedItem().getIdMateria(), 
                        comboBoxBimestre.getSelectionModel().getSelectedItem().getIdBimestre()),
                        comboBoxAlumno.getSelectionModel().getSelectedItem().getIdAlumno());
                    for(ActividadAlumno actividadAlumno : observableList){
                        notaBimestre = notaBimestre + actividadAlumno.getNota();
                        System.out.println(notaBimestre);
                        
                    }
                    
                    for(Notas nota : ControllerNotas.getCONTROLLER_Notas().getArrayList()){
                        if(comboBoxAlumno.getSelectionModel().getSelectedItem().getIdAlumno()
                                == nota.getAlumno().getIdAlumno() &&
                            comboBoxMateria.getSelectionModel().getSelectedItem().getIdMateria()
                                == nota.getMateria().getIdMateria()){
                            if(comboBoxBimestre.getSelectionModel().getSelectedItem()
                                    .getIdBimestre() == 1){
                                ControllerNotas.getCONTROLLER_Notas().modify(
                                        nota.getControlNotas(), nota.getAlumno().getIdAlumno(),
                                    nota.getMateria().getIdMateria(), notaBimestre,
                                    nota.getBimestreII(), nota.getBimestreIII(), nota.getBimestreIV());
                            } else if(comboBoxBimestre.getSelectionModel().getSelectedItem()
                                    .getIdBimestre() == 2){
                                ControllerNotas.getCONTROLLER_Notas().modify(nota.getControlNotas(),
                                        nota.getAlumno().getIdAlumno(),
                                    nota.getMateria().getIdMateria(), nota.getBimestreI(), notaBimestre,
                                    nota.getBimestreIII(), nota.getBimestreIV());
                            } else if(comboBoxBimestre.getSelectionModel().getSelectedItem().getIdBimestre() == 3){
                                ControllerNotas.getCONTROLLER_Notas().modify(
                                        nota.getControlNotas(), nota.getAlumno().getIdAlumno(),
                                    nota.getMateria().getIdMateria(), nota.getBimestreI(),
                                    nota.getBimestreII(), notaBimestre, nota.getBimestreIV());
                            } else if(comboBoxBimestre.getSelectionModel().getSelectedItem()
                                    .getIdBimestre() == 4){
                                ControllerNotas.getCONTROLLER_Notas().modify(nota
                                        .getControlNotas(), nota.getAlumno().getIdAlumno(),
                                    nota.getMateria().getIdMateria(), nota.getBimestreI(),
                                    nota.getBimestreII(), nota.getBimestreIII(), notaBimestre);
                            }
                            
                        }
                    }
                } catch(NullPointerException ex){
                   // updateTableViewItems();
                }
                
                }
                    Valor.setVisible(false);
                    sp.setVisible(false);
                    Valor2.setVisible(false);
                    sp2.setVisible(false);
                    buttonCalificar.setVisible(false);                
                
            }
        });
        
        gridPane.add(buttonCalificar, 5, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        hBox.getChildren().add(gridPane);
        hBox.setAlignment(Pos.CENTER_LEFT);
        return hBox;
        
    }
    
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerActividadAlumno
                .getCONTROLLER_ActividadAlumno().getArrayList());
    }
    
    public void updateOne(){
        updateObservableListOne();
        comboBoxSeccion.setItems(observableListSeccion);
    }
    
    public void updateTableViewItemsbusqueda(ArrayList<Actividad> actividad, int alumno) {
        observableList = FXCollections.observableArrayList(ControllerActividadAlumno
                .getCONTROLLER_ActividadAlumno().buscar(actividad, alumno));
        tableView.setItems(observableList);
    }
    
    public void updateObservableListOne() {
        observableListSeccion = FXCollections.observableArrayList(ControllerSeccion
                .getCONTROLLER_Seccion().getArrayList());
      
        observableListBimestre = FXCollections.observableArrayList(ControllerBimestre
                .getCONTROLLER_Bimestre().getArrayList());
    }
    
    public void updateObservableListAlumno(String codigo) {
        observableListAlumno = FXCollections.observableArrayList(ControllerAlumno
                .getCONTROLLER_Alumno().buscar(codigo));
        comboBoxAlumno.setItems(observableListAlumno);
    }
    
    public void updateObservableListMateria(String codigo){
        observableListMateria = FXCollections.observableArrayList(
                ControllerAsignacion.getCONTROLLER_AsignarMateria().getArrayListMateria(codigo));
        comboBoxMateria.setItems(observableListMateria);
    }
    
    public void updateObservableListProfesor(String nombre, String codigo){
        observableListProfesor = FXCollections.observableArrayList(
        ControllerAsignacion.getCONTROLLER_AsignarMateria().getArrayListProfesor(codigo, nombre));
        comboBoxProfesor.setItems(observableListProfesor);
    }    
    
    public void restarthBox() {
        hBox.getChildren().clear();
        hBox.getChildren().add(gridPane);
    }
}