/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.luisgarcia.bean.Asignacion;
import org.luisgarcia.controller.ControllerAsignacion;

/**
 *
 * @author Claudio Canel
 */
public class CRUDAsignarMateria {
    private static final CRUDAsignarMateria CRUD_AsignarMateria = new CRUDAsignarMateria();
    private HBox hBox;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Asignacion, Integer> tableColumnIdAsignarMateria;
    private TableColumn<Asignacion, Integer> tableColumnIdGrado;
    private TableColumn<Asignacion, Integer>  tablecolumnIdPeriodo;
    private TableColumn<Asignacion, Integer> tableColumnIdMateria;
    private TableColumn<Asignacion, Integer> tableColumnIdProfesor;
    private TableColumn<Asignacion, Integer> tableColumnIdDia;
    private TableColumn<Asignacion, Integer> tableColumnIdSalon;
    private TableView<Asignacion> tableView;
    private ObservableList observableList;

    private CRUDAsignarMateria() {
    }

    public static CRUDAsignarMateria getCRUD_AsignarMateria() {
        return CRUD_AsignarMateria;
    }

    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setGridLinesVisible(true);
        
        textTitle = new Text("AsignarMaterias");
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar periodo");
        
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateObservableList();
                tableView.setItems(observableList);
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
        
        buttonModificar = new Button("Modificar");
        
        buttonEliminar = new Button("Eliminar");
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdAsignarMateria = new TableColumn<>();
        tableColumnIdAsignarMateria.setText("ID");
        tableColumnIdAsignarMateria.setCellValueFactory(new PropertyValueFactory<>("idAsignarMateria"));
        tableColumnIdAsignarMateria.setMaxWidth(50);
        
        tableColumnIdGrado = new TableColumn<>();
        tableColumnIdGrado.setText("Grado");
        tableColumnIdGrado.setCellValueFactory(new PropertyValueFactory<>("idGrado"));
        tableColumnIdGrado.setMinWidth(100);
        
         tablecolumnIdPeriodo = new TableColumn<>();
         tablecolumnIdPeriodo.setText("Periodo");
         tablecolumnIdPeriodo.setCellValueFactory(new PropertyValueFactory<>("idPeriodo"));
         tablecolumnIdPeriodo.setMinWidth(100);

        tableColumnIdMateria = new TableColumn<>();
        tableColumnIdMateria.setText("Materia");
        tableColumnIdMateria.setCellValueFactory(new PropertyValueFactory<>("IdMateria"));
        tableColumnIdMateria.setMinWidth(100);
        
        tableColumnIdProfesor = new TableColumn<>();
        tableColumnIdProfesor.setText("Profesor");
        tableColumnIdProfesor.setCellValueFactory(new PropertyValueFactory<>("idProfesor"));
        tableColumnIdProfesor.setMinWidth(100);
        
        tableColumnIdDia = new TableColumn<>();
        tableColumnIdDia.setText("Dia");
        tableColumnIdDia.setCellValueFactory(new PropertyValueFactory<>("idDia"));
        tableColumnIdDia.setMinWidth(100);
        
        tableColumnIdSalon = new TableColumn<>();
        tableColumnIdSalon.setText("Salon");
        tableColumnIdSalon.setCellValueFactory(new PropertyValueFactory<>("idSalon"));
        tableColumnIdSalon.setMinWidth(100);
        
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdAsignarMateria, tableColumnIdGrado,
                 tablecolumnIdPeriodo,tableColumnIdMateria, tableColumnIdProfesor,
                tableColumnIdDia, tableColumnIdSalon);
        tableView.setMinSize(600, 600);
        gridPane.add(tableView, 0, 3, 2, 1);
        
        hBox.getChildren().add(gridPane);
        hBox.setAlignment(Pos.CENTER);
        
        return hBox;
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerAsignacion.getCONTROLLER_AsignarMateria().getArrayList());
    }
    
}



