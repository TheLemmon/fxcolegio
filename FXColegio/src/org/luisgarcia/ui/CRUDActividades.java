/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Actividades;
import org.luisgarcia.bean.Asignacion;
import org.luisgarcia.controller.ControllerActividades;
import org.luisgarcia.controller.ControllerAsignacion;

/**
 *
 * @author informatica
 */
public class CRUDActividades {
     private static final CRUDActividades CRUD_Actividades = new CRUDActividades();
    private HBox hBox;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Actividades, Integer> tableColumnIdActividades;
    private TableColumn<Actividades, Actividades> tableColumnAsignacion;
    private TableColumn<Actividades, String>  tableColumnDescripcion;
    private TableColumn<Actividades, Double> tableColumnValorNeto;
    private TableView<Actividades> tableView;
    private ObservableList observableList;

    private CRUDActividades() {
    }

    public static CRUDActividades getCRUD_Actividades() {
        return CRUD_Actividades;
    }
public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Actividadeses");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar periodo");
        textFieldBuscar.textProperty().addListener((newValue) -> {
            updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateObservableList();
                tableView.setItems(observableList);
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
        buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               hBox.getChildren().clear();
               hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(CreateActividades.getCREATE_Actividades().getGridPane()));
            }
        });
        
        buttonModificar = new Button("Modificar");

       buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(tableView.getSelectionModel().getSelectedItem() != null){
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(
                            ModificarActividades.getCREATE_Actividades().getGridPane(tableView.getSelectionModel().getSelectedItem())));
                } else {
                    hBox.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.initStyle(StageStyle.UTILITY);
                    alert.showAndWait();
                }
                
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.initStyle(StageStyle.DECORATED);
                        alert.setTitle("Descripcionlogo de Cofirmacion");
                        alert.setHeaderText("Segudo Que Desea Eliminar ");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK){
                            for(Actividades asignarcion : tableView.getSelectionModel().getSelectedItems()){
                           //     ControllerActividades.getCONTROLLER_AsignarDescripcion().delete(asignarcion.getIdActividades());
                            }
                            restarthBox();
                            updateTableViewItems();

                        } else {
                            }
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Descripcionlgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
       
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdActividades = new TableColumn<>("ID");
        tableColumnIdActividades.setCellValueFactory(new PropertyValueFactory<>("idActividaes"));
        tableColumnIdActividades.setMinWidth(80);
        
        tableColumnAsignacion = new TableColumn<>("Asignacion");
        tableColumnAsignacion.setCellValueFactory(new PropertyValueFactory<>("Asignacion"));
        tableColumnAsignacion.setMinWidth(220);
        
         tableColumnDescripcion = new TableColumn<>("Descripcion");
         tableColumnDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
         tableColumnDescripcion.setMinWidth(120);

        tableColumnValorNeto = new TableColumn<>("Valor");
        tableColumnValorNeto.setCellValueFactory(new PropertyValueFactory<>("valorNeto"));
        tableColumnValorNeto.setMinWidth(80);
                
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdActividades,
                tableColumnAsignacion, tableColumnDescripcion,  tableColumnValorNeto);
        tableView.setMinSize(500, 400);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                //hBox.getChildren().clear();
               // hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(VerActividades.getVer_Actividades()
                      //  .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                }
            }
        });
        
        gridPane.add(tableView, 0, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        hBox.getChildren().add(gridPane);
        hBox.setAlignment(Pos.CENTER_LEFT);
        return hBox;
    }
    
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerActividades.getCONTROLLER_Actividades().getArrayList());
    }
    
    public void updateTableViewItemsbusqueda(String nombre) {
        observableList = FXCollections.observableArrayList(ControllerActividades.getCONTROLLER_Actividades().buscar(nombre));
        tableView.setItems(observableList);
    }
    
    public void restarthBox() {
        hBox.getChildren().clear();
        hBox.getChildren().add(gridPane);
    }
}

class CreateActividades {
    private static final CreateActividades CREATE_Actividades =
            new CreateActividades();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Asignacion> comboBoxAsignacion;
    private Label labelDescripcion;
    private TextField textFieldDescripcion;
    private Label labelValoNeto;
    private TextField textFieldValorrNeto;
    private Button buttonAgregar;
    
    private ObservableList<Asignacion> observableListAsignacion;

    private CreateActividades() {
    }

    public static CreateActividades getCREATE_Actividades() {
        return CREATE_Actividades;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        updateObservableList();
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelSeccion = new Label("Asignacion:");
        gridPane.add(labelSeccion, 0, 2);
        comboBoxAsignacion = new ComboBox<>(observableListAsignacion);
        comboBoxAsignacion.setMinSize(140, 20);
        gridPane.add(comboBoxAsignacion, 1, 2);
        
        labelDescripcion = new Label("Descripcion:");
        gridPane.add(labelDescripcion, 0, 3);    
        textFieldDescripcion = new TextField();
        gridPane.add(textFieldDescripcion, 1, 3);
        
        labelValoNeto = new Label("Valor");
        gridPane.add(labelValoNeto, 0, 4);
        textFieldValorrNeto = new TextField();
        gridPane.add(textFieldValorrNeto, 1, 4);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(comboBoxAsignacion.getSelectionModel().isEmpty() || textFieldDescripcion.getText().isEmpty() ||
                        textFieldValorrNeto.getText().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if(ControllerActividades.getCONTROLLER_Actividades().buscarExistente(
                        textFieldDescripcion.getText(), comboBoxAsignacion.getSelectionModel().getSelectedItem().getIdAsignacion())) {
                    alert.setContentText("La Asignacion ya existe");
                    alert.showAndWait();
                } else {
                    agregar(comboBoxAsignacion.getSelectionModel().getSelectedItem().getIdAsignacion(), 
                            textFieldDescripcion.getText().trim(), Double.parseDouble(textFieldValorrNeto.getText()));
                    CRUDActividades.getCRUD_Actividades().updateTableViewItems();
                }
                
            }
        });
        gridPane.add(buttonAgregar, 0, 8);
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDActividades.getCRUD_Actividades().restarthBox();
            }
        });
        
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
    
   private void agregar(int idAsignacion, String descripcion, double valor) {
        ControllerActividades.getCONTROLLER_Actividades().add(idAsignacion, descripcion, valor);
        CRUDAsignacion.getCRUD_Asignacion().updateTableViewItems();
    }
    
    public void updateObservableList() {
        observableListAsignacion = FXCollections.observableArrayList(ControllerAsignacion.getCONTROLLER_AsignarMateria().getArrayList());
    }

}

class ModificarActividades {
    private static final ModificarActividades Modificar_Actividades =
            new ModificarActividades();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Asignacion> comboBoxAsignacion;
    private Label labelDescripcion;
    private TextField textFieldDescripcion;
    private Label labelValoNeto;
    private TextField textFieldValorrNeto;
    private Button buttonAgregar;
    
    private ObservableList<Asignacion> observableListAsignacion;

    private ModificarActividades() {
    }

    public static ModificarActividades getCREATE_Actividades() {
        return Modificar_Actividades;
    }

    public GridPane getGridPane(Actividades actividades) {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        updateObservableList();
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelSeccion = new Label("Asignacion:");
        gridPane.add(labelSeccion, 0, 2);
        comboBoxAsignacion = new ComboBox<>(observableListAsignacion);
        comboBoxAsignacion.getSelectionModel().select(actividades.getAsignacion().getIdAsignacion() - 1 );
        comboBoxAsignacion.setMinSize(140, 20);
        gridPane.add(comboBoxAsignacion, 1, 2);
        
        labelDescripcion = new Label("Descripcion:");
        gridPane.add(labelDescripcion, 0, 3);    
        textFieldDescripcion = new TextField(actividades.getDescripcion());
        gridPane.add(textFieldDescripcion, 1, 3);
        
        labelValoNeto = new Label("Valor");
        gridPane.add(labelValoNeto, 0, 4);
        textFieldValorrNeto = new TextField(String.valueOf(actividades.getValorNeto()));
        gridPane.add(textFieldValorrNeto, 1, 4);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(comboBoxAsignacion.getSelectionModel().isEmpty() || textFieldDescripcion.getText().isEmpty() ||
                        textFieldValorrNeto.getText().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if(ControllerActividades.getCONTROLLER_Actividades().buscarExistente(
                        textFieldDescripcion.getText(), comboBoxAsignacion.getSelectionModel().getSelectedItem().getIdAsignacion())) {
                    alert.setContentText("La Asignacion ya existe");
                    alert.showAndWait();
                } else {
                    ControllerActividades.getCONTROLLER_Actividades().modify(actividades.getIdActividaes(),
                            comboBoxAsignacion.getSelectionModel().getSelectedItem().getIdAsignacion(), 
                            textFieldDescripcion.getText().trim(), Double.parseDouble(textFieldValorrNeto.getText()));
                    CRUDActividades.getCRUD_Actividades().updateTableViewItems();
                }
                
            }
        });
        gridPane.add(buttonAgregar, 0, 8);
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDActividades.getCRUD_Actividades().restarthBox();
            }
        });
        
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
    
    public void updateObservableList() {
        observableListAsignacion = FXCollections.observableArrayList(ControllerAsignacion.getCONTROLLER_AsignarMateria().getArrayList());
    }

}
