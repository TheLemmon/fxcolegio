/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.luisgarcia.bean.Notas;
import org.luisgarcia.controller.ControllerControlNotas;

/**
 *
 * @author Claudio Canel
 */
public class CRUDControlNotas {
    private static final CRUDControlNotas CRUD_ControlNotas = new CRUDControlNotas();
    private HBox hBox;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Notas, Integer> tableColumnIdControlNotas;
    private TableColumn<Notas, Integer> tableColumnIdAlumno;
    private TableColumn<Notas, Integer>  tablecolumnBimestreI;
    private TableColumn<Notas, Integer> tableColumnIdMateria;
    private TableColumn<Notas, Integer> tableColumnBimestreII;
    private TableColumn<Notas, Integer> tableColumnBimestreIII;
    private TableColumn<Notas, Integer> tableColumnBimestreIV;
    private TableColumn<Notas, Integer> tableColumnBimestreV;
    private TableColumn<Notas, Integer> tableColumnExamenFinal;
    private TableColumn<Notas, Integer> tableColumnPromedio;
    private TableView<Notas> tableView;
    private ObservableList observableList;

    private CRUDControlNotas() {
    }

    public static CRUDControlNotas getCRUD_ControlNotas() {
        return CRUD_ControlNotas;
    }

    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setGridLinesVisible(true);
        
        textTitle = new Text("ControlNotass");
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar periodo");
        
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateObservableList();
                tableView.setItems(observableList);
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
        
        buttonModificar = new Button("Modificar");
        
        buttonEliminar = new Button("Eliminar");
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdControlNotas = new TableColumn<>();
        tableColumnIdControlNotas.setText("ID");
        tableColumnIdControlNotas.setCellValueFactory(new PropertyValueFactory<>("idControlNotas"));
        tableColumnIdControlNotas.setMaxWidth(50);
        
        tableColumnIdAlumno = new TableColumn<>();
        tableColumnIdAlumno.setText("Alumno");
        tableColumnIdAlumno.setCellValueFactory(new PropertyValueFactory<>("idAlumno"));
        tableColumnIdAlumno.setMinWidth(100);
        
         tablecolumnBimestreI = new TableColumn<>();
         tablecolumnBimestreI.setText("Bimestre I");
         tablecolumnBimestreI.setCellValueFactory(new PropertyValueFactory<>("bimestreI"));
         tablecolumnBimestreI.setMinWidth(100);

        tableColumnIdMateria = new TableColumn<>();
        tableColumnIdMateria.setText("Materia");
        tableColumnIdMateria.setCellValueFactory(new PropertyValueFactory<>("IdMateria"));
        tableColumnIdMateria.setMinWidth(100);
        
        tableColumnBimestreII = new TableColumn<>();
        tableColumnBimestreII.setText("Bimestre II");
        tableColumnBimestreII.setCellValueFactory(new PropertyValueFactory<>("bimestreII"));
        tableColumnBimestreII.setMinWidth(100);
        
        tableColumnBimestreIII = new TableColumn<>();
        tableColumnBimestreIII.setText("Bimestre III");
        tableColumnBimestreIII.setCellValueFactory(new PropertyValueFactory<>("bimestreIII"));
        tableColumnBimestreIII.setMinWidth(100);
        
        tableColumnBimestreIV = new TableColumn<>();
        tableColumnBimestreIV.setText("Bimestre IV");
        tableColumnBimestreIV.setCellValueFactory(new PropertyValueFactory<>("bimestreIV"));
        tableColumnBimestreIV.setMinWidth(100);
        
        tableColumnBimestreV = new TableColumn<>();
        tableColumnBimestreV.setText("Bimestre V");
        tableColumnBimestreV.setCellValueFactory(new PropertyValueFactory<>("bimestreV"));
        tableColumnBimestreV.setMinWidth(100);
        
        tableColumnExamenFinal = new TableColumn<>();
        tableColumnExamenFinal.setText("Examen Final");
        tableColumnExamenFinal.setCellValueFactory(new PropertyValueFactory<>("examenFinal"));
        tableColumnBimestreIV.setMinWidth(100);
        
        tableColumnPromedio = new TableColumn<>();
        tableColumnPromedio.setText("Promedio");
        tableColumnPromedio.setCellValueFactory(new PropertyValueFactory<>("Promedio"));
        tableColumnPromedio.setMinWidth(100);
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdControlNotas, tableColumnIdAlumno,
                 tablecolumnBimestreI,tableColumnIdMateria, tableColumnBimestreII,
                tableColumnBimestreIII, tableColumnBimestreIV, tableColumnBimestreV,
                tableColumnExamenFinal, tableColumnPromedio);
        tableView.setMinSize(600, 600);
        gridPane.add(tableView, 0, 3, 2, 1);
        
        hBox.getChildren().add(gridPane);
        hBox.setAlignment(Pos.CENTER);
        
        return hBox;
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerControlNotas.getCONTROLLER_ControlNotas().getArrayList());
    }
    
}



