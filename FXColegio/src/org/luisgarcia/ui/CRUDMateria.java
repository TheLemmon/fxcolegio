/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Materia;
import org.luisgarcia.controller.ControllerMateria;

/**
 *
 * @author Claudio Canel
 */
public class CRUDMateria {
    private static final CRUDMateria CRUD_Materia = new CRUDMateria();
    private HBox hBoxCRUD;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Materia, Integer> tableColumnIdMateria;
    private TableColumn<Materia, String> tableColumnNombre;
    private TableView<Materia> tableView;
    private ObservableList observableList;

    private CRUDMateria() {
	
    }

    public static CRUDMateria getCRUD_Materia() {
        return CRUD_Materia;
    }
    
    public void restarthBoxCRUD() {
        hBoxCRUD.getChildren().clear();
        hBoxCRUD.getChildren().add(gridPane);
    }
    
    public void updateTableViewItems() {
         updateObservableList();
         tableView.setItems(observableList);
    }

    public void updateTableViewItemsbusqueda(String nombre) {
        observableList = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_Materia().buscar(nombre));
        tableView.setItems(observableList);
    }
    
    public HBox gethBoxCRUD() {
        hBoxCRUD = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Materias");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar materia");
        textFieldBuscar.textProperty().addListener((newValue) -> {
            updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Actualizar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateTableViewItems();
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        /*gridPane.addEventFilter(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
          public void handle(MouseEvent e) {
            restarthBoxCRUD();
          };
        });*/
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
            buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    hBoxCRUD.getChildren().clear();
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(CreateMateria.getCREATE_Materia().getGridPane()));
            }
        });
        
        buttonModificar = new Button("Modificar");
        
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBoxCRUD.getChildren().clear();
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(UpdateMateria.getUPDATE_Materia()
                            .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                } else {
                    hBoxCRUD.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.initStyle(StageStyle.UTILITY);
                    alert.showAndWait();
                }
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.initStyle(StageStyle.DECORATED);
                        alert.setTitle("Materialogo de Cofirmacion");
                        alert.setHeaderText("Segudo Que Desea Eliminar ");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK){
                            for(Materia materia : tableView.getSelectionModel().getSelectedItems()){
                                ControllerMateria.getCONTROLLER_Materia().delete(materia.getIdMateria());
                            }
                            restarthBoxCRUD();
                            updateTableViewItems();
                        } 
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Materialgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdMateria = new TableColumn<>();
        tableColumnIdMateria.setText("ID");
        tableColumnIdMateria.setCellValueFactory(new PropertyValueFactory<>("idMateria"));
        tableColumnIdMateria.setMinWidth(90);
        
        tableColumnNombre = new TableColumn<>();
        tableColumnNombre.setText("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tableColumnNombre.setMinWidth(260);
       
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdMateria, tableColumnNombre);
        tableView.setMinSize(350, 400);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                    if(!tableView.getSelectionModel().isEmpty()){
                        hBoxCRUD.getChildren().clear();
                        hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation()
                                .getGridPanAnimation(VerMateria.getVer_Materia()
                                .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                    }
                }
            }
        });
        
        gridPane.add(tableView, 0, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        
        hBoxCRUD.getChildren().add(gridPane);
        hBoxCRUD.setAlignment(Pos.CENTER_LEFT);
        hBoxCRUD.setMinSize(700, 600);
        return hBoxCRUD;
    }
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_Materia().getArrayList());
    }
    
    public void ClearSelection(){
        tableView.getSelectionModel().clearSelection();
    }
}    
    /////////////   Creacion de Materia ///////////////////////
    
    class CreateMateria {
    private static final CreateMateria CREATE_Materia = new CreateMateria();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonAgregar;

    private CreateMateria() {
    }

    public static CreateMateria getCREATE_Materia() {
        return CREATE_Materia;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        
        textFieldNombre = new TextField();
        textFieldNombre.setPromptText("Nombre");
 
        gridPane.add(textFieldNombre, 1, 3, 2, 1);
        String dato = textFieldNombre.getText();
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(textFieldNombre.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();   
                } else if (ControllerMateria.getCONTROLLER_Materia().getMateria
                            (textFieldNombre.getText().trim()) != null){
                    alert.setContentText("Ya existe la materia");
                    alert.showAndWait();
                } else {
                ControllerMateria.getCONTROLLER_Materia().add(textFieldNombre.getText().trim());
                CRUDMateria.getCRUD_Materia().updateTableViewItems();
                textFieldNombre.clear();
                }
                try{
                    CreateAsignacion.getCREATE_LIBRO().updateTableViewItems();
                } catch(NullPointerException eo){
                    
                }
            }
 
        });;
        gridPane.add(buttonAgregar, 0, 5);
        
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDMateria.getCRUD_Materia().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 2, 5);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(450, 400);
        
        return gridPane;
    }

    
}
        ////////////////////        ACTUALIZACION DE DIA   /////////////////////
class UpdateMateria {
    private static final UpdateMateria UPDATE_Materia = new UpdateMateria();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;

    private UpdateMateria() {
    }

    public static UpdateMateria getUPDATE_Materia() {
        return UPDATE_Materia;
    }

    public GridPane getGridPane(Materia materia) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Modificar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        
        textFieldNombre = new TextField(materia.getNombre());
        gridPane.add(textFieldNombre, 1, 3, 2, 1);
        
        
        buttonModificar = new Button("Modificar");
        buttonModificar.setDefaultButton(true);
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(textFieldNombre.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                   
                } else if (ControllerMateria.getCONTROLLER_Materia().getMateria
                            (textFieldNombre.getText().trim()) != null){
                    alert.setContentText("Ya existe la materia");
                    alert.showAndWait();
                } else {
                    ControllerMateria.getCONTROLLER_Materia().modify(materia.getIdMateria(), 
                        textFieldNombre.getText().trim());
                    CRUDMateria.getCRUD_Materia().updateTableViewItems();
                }
                try{
                    CreateAsignacion.getCREATE_LIBRO().updateTableViewItems();
                } catch(NullPointerException eo){
                    
                }
            }
        });
        gridPane.add(buttonModificar, 0, 5);
        
        Button buttonCerrar = new Button("Cerrar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDMateria.getCRUD_Materia().restarthBoxCRUD();
                CRUDMateria.getCRUD_Materia().ClearSelection();
            }
        });
        gridPane.add(buttonCerrar, 2, 5);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(400, 400);
        
        return gridPane;
    }
}

class VerMateria {
    private static final VerMateria Ver_Materia = new VerMateria();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;

    private VerMateria() {
    }

    public static VerMateria getVer_Materia() {
        return Ver_Materia;
    }

    public GridPane getGridPane(Materia materia) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Ver");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        
        textFieldNombre = new TextField(materia.getNombre());
        textFieldNombre.setEditable(false);
        gridPane.add(textFieldNombre, 1, 3, 2, 1);
        
        Button buttonCerrar = new Button("Cerrar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDMateria.getCRUD_Materia().restarthBoxCRUD();
                CRUDMateria.getCRUD_Materia().ClearSelection();
            }
        });
        gridPane.add(buttonCerrar, 2, 5);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(400, 400);
        
        return gridPane;
    }
}