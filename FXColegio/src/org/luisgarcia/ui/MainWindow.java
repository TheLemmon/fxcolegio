/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import javafx.scene.image.Image;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.luisgarcia.controller.ControllerUsuario;

/**
 *
 * @author Claudio Canel
 */
public class MainWindow extends Application {
    private VBox vBox;
    private MenuBar menuBar;
    private Menu menuUsuario;
    private MenuItem menuDesconectar;
    private Stage stage;
    
    private Scene scene;

    public MainWindow() {
    }
    
    @Override
    public void start(Stage primaryStage) {
        
        stage = primaryStage;
        vBox = new VBox();
             
        scene = new Scene(vBox, 1100, 650);
        scene.getStylesheets().addAll("/org/luisgarcia/resource/root.css");
        

        Image icon = new Image(getClass().getResourceAsStream("colegio.png"));
        stage.getIcons().add(icon);
        stage.setScene(scene);
        stage.setResizable(false);
        
       
        AccessWindow.getACCESS_WINDOW().showWindow();
        AccessWindow.getACCESS_WINDOW().addEventFilter(WindowEvent.WINDOW_HIDING, new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                if (ControllerUsuario.getCONTROLLER_Usuario().getAuthenticate()) {
                    stage.show();
                    Dashboard.getDASHBOARD().updateTabPane();
                    updateVbox();
                }
                
            } 
        });
    }

    public MenuBar getMenuBar() {
        
        menuBar = new MenuBar();
        
        menuUsuario = new Menu("_Usuario");
        menuDesconectar = new MenuItem("_Desconectar");
        menuDesconectar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                ControllerUsuario.getCONTROLLER_Usuario().deauthenticate();
                stage.close();
                AccessWindow.getACCESS_WINDOW().showWindow();
            }
        });
        menuUsuario.getItems().add(menuDesconectar);
        menuBar.getMenus().addAll(menuUsuario);
        return menuBar;
    }
    public void updateVbox(){
        vBox.getChildren().clear();
        vBox.getChildren().addAll(Dashboard.getDASHBOARD().getTabPane(), getMenuBar());
        
    }
        
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
