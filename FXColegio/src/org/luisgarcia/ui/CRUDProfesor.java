/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Profesor;
import org.luisgarcia.controller.ControllerProfesor;

/**
 *
 * @author Claudio Canel
 */
public class CRUDProfesor {
    private static final CRUDProfesor CRUD_Profesor = new CRUDProfesor();
    private HBox hBoxCRUD;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Profesor, Integer> tableColumnIdProfesor;
    private TableColumn<Profesor, String> tableColumnDpi;
    private TableColumn<Profesor, String> tableColumnNombre;
    private TableColumn<Profesor, String> tableColumnTelefono;
    private TableColumn<Profesor, String> tableColumnDireccion;
    private TableColumn<Profesor, String> tableColumnCorreo;
    private TableView<Profesor> tableView;
    private ObservableList observableList;

    private CRUDProfesor() {
    }

    public static synchronized CRUDProfesor getCRUD_Profesor() {
        return CRUD_Profesor;
    }
    
    public void restarthBoxCRUD() {
        hBoxCRUD.getChildren().clear();
        hBoxCRUD.getChildren().add(gridPane);
    }
    
    public void updateTableViewItems() {
         updateObservableList();
         tableView.setItems(observableList);
    }
    
    public void updateTableViewItemsbusqueda(String nombre) {
         observableList = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_Profesor().buscar(nombre));
         tableView.setItems(observableList);
    }
    
    public HBox gethBoxCRUD() {
        hBoxCRUD = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("  Profesores  ");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar profesor");
        textFieldBuscar.textProperty().addListener((newValue) -> {
            updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateTableViewItems();
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
            buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    hBoxCRUD.getChildren().clear();
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(CreateProfesor.getCREATE_Profesor().getGridPane()));
            }
        });
        
        buttonModificar = new Button("Modificar");
        
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBoxCRUD.getChildren().clear();
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(UpdateProfesor.getUPDATE_Profesor()
                            .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                } else {
                    hBoxCRUD.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Profesorlgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.showAndWait();
                }
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.initStyle(StageStyle.DECORATED);
                        alert.setTitle("Cofirmacion");
                        alert.setHeaderText("Segudo Que Desea Eliminar ");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK){
                            for(Profesor profesor: tableView.getSelectionModel().getSelectedItems()){
                                ControllerProfesor.getCONTROLLER_Profesor().delete(tableView.getSelectionModel().getSelectedItem().getIdProfesor());
                            }
                            restarthBoxCRUD();
                            updateTableViewItems();

                        } else {
                            }
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Dialogo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdProfesor = new TableColumn<>();
        tableColumnIdProfesor.setText("ID");
        tableColumnIdProfesor.setCellValueFactory(new PropertyValueFactory<>("idProfesor"));
        tableColumnIdProfesor.setMinWidth(70);
        
        tableColumnDpi = new TableColumn<>();
        tableColumnDpi.setText("Dpi");
        tableColumnDpi.setCellValueFactory(new PropertyValueFactory<>("dpi"));
        tableColumnDpi.setMinWidth(100);
        
        tableColumnNombre = new TableColumn<>();
        tableColumnNombre.setText("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tableColumnNombre.setMinWidth(200);
        
        tableColumnTelefono = new TableColumn<>();
        tableColumnTelefono.setText("Telefono");
        tableColumnTelefono.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        tableColumnTelefono.setMinWidth(100);
        
        tableColumnDireccion = new TableColumn<>();
        tableColumnDireccion.setText("Direccion");
        tableColumnDireccion.setCellValueFactory(new PropertyValueFactory<>("direccion"));
        tableColumnDireccion.setMinWidth(100);
        
        tableColumnCorreo = new TableColumn<>();
        tableColumnCorreo.setText("Correo");
        tableColumnCorreo.setCellValueFactory(new PropertyValueFactory<>("correo"));
        tableColumnCorreo.setMinWidth(100);
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdProfesor, tableColumnDpi, tableColumnNombre,
                tableColumnTelefono, tableColumnDireccion, tableColumnCorreo);
        tableView.setMinSize(670, 400);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                hBoxCRUD.getChildren().clear();
                hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(VerProfesor.getVer_Profesor()
                        .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                }
            }
        });
        
        gridPane.add(tableView, 0, 3, 2, 1);
        
        hBoxCRUD.getChildren().add(gridPane);
        hBoxCRUD.setAlignment(Pos.CENTER_LEFT);
        hBoxCRUD.setMinSize(630, 400);
        return hBoxCRUD;
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_Profesor().getArrayList());
    }
}    
    /////////////   Creacion de Profesor ///////////////////////
    
    class CreateProfesor {
    private static final CreateProfesor CREATE_Profesor = new CreateProfesor();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Button buttonAgregar;

    private CreateProfesor() {
    }

    public static synchronized CreateProfesor getCREATE_Profesor() {
        return CREATE_Profesor;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        Label labelDpi = new Label("Dpi: ");
        gridPane.add(labelDpi, 0, 3);
        TextField textFieldDpi = new TextField();
        textFieldDpi.setPromptText("Dpi");
        gridPane.add(textFieldDpi, 1, 3, 2, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 4);
        textFieldNombre = new TextField();
        textFieldNombre.setPromptText("Nombre");
        gridPane.add(textFieldNombre, 1, 4, 2, 1);
        
        Label labelDireccion = new Label("Direccion: ");
        gridPane.add(labelDireccion, 0, 5);
        TextField textFieldDireccion = new TextField();
        textFieldDireccion.setPromptText("Direccion");
        gridPane.add(textFieldDireccion, 1, 5, 2, 1);
        
        Label labelTelefono = new Label("Telefono: ");
        gridPane.add(labelTelefono, 0, 6);
        TextField textFieldTelefono = new TextField();
        textFieldTelefono.setPromptText("Telefono");
        gridPane.add(textFieldTelefono, 1, 6, 2, 1);
        
        Label labelCorreo = new Label("Correo: ");
        gridPane.add(labelCorreo, 0, 7);
        TextField textFieldCorreo = new TextField();
        textFieldCorreo.setPromptText("Correo");
        gridPane.add(textFieldCorreo, 1, 7, 2, 1);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(textFieldNombre.getText().trim().isEmpty() || textFieldDpi.getText().trim().isEmpty()
                        || textFieldTelefono.getText().trim().isEmpty() || textFieldDireccion.getText().trim().isEmpty()
                        || textFieldCorreo.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();

                } else if(ControllerProfesor.getCONTROLLER_Profesor().getProfesor(textFieldNombre.getText())){
                    alert.setContentText("El profesor ya existe");
                    alert.showAndWait();
                } else {
                    ControllerProfesor.getCONTROLLER_Profesor().add(textFieldDpi.getText(), textFieldNombre.getText(), 
                            textFieldDireccion.getText(), textFieldTelefono.getText(), textFieldCorreo.getText());
                    CRUDProfesor.getCRUD_Profesor().updateTableViewItems();
                    textFieldNombre.clear();
                    textFieldDpi.clear();
                    textFieldTelefono.clear();
                    textFieldDireccion.clear();
                    textFieldCorreo.clear();
                }
                try{
                    CreateAsignacion.getCREATE_LIBRO().updateTableViewItems();
                } catch(NullPointerException eo){
                    
                }
            }
 
        });;
        gridPane.add(buttonAgregar, 0, 9);
        
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDProfesor.getCRUD_Profesor().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 1, 9);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        
        return gridPane;
    }
}
        ////////////////////        ACTUALIZACION DE DIA   /////////////////////
class UpdateProfesor {
    private static final UpdateProfesor UPDATE_Profesor = new UpdateProfesor();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;

    private UpdateProfesor() {
    }

    public static synchronized UpdateProfesor getUPDATE_Profesor() {
        return UPDATE_Profesor;
    }

    public GridPane getGridPane(Profesor profesor) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("      Modificar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 2, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        textFieldNombre = new TextField(profesor.getNombre());
        gridPane.add(textFieldNombre, 1, 3, 2, 1);
        
        Label labelDpi = new Label("Dpi");
        gridPane.add(labelDpi, 0, 4);
        TextField textFieldDpi = new TextField(profesor.getDpi());
        gridPane.add(textFieldDpi, 1, 4, 2, 1);
        
        Label labelDireccion = new Label("Direccion");
        gridPane.add(labelDireccion, 0, 5);
        TextField textFieldDireccion = new TextField(profesor.getDireccion());
        gridPane.add(textFieldDireccion, 1, 5, 2, 1);
        
        
        Label labelTelefono = new Label("Telefono");
        gridPane.add(labelTelefono, 0, 6);
        TextField textFieldTelefono = new TextField(profesor.getTelefono());
        gridPane.add(textFieldTelefono, 1, 6, 2, 1);
        
        Label labelCorreo = new Label("Correo");
        gridPane.add(labelCorreo, 0, 7);
        TextField textFieldCorreo = new TextField(profesor.getCorreo());
        gridPane.add(textFieldCorreo, 1, 7, 2, 1);
        
        
        buttonModificar = new Button("Modificar");
        buttonModificar.setDefaultButton(true);
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(textFieldNombre.getText().trim().isEmpty() || textFieldDpi.getText().trim().isEmpty()
                        || textFieldTelefono.getText().trim().isEmpty() || textFieldDireccion.getText().trim().isEmpty()
                        || textFieldCorreo.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();

                } else if(ControllerProfesor.getCONTROLLER_Profesor().getProfesor(textFieldNombre.getText())){
                    alert.setContentText("El profesor ya existe");
                    alert.showAndWait();
                } else {
                    ControllerProfesor.getCONTROLLER_Profesor().modify(profesor.getIdProfesor(), 
                        textFieldDpi.getText(), textFieldNombre.getText(), textFieldDireccion.getText(),
                        textFieldTelefono.getText(), textFieldCorreo.getText());
                    CRUDProfesor.getCRUD_Profesor().updateTableViewItems();   
                }
                try{
                    CreateAsignacion.getCREATE_LIBRO().updateTableViewItems();
                } catch(NullPointerException eo){
                    
                }
            }
        });
        gridPane.add(buttonModificar, 0, 9);
        
        Button buttonCerrar = new Button("Cerrar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDProfesor.getCRUD_Profesor().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 1, 9);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(480, 400);
        
        return gridPane;
    }
}

class VerProfesor {
    private static final VerProfesor Ver_Profesor = new VerProfesor();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;

    private VerProfesor() {
    }

    public static VerProfesor getVer_Profesor() {
        return Ver_Profesor;
    }

    public GridPane getGridPane(Profesor profesor) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Ver");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        textFieldNombre = new TextField(profesor.getNombre());
        textFieldNombre.setEditable(false);
        gridPane.add(textFieldNombre, 1, 3, 3, 1);
        
        Label labelDpi = new Label("Dpi");
        gridPane.add(labelDpi, 0, 4);
        TextField textFieldDpi = new TextField(profesor.getDpi());
        textFieldDpi.setEditable(false);
        gridPane.add(textFieldDpi, 1, 4, 3, 1);
        
        Label labelDireccion = new Label("Direccion");
        gridPane.add(labelDireccion, 0, 5);
        TextField textFieldDireccion = new TextField(profesor.getDireccion());
        textFieldDireccion.setEditable(false);
        gridPane.add(textFieldDireccion, 1, 5, 3, 1);
        
        Label labelTelefono = new Label("Telefono");
        gridPane.add(labelTelefono, 0, 6);
        TextField textFieldTelefono = new TextField(profesor.getTelefono());
        textFieldTelefono.setEditable(false);
        gridPane.add(textFieldTelefono, 1, 6, 3, 1);
        
        Label labelCorreo = new Label("Correo");
        gridPane.add(labelCorreo, 0, 7);
        TextField textFieldCorreo = new TextField(profesor.getCorreo());
        textFieldCorreo.setEditable(false);
        gridPane.add(textFieldCorreo, 1, 7, 3, 1);
        
        Button buttonCerrar = new Button("Cerrar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDProfesor.getCRUD_Profesor().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 1, 9);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(480, 400);
        
        return gridPane;
    }
}