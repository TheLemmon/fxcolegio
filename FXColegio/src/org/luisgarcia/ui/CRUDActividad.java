/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Actividad;
import org.luisgarcia.bean.Alumno;
import org.luisgarcia.bean.Asignacion;
import org.luisgarcia.bean.Bimestre;
import org.luisgarcia.bean.Materia;
import org.luisgarcia.bean.Profesor;
import org.luisgarcia.bean.Seccion;
import org.luisgarcia.controller.ControllerActividad;
import org.luisgarcia.controller.ControllerActividadAlumno;
import org.luisgarcia.controller.ControllerAlumno;
import org.luisgarcia.controller.ControllerAsignacion;
import org.luisgarcia.controller.ControllerBimestre;
import org.luisgarcia.controller.ControllerMateria;
import org.luisgarcia.controller.ControllerProfesor;
import org.luisgarcia.controller.ControllerSeccion;

/**
 *
 * @author informatica
 */
public class CRUDActividad {
    private static final CRUDActividad CRUD_Actividad = new CRUDActividad();
    private HBox hBox;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Actividad, Integer> tableColumnIdActividades;
    private TableColumn<Actividad, Asignacion> tableColumnAsignacion;
    private TableColumn<Actividad, Bimestre> tableColumnBimestre;
    private TableColumn<Actividad, String>  tableColumnDescripcion;
    private TableColumn<Actividad, Integer> tableColumnValorNeto;
    private TableView<Actividad> tableView;
    private ObservableList observableList;
    private SpinnerValueFactory svf;

    private CRUDActividad() {
    }

    public static CRUDActividad getCRUD_Actividad() {
        return CRUD_Actividad;
    }
    
    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Actividadeses");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar periodo");
        textFieldBuscar.textProperty().addListener((newValue) -> {
            updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateObservableList();
                tableView.setItems(observableList);
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
        buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
               hBox.getChildren().clear();
               hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(CreateActividad.getCREATE_Actividades().getGridPane()));
            }
        });
        
        buttonModificar = new Button("Modificar");

       buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(tableView.getSelectionModel().getSelectedItem() != null){
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(
                            ModificarActividad.getCREATE_Actividades().getGridPane(tableView.getSelectionModel().getSelectedItem())));
                } else {
                    hBox.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.initStyle(StageStyle.UTILITY);
                    alert.showAndWait();
                }
                
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.initStyle(StageStyle.DECORATED);
                        alert.setTitle("Descripcionlogo de Cofirmacion");
                        alert.setHeaderText("Segudo Que Desea Eliminar ");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK){
                            for(Actividad asignarcion : tableView.getSelectionModel().getSelectedItems()){
                                ControllerActividad.getCONTROLLER_Actividad().delete(asignarcion.getIdActividad());
                            }
                            restarthBox();
                            updateTableViewItems();

                        } else {
                            }
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Descripcionlgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
       
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdActividades = new TableColumn<>("ID");
        tableColumnIdActividades.setCellValueFactory(new PropertyValueFactory<>("idActividad"));
        tableColumnIdActividades.setMinWidth(80);
        
        tableColumnAsignacion = new TableColumn<>("Asignacion");
        tableColumnAsignacion.setCellValueFactory(new PropertyValueFactory<>("Asignacion"));
        tableColumnAsignacion.setMinWidth(220);
        
        tableColumnBimestre = new TableColumn<>("Bimestre");
        tableColumnBimestre.setCellValueFactory(new PropertyValueFactory<>("Bimestre"));
        tableColumnBimestre.setMinWidth(100);
        
         tableColumnDescripcion = new TableColumn<>("Descripcion");
         tableColumnDescripcion.setCellValueFactory(new PropertyValueFactory<>("descripcion"));
         tableColumnDescripcion.setMinWidth(120);

        tableColumnValorNeto = new TableColumn<>("Valor");
        tableColumnValorNeto.setCellValueFactory(new PropertyValueFactory<>("valorNeto"));
        tableColumnValorNeto.setMinWidth(80);
                
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdActividades,
                tableColumnAsignacion, tableColumnDescripcion,  tableColumnValorNeto, tableColumnBimestre);
        tableView.setMinSize(600, 400);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                //hBox.getChildren().clear();
               // hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(VerActividades.getVer_Actividades()
                      //  .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                }
            }
        });
        
        gridPane.add(tableView, 0, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        hBox.getChildren().add(gridPane);
        hBox.setAlignment(Pos.CENTER_LEFT);
        return hBox;
    }
    
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerActividad.getCONTROLLER_Actividad().getArrayList());
    }
    
    public void updateTableViewItemsbusqueda(String nombre) {
        observableList = FXCollections.observableArrayList(ControllerActividad.getCONTROLLER_Actividad().buscar(nombre));
        tableView.setItems(observableList);
    }
    
    public void restarthBox() {
        hBox.getChildren().clear();
        hBox.getChildren().add(gridPane);
    }
}

class CreateActividad {
    private static final CreateActividad CREATE_Actividades =
            new CreateActividad();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Seccion> comboBoxSeccion;
    private ComboBox<Materia> comboBoxMateria;
    private ComboBox<Profesor> comboBoxProfesor;
     private ComboBox<Alumno> comboBoxAlumno;
    private ComboBox<Bimestre> comboBoxBimestre;
    private Label labelDescripcion;
    private TextField textFieldDescripcion;
    private Label labelValoNeto;
    private TextField textFieldValorrNeto;
    private Button buttonAgregar;
    private int valor = 1;
    private SpinnerValueFactory svf;
    private  Spinner sp;
    private int x;
    private ObservableList<Seccion> observableListSeccion;
    private ObservableList<Materia> observableListMateria;
    private ObservableList<Profesor> observableListProfesor;
    private ObservableList<Bimestre> observableListBimestre;
    
    private CreateActividad() {
    }

    public static CreateActividad getCREATE_Actividades() {
        return CREATE_Actividades;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        updateObservableList();
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 2);
        comboBoxSeccion = new ComboBox<>(observableListSeccion);
        comboBoxSeccion.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(comboBoxSeccion.getSelectionModel().getSelectedItem() != null){
                    comboBoxMateria.getSelectionModel().clearSelection();
                comboBoxProfesor.getSelectionModel().clearSelection();
                updateObservableListMateria(comboBoxSeccion.getSelectionModel()
                        .getSelectedItem().getCodigo());
                }
                
            }
        });
        comboBoxSeccion.setMinSize(140, 20);
        gridPane.add(comboBoxSeccion, 1, 2);
        
        Label labelMateria = new Label("Materia:");
        gridPane.add(labelMateria, 0, 3);
        comboBoxMateria = new ComboBox<>(observableListMateria);
        comboBoxMateria.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(comboBoxSeccion.getSelectionModel().getSelectedItem() != null){
                    updateObservableListProfesor(comboBoxMateria.getSelectionModel()
                        .getSelectedItem().getNombre(), comboBoxSeccion.getSelectionModel()
                                .getSelectedItem().getCodigo());
                }
                    
                
            }
        });
        comboBoxMateria.setMinSize(140, 20);
        gridPane.add(comboBoxMateria, 1, 3);
        
        
        Label labelBimestre = new Label("Bimestre:");
        gridPane.add(labelBimestre, 0, 5);
        comboBoxBimestre = new ComboBox<>(observableListBimestre);
        comboBoxBimestre.setOnAction(new EventHandler<ActionEvent>() { 
            @Override
            public void handle(ActionEvent event) {
                if(comboBoxSeccion.getSelectionModel().getSelectedItem() != null &&
                   comboBoxMateria.getSelectionModel().getSelectedItem() != null &&
                   comboBoxProfesor.getSelectionModel().getSelectedItem() != null &&
                   comboBoxBimestre.getSelectionModel().getSelectedItem() != null){
                        
                    valor = ControllerActividad.getCONTROLLER_Actividad().getValorNeto(
                        ControllerAsignacion.getCONTROLLER_AsignarMateria().searchOne(
                            comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo(),
                            comboBoxMateria.getSelectionModel().getSelectedItem().getNombre(),
                            comboBoxProfesor.getSelectionModel().getSelectedItem().getNombre()).getIdAsignacion(),
                            comboBoxBimestre.getSelectionModel().getSelectedItem().getIdBimestre());
                     svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, valor);
                     sp.setValueFactory(svf);
                }
                 
            }
        });
        comboBoxBimestre.setMinSize(140, 20);
        gridPane.add(comboBoxBimestre, 1, 5);
        
       
        Label labelProfesor = new Label("Profesor:");
        gridPane.add(labelProfesor, 0, 4);
        comboBoxProfesor = new ComboBox<>(observableListProfesor);
        
        comboBoxProfesor.setMinSize(140, 20);
        gridPane.add(comboBoxProfesor, 1, 4);
        
        labelDescripcion = new Label("Descripcion:");
        gridPane.add(labelDescripcion, 0, 6);    
        textFieldDescripcion = new TextField();
        gridPane.add(textFieldDescripcion, 1, 6);
        
        labelValoNeto = new Label("Valor");
        gridPane.add(labelValoNeto, 0, 7);
        sp = new Spinner();
        sp.setValueFactory(svf);
        sp.setMinSize(140, 20);
        sp.setPrefWidth(80);
        sp.setEditable(true);
        gridPane.add(sp, 1, 7);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(Integer.parseInt(sp.getEditor().getText()) > valor){
                     alert.setContentText("La Nota es muy grande");
                    alert.showAndWait();
                } else {
                    if(comboBoxSeccion.getSelectionModel().isEmpty() ||
                        comboBoxMateria.getSelectionModel().isEmpty() ||
                        comboBoxProfesor.getSelectionModel().isEmpty() ||
                        textFieldDescripcion.getText().isEmpty() ||
                        comboBoxBimestre.getSelectionModel().isEmpty() || 
                        sp.getEditor().getText().equalsIgnoreCase("0")){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if(ControllerActividad.getCONTROLLER_Actividad().buscarExistente(
                        textFieldDescripcion.getText(), ControllerAsignacion.getCONTROLLER_AsignarMateria().searchOne(
                            comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo(),
                            comboBoxMateria.getSelectionModel().getSelectedItem().getNombre(),
                            comboBoxProfesor.getSelectionModel().getSelectedItem().getNombre()).getIdAsignacion(), 
                            Integer.parseInt(sp.getEditor().getText()))) {
                    alert.setContentText("La Actividad ya existe");
                    alert.showAndWait();
                } else {
                    agregar(ControllerAsignacion.getCONTROLLER_AsignarMateria().
                            searchOne(comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo(),
                                    comboBoxMateria.getSelectionModel().getSelectedItem().getNombre(),
                                    comboBoxProfesor.getSelectionModel().getSelectedItem().getNombre()).getIdAsignacion(),
                            comboBoxBimestre.getSelectionModel().getSelectedItem().getIdBimestre(),
                            textFieldDescripcion.getText().trim(), Integer.parseInt(sp.getEditor().getText()));
                    
                    
                    for(Alumno alumno : ControllerAlumno.getCONTROLLER_Alumno().buscar(
                            comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo())){
                        ControllerActividadAlumno.getCONTROLLER_ActividadAlumno().add(
                                ControllerActividad.getCONTROLLER_Actividad().getInt(
                                    comboBoxSeccion.getSelectionModel().getSelectedItem().getIdSeccion(), 
                                    comboBoxProfesor.getSelectionModel().getSelectedItem().getIdProfesor(),
                                    comboBoxMateria.getSelectionModel().getSelectedItem().getIdMateria()),
                                alumno.getIdAlumno(), 0, Integer.parseInt(sp.getEditor().getText()));
                    }
                    CRUDActividad.getCRUD_Actividad().updateTableViewItems();
                    comboBoxSeccion.getSelectionModel().clearSelection();
                    comboBoxMateria.getSelectionModel().clearSelection();
                    comboBoxProfesor.getSelectionModel().clearSelection();
                    comboBoxBimestre.getSelectionModel().clearSelection();
                    textFieldDescripcion.clear();
                     svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 0);
                     sp.setValueFactory(svf);
                     
                }
                }
                
                
            }
        });
        gridPane.add(buttonAgregar, 0, 8);
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDActividad.getCRUD_Actividad().restarthBox();
            }
        });
        
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
    
   private void agregar(int idAsignacion, int idBimestre, String descripcion, int valor) {
        ControllerActividad.getCONTROLLER_Actividad().add(idAsignacion, idBimestre, descripcion, valor);
        CRUDAsignacion.getCRUD_Asignacion().updateTableViewItems();
    }
    
    public void updateObservableList() {
        observableListSeccion = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_Seccion().getArrayList());
        //observableListMateria = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_Materia().getArrayList());
       // observableListProfesor = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_Profesor().getArrayList());
        observableListBimestre = FXCollections.observableArrayList(ControllerBimestre.getCONTROLLER_Bimestre().getArrayList());
    }
    
    public void updateObservableListMateria(String codigo){
        observableListMateria = FXCollections.observableArrayList(
                ControllerAsignacion.getCONTROLLER_AsignarMateria().getArrayListMateria(codigo));
        comboBoxMateria.setItems(observableListMateria);
    }
    
    public void updateObservableListProfesor(String nombre, String codigo){
        observableListProfesor = FXCollections.observableArrayList(
        ControllerAsignacion.getCONTROLLER_AsignarMateria().getArrayListProfesor(codigo, nombre));
        comboBoxProfesor.setItems(observableListProfesor);
    }    

}

class ModificarActividad {
    private static final ModificarActividad Modificar_Actividades =
            new ModificarActividad();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Seccion> comboBoxSeccion;
    private ComboBox<Materia> comboBoxMateria;
    private ComboBox<Profesor> comboBoxProfesor;
    private ComboBox<Bimestre> comboBoxBimestre;
    private Label labelDescripcion;
    private TextField textFieldDescripcion;
    private Label labelValoNeto;
    private TextField textFieldValorrNeto;
    private Button buttonAgregar;
    private int valor = 1;
    private SpinnerValueFactory svf;
    private  Spinner sp;
    
    private ObservableList<Seccion> observableListSeccion;
    private ObservableList<Materia> observableListMateria;
    private ObservableList<Profesor> observableListProfesor;
    private ObservableList<Bimestre> observableListBimestre;

    private ModificarActividad() {
    }

    public static ModificarActividad getCREATE_Actividades() {
        return Modificar_Actividades;
    }

    public GridPane getGridPane(Actividad actividades) {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        updateObservableList();
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 2);
        comboBoxSeccion = new ComboBox<>(observableListSeccion);
        comboBoxSeccion.getSelectionModel().select(actividades.getAsignacion().getSeccion());
        comboBoxSeccion.setMinSize(140, 20);
        gridPane.add(comboBoxSeccion, 1, 2);
        
        Label labelMateria = new Label("Materia:");
        gridPane.add(labelMateria, 0, 3);
        comboBoxMateria = new ComboBox<>(observableListMateria);
        comboBoxMateria.getSelectionModel().select(actividades.getAsignacion().getMateria());
        comboBoxMateria.setMinSize(140, 20);
        gridPane.add(comboBoxMateria, 1, 3);
        
        Label labelProfesor = new Label("Profesor:");
        gridPane.add(labelProfesor, 0, 4);
        comboBoxProfesor = new ComboBox<>(observableListProfesor);
        comboBoxProfesor.getSelectionModel().select(actividades.getAsignacion().getProfesor());
        comboBoxProfesor.setMinSize(140, 20);
        gridPane.add(comboBoxProfesor, 1, 4);
        
        Label labelBimestre = new Label("Bimestre:");
        gridPane.add(labelBimestre, 0, 5);
        comboBoxBimestre = new ComboBox<>(observableListBimestre);
        comboBoxBimestre.getSelectionModel().select(actividades.getBimestre());
        comboBoxBimestre.setMinSize(140, 20);
        gridPane.add(comboBoxBimestre, 1, 5);
        
        labelDescripcion = new Label("Descripcion:");
        gridPane.add(labelDescripcion, 0, 6);    
        textFieldDescripcion = new TextField(actividades.getDescripcion());
        gridPane.add(textFieldDescripcion, 1, 6);
        try{
            valor = ControllerActividad.getCONTROLLER_Actividad().getValorNetoDos(
                ControllerAsignacion.getCONTROLLER_AsignarMateria().searchOne(
                    actividades.getAsignacion().getSeccion().getCodigo(),
                    actividades.getAsignacion().getMateria().getNombre(),
                    actividades.getAsignacion().getProfesor().getNombre()).getIdAsignacion(), actividades.getValorNeto());
            svf = new SpinnerValueFactory.IntegerSpinnerValueFactory(0, valor);
        sp.setValueFactory(svf);
        } catch (NullPointerException ex){
            
        }
        labelValoNeto = new Label("Valor");
        gridPane.add(labelValoNeto, 0, 7);
        sp = new Spinner();
        sp.setValueFactory(svf);
        sp.setMinSize(140, 20);
        sp.setPrefWidth(80);
        sp.setEditable(true);
        gridPane.add(sp, 1, 7);
        
        buttonAgregar = new Button("Aceptar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(comboBoxSeccion.getSelectionModel() == null ||
                        comboBoxMateria.getSelectionModel() == null ||
                        comboBoxProfesor.getSelectionModel() == null || 
                        textFieldDescripcion.getText().isEmpty() ||
                        sp.getEditor().getText().equals("0") || comboBoxBimestre.getSelectionModel() == null){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if(ControllerActividad.getCONTROLLER_Actividad().buscarExistente(
                        textFieldDescripcion.getText(), ControllerAsignacion.getCONTROLLER_AsignarMateria().searchOne(
                        comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo(),
                        comboBoxMateria.getSelectionModel().getSelectedItem().getNombre(),
                        comboBoxProfesor.getSelectionModel().getSelectedItem().getNombre()).getIdAsignacion(),
                        Integer.parseInt(sp.getEditor().getText())) || comboBoxBimestre.getSelectionModel() == null) {
                    alert.setContentText("La Actividad ya existe");
                    alert.showAndWait();
                } else {
                    ControllerActividad.getCONTROLLER_Actividad().modify(actividades.getIdActividad(),
                            ControllerAsignacion.getCONTROLLER_AsignarMateria().searchOne(
                        comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo(),
                        comboBoxMateria.getSelectionModel().getSelectedItem().getNombre(),
                        comboBoxProfesor.getSelectionModel().getSelectedItem().getNombre()).getIdAsignacion(),
                            comboBoxBimestre.getSelectionModel().getSelectedItem().getIdBimestre(),
                            textFieldDescripcion.getText().trim(), Integer.parseInt(sp.getEditor().getText()));
                    CRUDActividad.getCRUD_Actividad().updateTableViewItems();
                }
                
            }
        });
        gridPane.add(buttonAgregar, 0, 8);
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDActividad.getCRUD_Actividad().restarthBox();
            }
        });
        
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
    
    public void updateObservableList() {
        observableListProfesor = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_Profesor().getArrayList());
        observableListMateria = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_Materia().getArrayList());
        observableListSeccion = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_Seccion().getArrayList());
        observableListBimestre = FXCollections.observableArrayList(ControllerBimestre.getCONTROLLER_Bimestre().getArrayList());
        
    }

}
