/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Alumno;
import org.luisgarcia.bean.Asignacion;
import org.luisgarcia.bean.Materia;
import org.luisgarcia.bean.Profesor;
import org.luisgarcia.bean.Salon;
import org.luisgarcia.bean.Seccion;
import org.luisgarcia.controller.ControllerAlumno;
import org.luisgarcia.controller.ControllerAsignacion;
import org.luisgarcia.controller.ControllerMateria;
import org.luisgarcia.controller.ControllerNotas;
import org.luisgarcia.controller.ControllerProfesor;
import org.luisgarcia.controller.ControllerSalon;
import org.luisgarcia.controller.ControllerSeccion;

/**
 *
 * @author Fernando
 */
public class CRUDAsignacion {
    private static final CRUDAsignacion CRUD_Asignacion = new CRUDAsignacion();
    private HBox hBox;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Asignacion, Integer> tableColumnIdAsignacion;
    private TableColumn<Asignacion, Seccion> tableColumnSeccion;
    private TableColumn<Asignacion, Materia>  tableColumnMateria;
    private TableColumn<Asignacion, Profesor> tableColumnProfesor;
    private TableColumn<Asignacion, Salon> tableColumnSalon;
    private TableView<Asignacion> tableView;
    private ObservableList observableList;

    private CRUDAsignacion() {
    }

    public static CRUDAsignacion getCRUD_Asignacion() {
        return CRUD_Asignacion;
    }
    public HBox gethBox() {
        hBox = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Asignaciones");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar periodo");
        textFieldBuscar.textProperty().addListener((newValue) -> {
            updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateObservableList();
                tableView.setItems(observableList);
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
        buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(CreateAsignacion.getCREATE_LIBRO().getGridPane()));
            }
        });
        
        buttonModificar = new Button("Modificar");

       buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(tableView.getSelectionModel().getSelectedItem() != null){
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(ModificarAsignacion.getModificar_Asignacion()
                        .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                } else {
                    hBox.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.initStyle(StageStyle.UTILITY);
                    alert.showAndWait();
                }
                
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.initStyle(StageStyle.DECORATED);
                        alert.setTitle("Materialogo de Cofirmacion");
                        alert.setHeaderText("Segudo Que Desea Eliminar ");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK){
                            for(Asignacion asignarcion : tableView.getSelectionModel().getSelectedItems()){
                                ControllerAsignacion.getCONTROLLER_AsignarMateria().delete(asignarcion.getIdAsignacion());
                            }
                            restarthBox();
                            updateTableViewItems();

                        } else {
                            }
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Materialgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
        
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdAsignacion = new TableColumn<>("ID");
        tableColumnIdAsignacion.setCellValueFactory(new PropertyValueFactory<>("idAsignacion"));
        tableColumnIdAsignacion.setMinWidth(80);
        
        tableColumnSeccion = new TableColumn<>("Grado");
        tableColumnSeccion.setCellValueFactory(new PropertyValueFactory<>("Seccion"));
        tableColumnSeccion.setMinWidth(100);
        
         tableColumnMateria = new TableColumn<>("Materia");
         tableColumnMateria.setCellValueFactory(new PropertyValueFactory<>("Materia"));
         tableColumnMateria.setMinWidth(120);

        tableColumnProfesor = new TableColumn<>("Profesor");
        tableColumnProfesor.setCellValueFactory(new PropertyValueFactory<>("Profesor"));
        tableColumnProfesor.setMinWidth(100);
        
        tableColumnSalon  = new TableColumn<>("Salon");
        tableColumnSalon.setCellValueFactory(new PropertyValueFactory<>("Salon"));
        tableColumnSalon.setMinWidth(100);
            
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdAsignacion, tableColumnMateria, 
                tableColumnSeccion, tableColumnProfesor, tableColumnSalon);
        tableView.setMinSize(500, 400);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(VerAsignacion.getVer_Asignacion()
                        .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                }
            }
        });
        
        gridPane.add(tableView, 0, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        hBox.getChildren().add(gridPane);
        hBox.setAlignment(Pos.CENTER_LEFT);
        return hBox;
    }
    
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerAsignacion.getCONTROLLER_AsignarMateria().getArrayList());
    }
    
    public void updateTableViewItemsbusqueda(String nombre) {
         observableList = FXCollections.observableArrayList(ControllerAsignacion.getCONTROLLER_AsignarMateria().buscar(nombre));
         tableView.setItems(observableList);
    }
    
    public void restarthBox() {
        hBox.getChildren().clear();
        hBox.getChildren().add(gridPane);
    }
}

class CreateAsignacion {
    private static final CreateAsignacion CREATE_Asignacion =
            new CreateAsignacion();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Seccion> comboBoxSeccion;
    private ComboBox<Materia> comboBoxMateria;
    private ComboBox<Profesor> comboBoxProfesor;
    private ComboBox<Salon> comboBoxSalon;
    private Label labelMateria;
    private TextField textFieldCarnet;
    private Label labelSalon;
    private TextField textFieldNombre;
    private Label labelProfesor;
    private TextField textFieldDireccion;
    private Button buttonAgregar;
    
    private ObservableList<Seccion> observableListSeccion;
    private ObservableList<Materia> observableListMateria;
    private ObservableList<Profesor> observableListProfesor;
    private ObservableList<Salon> observableListSalon;

    private CreateAsignacion() {
    }

    public static CreateAsignacion getCREATE_LIBRO() {
        return CREATE_Asignacion;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        updateObservableList();
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 2);
        comboBoxSeccion = new ComboBox<>(observableListSeccion);
        comboBoxSeccion.setMinSize(140, 20);
        gridPane.add(comboBoxSeccion, 1, 2);
        
        labelMateria = new Label("Materia:");
        gridPane.add(labelMateria, 0, 3);
        comboBoxMateria = new ComboBox<>(observableListMateria);
        comboBoxMateria.setMinSize(140, 20);
        gridPane.add(comboBoxMateria, 1, 3);
        
        labelSalon = new Label("Salon:");
        gridPane.add(labelSalon, 0, 4);
        comboBoxSalon = new ComboBox<>(observableListSalon);
        comboBoxSalon.setMinSize(140, 20);
        gridPane.add(comboBoxSalon, 1, 4);
                
        labelProfesor = new Label("Profesor:");
        gridPane.add(labelProfesor, 0, 5);
        comboBoxProfesor = new ComboBox<>(observableListProfesor);
        comboBoxProfesor.setMinSize(140, 20);
        gridPane.add(comboBoxProfesor, 1, 5);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(comboBoxSeccion.getSelectionModel().isEmpty() || comboBoxProfesor.getSelectionModel().isEmpty() ||
                        comboBoxMateria.getSelectionModel().isEmpty() || comboBoxSalon.getSelectionModel().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if(ControllerAsignacion.getCONTROLLER_AsignarMateria().buscar(
                        comboBoxMateria.getSelectionModel().getSelectedItem().getNombre()
                        , comboBoxProfesor.getSelectionModel().getSelectedItem().getNombre()
                        , comboBoxSalon.getSelectionModel().getSelectedItem().getNombre(), 
                        comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo())) {
                    alert.setContentText("La Asignacion ya existe");
                    alert.showAndWait();
                } else {
                    agregar(comboBoxMateria.getSelectionModel().getSelectedItem(), 
                            comboBoxProfesor.getSelectionModel().getSelectedItem(),
                            comboBoxSalon.getSelectionModel().getSelectedItem(),
                            comboBoxSeccion.getSelectionModel().getSelectedItem());
                    for(Alumno alumno: ControllerAlumno.getCONTROLLER_Alumno().buscar(
                            comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo())){
                        ControllerNotas.getCONTROLLER_Notas().add(alumno.getIdAlumno(),
                                comboBoxMateria.getSelectionModel().getSelectedItem().getIdMateria());
                    }
                    
                }
                
            }
        });
        gridPane.add(buttonAgregar, 0, 8);
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAsignacion.getCRUD_Asignacion().restarthBox();
            }
        });
        
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
    
    private void agregar(Materia materia, Profesor profesor, Salon salon, Seccion seccion) {
        ControllerAsignacion.getCONTROLLER_AsignarMateria().add(seccion.getIdSeccion(),
                materia.getIdMateria(), profesor.getIdProfesor(), salon.getIdSalon());
        CRUDAsignacion.getCRUD_Asignacion().updateTableViewItems();
    }
    
    public void updateObservableList() {
        observableListSeccion = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_Seccion().getArrayList());
        observableListMateria = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_Materia().getArrayList());
        observableListProfesor = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_Profesor().getArrayList());
        observableListSalon = FXCollections.observableArrayList(ControllerSalon.getCONTROLLER_Salon().getArrayList());
    }
    public void updateTableViewItems() {
        updateObservableList();
        comboBoxSeccion.setItems(observableListSeccion);
        comboBoxMateria.setItems(observableListMateria);
        comboBoxSalon.setItems(observableListSalon);
        comboBoxProfesor.setItems(observableListProfesor);
    }

}
class ModificarAsignacion {
    private static final ModificarAsignacion Modificar_Asignacion =
            new ModificarAsignacion();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Seccion> comboBoxSeccion;
    private ComboBox<Materia> comboBoxMateria;
    private ComboBox<Profesor> comboBoxProfesor;
    private ComboBox<Salon> comboBoxSalon;
    private Label labelMateria;
    private TextField textFieldCarnet;
    private Label labelSalon;
    private TextField textFieldNombre;
    private Label labelProfesor;
    private TextField textFieldDireccion;
    private Button buttonAgregar;
    
    private ObservableList<Seccion> observableListSeccion;
    private ObservableList<Materia> observableListMateria;
    private ObservableList<Profesor> observableListProfesor;
    private ObservableList<Salon> observableListSalon;

    private ModificarAsignacion() {
    }

    public static ModificarAsignacion getModificar_Asignacion() {
        return Modificar_Asignacion;
    }

    public GridPane getGridPane(Asignacion asignacion) {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        updateObservableList();
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        textTitle = new Text("Modificar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 2);
        comboBoxSeccion = new ComboBox<>(observableListSeccion);
        comboBoxSeccion.getSelectionModel().select(asignacion.getSeccion().getIdSeccion() -1);
        comboBoxSeccion.setMinSize(140, 20);
        gridPane.add(comboBoxSeccion, 1, 2);
        
        labelMateria = new Label("Materia:");
        gridPane.add(labelMateria, 0, 3);
        comboBoxMateria = new ComboBox<>(observableListMateria);
        comboBoxMateria.getSelectionModel().select(asignacion.getMateria().getIdMateria() -1);
        comboBoxMateria.setMinSize(140, 20);
        gridPane.add(comboBoxMateria, 1, 3);
        
        labelSalon = new Label("Salon:");
        gridPane.add(labelSalon, 0, 4);
        comboBoxSalon = new ComboBox<>(observableListSalon);
        comboBoxSalon.getSelectionModel().select(asignacion.getSalon().getIdSalon() -1);
        comboBoxSalon.setMinSize(140, 20);
        gridPane.add(comboBoxSalon, 1, 4);
                
        labelProfesor = new Label("Profesor:");
        gridPane.add(labelProfesor, 0, 5);
        comboBoxProfesor = new ComboBox<>(observableListProfesor);
        comboBoxProfesor.getSelectionModel().select(asignacion.getProfesor().getIdProfesor() - 1);
        comboBoxProfesor.setMinSize(140, 20);
        gridPane.add(comboBoxProfesor, 1, 5);
        
        buttonAgregar = new Button("Aceptar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(comboBoxSeccion.getSelectionModel().isEmpty() || comboBoxProfesor.getSelectionModel().isEmpty() ||
                        comboBoxMateria.getSelectionModel().isEmpty() || comboBoxSalon.getSelectionModel().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if(ControllerAsignacion.getCONTROLLER_AsignarMateria().buscar(
                        comboBoxMateria.getSelectionModel().getSelectedItem().getNombre()
                        , comboBoxProfesor.getSelectionModel().getSelectedItem().getNombre()
                        , comboBoxSalon.getSelectionModel().getSelectedItem().getNombre(), 
                        comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo())) {
                    alert.setContentText("La Asignacion ya existe");
                    alert.showAndWait();
                } else {
                    ControllerAsignacion.getCONTROLLER_AsignarMateria().modify(asignacion.getIdAsignacion(),
                            comboBoxSeccion.getSelectionModel().getSelectedItem().getIdSeccion(),
                            comboBoxMateria.getSelectionModel().getSelectedItem().getIdMateria(), 
                            comboBoxProfesor.getSelectionModel().getSelectedItem().getIdProfesor(),
                            comboBoxSalon.getSelectionModel().getSelectedItem().getIdSalon());
                            CRUDAsignacion.getCRUD_Asignacion().updateTableViewItems();
                }
                
            }
        });
        gridPane.add(buttonAgregar, 0, 8);
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAsignacion.getCRUD_Asignacion().restarthBox();
            }
        });
        
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
        public void updateObservableList() {
        observableListSeccion = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_Seccion().getArrayList());
        observableListMateria = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_Materia().getArrayList());
        observableListProfesor = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_Profesor().getArrayList());
        observableListSalon = FXCollections.observableArrayList(ControllerSalon.getCONTROLLER_Salon().getArrayList());
    }
    public void updateTableViewItems() {
        updateObservableList();
        comboBoxSeccion.setItems(observableListSeccion);
        comboBoxMateria.setItems(observableListMateria);
        comboBoxSalon.setItems(observableListSalon);
        comboBoxProfesor.setItems(observableListProfesor);
    }
}

class VerAsignacion {
    private static final VerAsignacion Ver_Asignacion = new VerAsignacion();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Seccion> comboBoxSeccion;
    private ComboBox<Materia> comboBoxMateria;
    private ComboBox<Profesor> comboBoxProfesor;
    private ComboBox<Salon> comboBoxSalon;
    private Label labelMateria;
    private TextField textFieldCarnet;
    private Label labelSalon;
    private TextField textFieldNombre;
    private Label labelProfesor;
    private TextField textFieldDireccion;
    private Button buttonAgregar;
    
    private ObservableList<Seccion> observableListSeccion;
    private ObservableList<Materia> observableListMateria;
    private ObservableList<Profesor> observableListProfesor;
    private ObservableList<Salon> observableListSalon;

    private VerAsignacion() {
    }

    public static VerAsignacion getVer_Asignacion() {
        return Ver_Asignacion;
    }

    public GridPane getGridPane(Asignacion asignacion) {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        updateObservableList();
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        textTitle = new Text("Ver");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 2);
        comboBoxSeccion = new ComboBox<>(observableListSeccion);
        comboBoxSeccion.getSelectionModel().select(asignacion.getSeccion().getIdSeccion() -1);
        comboBoxSeccion.setMinSize(140, 20);
        gridPane.add(comboBoxSeccion, 1, 2);
        
        labelMateria = new Label("Materia:");
        gridPane.add(labelMateria, 0, 3);
        comboBoxMateria = new ComboBox<>(observableListMateria);
        comboBoxMateria.getSelectionModel().select(asignacion.getMateria().getIdMateria() -1);
        comboBoxMateria.setMinSize(140, 20);
        gridPane.add(comboBoxMateria, 1, 3);
        
        labelSalon = new Label("Salon:");
        gridPane.add(labelSalon, 0, 4);
        comboBoxSalon = new ComboBox<>(observableListSalon);
        comboBoxSalon.getSelectionModel().select(asignacion.getSalon().getIdSalon() -1);
        comboBoxSalon.setMinSize(140, 20);
        gridPane.add(comboBoxSalon, 1, 4);
                
        labelProfesor = new Label("Profesor:");
        gridPane.add(labelProfesor, 0, 5);
        comboBoxProfesor = new ComboBox<>(observableListProfesor);
        comboBoxProfesor.getSelectionModel().select(asignacion.getProfesor().getIdProfesor() - 1);
        comboBoxProfesor.setMinSize(140, 20);
        gridPane.add(comboBoxProfesor, 1, 5);

        Button buttonCerrar = new Button(" Cerrar ");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAsignacion.getCRUD_Asignacion().restarthBox();
            }
        });
        
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
        public void updateObservableList() {
        observableListSeccion = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_Seccion().getArrayList());
        observableListMateria = FXCollections.observableArrayList(ControllerMateria.getCONTROLLER_Materia().getArrayList());
        observableListProfesor = FXCollections.observableArrayList(ControllerProfesor.getCONTROLLER_Profesor().getArrayList());
        observableListSalon = FXCollections.observableArrayList(ControllerSalon.getCONTROLLER_Salon().getArrayList());
    }

}