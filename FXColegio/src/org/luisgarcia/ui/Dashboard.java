/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 *
 * @author Claudio Canel
 */
public class Dashboard {
    private static final Dashboard DASHBOARD = new Dashboard();
    private Tab tabMateria;
    private Tab tabSalon;
    private Tab tabSeccion;
    private Tab tabProfesor;
    private Tab tabGrado;
    private Tab tabAlumno;
    private Tab tabActividades;
    private Tab tabUsuario;
    private Tab tabAsignacion;
    private Tab tabActividadAlumno;
    private HBox hBox;
    private Text textTitle;
    private TabPane tabPane;
    private Tab tabDashboard; 
    private Button Salon;
    private Button Usuario;
    private Button Materia;
    private boolean vMateria;
    private boolean vUsuario;
    private boolean vProfesor;
    private boolean vSalones;
    private boolean vAlumno;
    private boolean vSeccion;
    private boolean vActividadAlumno;
    private boolean vAsignacion;
    private boolean vActividades;
    private GridPane gridPane;
    
    private Dashboard() {
        tabPane = new TabPane();
        
    }

    public static Dashboard getDASHBOARD() {
        return DASHBOARD;
    }

    public TabPane getTabPane() {
        
        tabDashboard = new Tab("   Inicio   ");
        tabMateria = new Tab("Materia");
        tabUsuario = new Tab("Usuarios");
        tabSalon = new Tab("Salones");
        tabProfesor = new Tab("Profesores");
        tabAlumno = new Tab("Alumnos");
        tabSeccion = new Tab("Secciones");
        tabAsignacion = new Tab("Asignaciones");
        tabActividades = new Tab("Actividades");
        tabActividadAlumno = new Tab("Actividad Alumno");
        
       tabDashboard.setClosable(false);
       hBox = new HBox();
       tabPane.getTabs().clear();
       gridPane = new GridPane();
       gridPane.setHgap(15);
       gridPane.setVgap(15);
      
       gridPane.setPadding(new Insets(25, 25, 25, 25));
       gridPane.setMinSize(504, 600);
       gridPane.getStyleClass().add("gridPane");
       
       tabSeccion.setContent(CRUDSeccion.getCRUD_Seccion().gethBoxCRUD());
       tabUsuario.setContent(CRUDUsuario.getCRUD_Usuario().gethBoxCRUD()); 
       tabSalon.setContent(CRUDSalon.getCRUD_Salon().gethBoxCRUD());
       tabMateria.setContent(CRUDMateria.getCRUD_Materia().gethBoxCRUD());
       tabProfesor.setContent(CRUDProfesor.getCRUD_Profesor().gethBoxCRUD());
       tabAlumno.setContent(CRUDAlumno.getCRUD_Alumno().gethBox());
       tabAsignacion.setContent(CRUDAsignacion.getCRUD_Asignacion().gethBox());
       tabActividades.setContent(CRUDActividad.getCRUD_Actividad().gethBox());
       tabActividadAlumno.setContent(CRUDActividadAlumno.getCRUD_ActividadAlumno().gethBox());
       
       /////////////////////////// Usuarios             
       Usuario = new Button("Usuarios");
       Usuario.setMinSize(140, 40);
       Usuario.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(vUsuario){
                   CRUDUsuario.getCRUD_Usuario().restarthBoxCRUD();
                   tabPane.getTabs().add(tabUsuario);
                   tabPane.getSelectionModel().select(tabUsuario);
                   vUsuario = false;
                } else{
                  tabPane.getSelectionModel().select(tabUsuario);  
                }  
            }
        });
       
            tabUsuario.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               vUsuario = true;
               
            }
        });
       
       gridPane.add(Usuario, 0, 2);
       
       /////////////////////////// ActividadAlumno             
       Button ActividadAlumno = new Button("Actividad Alumno");
       ActividadAlumno.setMinSize(140, 40);
       ActividadAlumno.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(vActividadAlumno){
                   CRUDActividadAlumno.getCRUD_ActividadAlumno().updateObservableListOne();
                   tabPane.getTabs().add(tabActividadAlumno);
                   tabPane.getSelectionModel().select(tabActividadAlumno);
                   vActividadAlumno = false;
                } else{
                  tabPane.getSelectionModel().select(tabActividadAlumno);  
                }  
            }
        });
       
            tabActividadAlumno.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               CRUDActividadAlumno.getCRUD_ActividadAlumno().restarthBox();
               vActividadAlumno = true;
               
            }
        });
       
       gridPane.add(ActividadAlumno, 2, 2);
       
       ////////////////////////////////////////////   Salon
       Salon = new Button("Salones");
       Salon.setMinSize(140, 40);
       Salon.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(vSalones){
                  CRUDSalon.getCRUD_Salon().restarthBoxCRUD();
                  tabPane.getTabs().add(tabSalon);
                  tabPane.getSelectionModel().select(tabSalon);
                  vSalones = false;
                } else{
                  tabPane.getSelectionModel().select(tabSalon);  
                }  
            }
        });
       
            tabSalon.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               vSalones = true;
               
            }
        });
       
        gridPane.add(Salon, 0, 3);
        
        ////////////////////////////////////////// Materia
        
       Materia = new Button("Materia");
       Materia.setMinSize(140, 40);
       Materia.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(vMateria){
                  tabPane.getTabs().add(tabMateria);
                  tabPane.getSelectionModel().select(tabMateria);
                  vMateria = false;
                } else{
                  tabPane.getSelectionModel().select(tabMateria);  
                }  
            }
        });
       
            tabMateria.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               vMateria = true;
               
            }
        });
       
        gridPane.add(Materia, 0, 4);
        
        ///////////////////////////////////  Profesor
       
       Button Profesor = new Button("Profesor");
       Profesor.setMinSize(140, 40);
       Profesor.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                  if(vProfesor){
                    CRUDProfesor.getCRUD_Profesor().restarthBoxCRUD();
                    tabPane.getTabs().add(tabProfesor);
                    tabPane.getSelectionModel().select(tabProfesor);
                    vProfesor = false;
                } else{
                  tabPane.getSelectionModel().select(tabProfesor);  
                }  
            }
        });
       
            tabProfesor.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               vProfesor = true;
               
            }
        });
       
        gridPane.add(Profesor, 0, 5);
        
                ///////////////////////////////////  Alumno
       
       Button Alumno = new Button("Alumno");
       Alumno.setMinSize(140, 40);
       Alumno.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                  if(vAlumno){               
                    CRUDAlumno.getCRUD_Alumno().updateTableViewItems();
                    tabPane.getTabs().add(tabAlumno);
                    tabPane.getSelectionModel().select(tabAlumno);
                    vAlumno = false;
                } else{
                  tabPane.getSelectionModel().select(tabAlumno);  
                }  
            }
        });
       
            tabAlumno.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               vAlumno = true;
               
            }
        });
       
        gridPane.add(Alumno, 0, 6);
        
                        ///////////////////////////////////  Seccion
       
       Button Seccion = new Button("Seccion");
       Seccion.setMinSize(140, 40);
       Seccion.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                  if(vSeccion){
                    CRUDSeccion.getCRUD_Seccion().restarthBoxCRUD();
                    tabPane.getTabs().add(tabSeccion);
                    tabPane.getSelectionModel().select(tabSeccion);
                    vSeccion = false;
                } else{
                  tabPane.getSelectionModel().select(tabSeccion);  
                }  
            }
        });
       
            tabSeccion.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               vSeccion = true;
               
            }
        });
       
        gridPane.add(Seccion, 0, 7);
        
       ///////////////////////////////////  Asignacion  
       Button Asignacion = new Button("Asignacion");
       Asignacion.setMinSize(140, 40);
       Asignacion.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                  if(vAsignacion){
                    CRUDAsignacion.getCRUD_Asignacion().restarthBox();
                    tabPane.getTabs().add(tabAsignacion);
                    tabPane.getSelectionModel().select(tabAsignacion);
                    vAsignacion = false;
                } else{
                  tabPane.getSelectionModel().select(tabAsignacion);  
                }  
            }
        });
       
        tabAsignacion.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               vAsignacion = true;
               
            }
        });
        gridPane.add(Asignacion, 0, 8);     
        
        ///////////////////////////////////  Asignacion  
       Button Actividades = new Button("Actividades");
       Actividades.setMinSize(140, 40);
       Actividades.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                  if(vActividades){
                    CRUDActividad.getCRUD_Actividad().restarthBox();
                    tabPane.getTabs().add(tabActividades);
                    tabPane.getSelectionModel().select(tabActividades);
                    vActividades = false;
                } else{
                  tabPane.getSelectionModel().select(tabActividades);  
                }  
            }
        });
       
        tabActividades.setOnClosed(new EventHandler<Event>() {
            @Override
            public void handle(Event t) {
               vActividades = true;
               
            }
        });
            
        gridPane.add(Actividades, 0, 9);        
        
        hBox.getChildren().addAll(gridPane);
        hBox.setAlignment(Pos.CENTER_RIGHT);
        hBox.setMinSize(500, 600);
        tabDashboard.setContent(hBox);
        tabPane.getTabs().add(tabDashboard);   
        return tabPane;
    }
 
   public void updateTabPane(){
       tabPane.getTabs().clear();
       vUsuario = true;
       vSeccion = true;
       vMateria = true;
       vSalones = true;
       vProfesor = true;
       vAlumno = true;
       vAsignacion = true;
       vActividades = true;
       vActividadAlumno = true;
   }
}
