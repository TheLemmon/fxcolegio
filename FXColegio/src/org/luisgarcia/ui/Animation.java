/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableBooleanValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.Duration;

/**
 *
 * @author Fernando
 */
public class Animation {
        private static final Animation animation = new Animation();
        
        private Animation(){
        }
        
        public static Animation getAnimation(){
            return animation;
        }        
                
        public GridPane getGridPanAnimation(GridPane ingreso){
            GridPane gridPanX = new GridPane();
        
            ColumnConstraints leftCol = new ColumnConstraints();
            leftCol.setPrefWidth(100);
            ColumnConstraints rightCol = new ColumnConstraints();
            rightCol.setPercentWidth(100);
        
            gridPanX.getColumnConstraints().addAll(leftCol, rightCol);

            ObservableBooleanValue expanded = leftCol.prefWidthProperty().greaterThan(0);

                KeyFrame start;
                KeyFrame end;
                
                start = new KeyFrame(Duration.ZERO, new KeyValue(rightCol.prefWidthProperty(), 100));
                end = new KeyFrame(Duration.millis(250), new KeyValue(leftCol.prefWidthProperty(), 0));
                     
                Timeline timeline = new Timeline(start, end);
                timeline.play();

            Node content = ingreso;
            HBox h = new HBox();
            h.setMinSize(150, 600);
            h.setVisible(true);
            h.setAlignment(Pos.TOP_RIGHT);
        
            gridPanX.add(h, 0,0);
            gridPanX.add(content, 1, 0);
        return gridPanX;
        }
             public GridPane getGridPanAnimationReverse(GridPane ingreso){
            GridPane gridPanX = new GridPane();
        
            ColumnConstraints leftCol = new ColumnConstraints();
            leftCol.setPrefWidth(100);
            ColumnConstraints rightCol = new ColumnConstraints();
            rightCol.setPercentWidth(100);
        
            gridPanX.getColumnConstraints().addAll(leftCol, rightCol);

            ObservableBooleanValue expanded = leftCol.prefWidthProperty().greaterThan(0);

                KeyFrame start;
                KeyFrame end;
                
                start = new KeyFrame(Duration.ZERO, new KeyValue(rightCol.prefWidthProperty(), 0));
                end = new KeyFrame(Duration.millis(250), new KeyValue(leftCol.prefWidthProperty(), 400));
                     
                Timeline timeline = new Timeline(start, end);
                timeline.play();

            Node content = ingreso;
            HBox h = new HBox();
            h.setMinSize(150, 600);
            h.setVisible(true);
            h.setAlignment(Pos.TOP_RIGHT);
        
            gridPanX.add(h, 0,0);
            gridPanX.add(content, 1, 0);
        return gridPanX;
        }
}
