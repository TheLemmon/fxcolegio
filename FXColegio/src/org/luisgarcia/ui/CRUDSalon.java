/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Salon;
import org.luisgarcia.controller.ControllerSalon;

/**
 *
 * @author Claudio Canel
 */
public class CRUDSalon {
    private static final CRUDSalon CRUD_Salon = new CRUDSalon();
    private HBox hBoxCRUD;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private Button buttonDashboard;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Salon, Integer> tableColumnIdSalon;
    private TableColumn<Salon, String> tableColumnNombre;
    private TableView<Salon> tableView;
    private ObservableList observableList;

    private CRUDSalon() {
    }

    public static CRUDSalon getCRUD_Salon() {
        return CRUD_Salon;
    }
    
    public void restarthBoxCRUD() {
        hBoxCRUD.getChildren().clear();
        hBoxCRUD.getChildren().add(gridPane);
    }
    
    public void updateTableViewItems() {
         updateObservableList();
         tableView.setItems(observableList);
    }
    
    public void updateTableViewItemsbusqueda(String nombre) {
         observableList = FXCollections.observableArrayList(ControllerSalon.getCONTROLLER_Salon().buscar(nombre));
         tableView.setItems(observableList);
    }

    public HBox gethBoxCRUD() {
        hBoxCRUD = new HBox();
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Salones");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
             
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar salon");
        textFieldBuscar.textProperty().addListener((newValue) -> {
        updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Actualizar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                textFieldBuscar.clear();
                updateTableViewItems();
            }
        });

        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
            buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    hBoxCRUD.getChildren().clear();
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(CreateSalon.getCREATE_Salon().getGridPane()));
            }
        });
        
        buttonModificar = new Button("Modificar");
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBoxCRUD.getChildren().clear();
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(UpdateSalon.getUPDATE_Salon()
                            .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                } else {
                    hBoxCRUD.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.initStyle(StageStyle.UTILITY);
                    alert.showAndWait();
                }
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.initStyle(StageStyle.DECORATED);
                        alert.setTitle("Salonlogo de Cofirmacion");
                        alert.setHeaderText("Segudo Que Desea Eliminar ");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK){
                            for(Salon salon : tableView.getSelectionModel().getSelectedItems()){
                                    ControllerSalon.getCONTROLLER_Salon().delete(salon.getIdSalon());
                                }
                            }
                        restarthBoxCRUD();
                        updateTableViewItems();
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Salonlgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdSalon = new TableColumn<>();
        tableColumnIdSalon.setText("ID");
        tableColumnIdSalon.setCellValueFactory(new PropertyValueFactory<>("idSalon"));
        tableColumnIdSalon.setMinWidth(90);
        
        tableColumnNombre = new TableColumn<>();
        tableColumnNombre.setText("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tableColumnNombre.setMinWidth(260);
        
       updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdSalon, tableColumnNombre);
        tableView.setMinSize(350, 400);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                hBoxCRUD.getChildren().clear();
                hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(VerSalon.getVer_Salon()
                        .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                }
            }
        });

        gridPane.add(tableView, 0, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        hBoxCRUD.getChildren().addAll(gridPane);
        hBoxCRUD.setAlignment(Pos.CENTER_LEFT);
        hBoxCRUD.setMinSize(700, 600);
        return hBoxCRUD;
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerSalon.getCONTROLLER_Salon().getArrayList());
    }
}    
    /////////////   Creacion de Salon ///////////////////////
    
    class CreateSalon {
    private static final CreateSalon CREATE_Salon = new CreateSalon();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonAgregar;

    private CreateSalon() {
    }

    public static CreateSalon getCREATE_Salon() {
        return CREATE_Salon;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        
        textFieldNombre = new TextField();
        textFieldNombre.setPromptText("Nombre");
        gridPane.add(textFieldNombre, 1, 3, 2, 1);
        String dato = textFieldNombre.getText();
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(textFieldNombre.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if (ControllerSalon.getCONTROLLER_Salon().buscarUno(textFieldNombre.getText().trim()) != null) {
                    alert.setContentText("El salon ya existe");
                    alert.showAndWait();
                    
                } else {
                ControllerSalon.getCONTROLLER_Salon().add(textFieldNombre.getText().trim());
                CRUDSalon.getCRUD_Salon().updateTableViewItems();
                textFieldNombre.clear();
                }
                try{
                    CreateAsignacion.getCREATE_LIBRO().updateTableViewItems();
                } catch(NullPointerException eo){
                    
                }
            }
 
        });;
        gridPane.add(buttonAgregar, 0, 5);
        
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDSalon.getCRUD_Salon().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 2, 5);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(450, 400);
        
        return gridPane;
    }
}
        ////////////////////        ACTUALIZACION DE DIA   /////////////////////
    class UpdateSalon {
    private static final UpdateSalon UPDATE_Salon = new UpdateSalon();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;

    private UpdateSalon() {
    }

    public static UpdateSalon getUPDATE_Salon() {
        return UPDATE_Salon;
    }

    public GridPane getGridPane(Salon dia) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Modificar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        
        textFieldNombre = new TextField(dia.getNombre());
        gridPane.add(textFieldNombre, 1, 3, 2, 1);
        
        
        buttonModificar = new Button("Modificar");
        buttonModificar.setDefaultButton(true);
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(textFieldNombre.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if(ControllerSalon.getCONTROLLER_Salon().buscarUno(textFieldNombre.getText().trim()) != null){
                    alert.setContentText("El Salon ya existe");
                    alert.showAndWait();
                } 
                else {
                    ControllerSalon.getCONTROLLER_Salon().modify(dia.getIdSalon(), 
                        textFieldNombre.getText().trim());
                    CRUDSalon.getCRUD_Salon().updateTableViewItems();
                }
                try{
                    CreateAsignacion.getCREATE_LIBRO().updateTableViewItems();
                } catch(NullPointerException eo){
                    
                }
            }
        });
        gridPane.add(buttonModificar, 0, 5);
        
        Button buttonCerrar = new Button("Cerrar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDSalon.getCRUD_Salon().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 2, 5);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(400, 400);
        
        return gridPane;
    }
}

class VerSalon {
    private static final VerSalon Ver_Salon = new VerSalon();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;

    private VerSalon() {
    }

    public static VerSalon getVer_Salon() {
        return Ver_Salon;
    }

    public GridPane getGridPane(Salon salon) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Ver");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        
        textFieldNombre = new TextField(salon.getNombre());
        textFieldNombre.setEditable(false);
        gridPane.add(textFieldNombre, 1, 3, 2, 1);
        
        Button buttonCerrar = new Button("Cerrar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDSalon.getCRUD_Salon().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 2, 5);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(400, 400);
        
        return gridPane;
    }
}