/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Usuario;
import org.luisgarcia.controller.ControllerUsuario;

/**
 *
 * @author Claudio Canel
 */
public class CRUDUsuario {
    private static final CRUDUsuario CRUD_Usuario = new CRUDUsuario();
    private HBox hBoxCRUD;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Usuario, Integer> tableColumnIdUsuario;
    private TableColumn<Usuario, String> tableColumnNombre;
     private TableColumn<Usuario, String> tableColumnContrasenia;
    private TableView<Usuario> tableView;
    private ObservableList observableList;

    private CRUDUsuario() {
    }

    public static synchronized CRUDUsuario getCRUD_Usuario() {
        return CRUD_Usuario;
    }
    
    public void restarthBoxCRUD() {
        hBoxCRUD.getChildren().clear();
        hBoxCRUD.getChildren().add(gridPane);
    }
    public void restarAnimation(GridPane grid){
        hBoxCRUD.getChildren().clear();
        hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimationReverse(grid));
    }
    
    public void updateTableViewItems() {
         updateObservableList();
         tableView.setItems(observableList);
    }
    public void updateTableViewItemsbusqueda(String nombre) {
         observableList = FXCollections.observableArrayList(ControllerUsuario.getCONTROLLER_Usuario().getBuscar(nombre));
         tableView.setItems(observableList);
    }

    public HBox gethBoxCRUD() {
        hBoxCRUD = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        gridPane.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
               restarthBoxCRUD();
               
                }
            }
        });
        
        textTitle = new Text("  Usuarios  ");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar usuario");
        textFieldBuscar.textProperty().addListener((newValue) -> {
            updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Buscar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateTableViewItems();
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
            buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    hBoxCRUD.getChildren().clear();
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation()
                            .getGridPanAnimation(CreateUsuario.getCREATE_Usuario().getGridPane()));
                    tableView.getSelectionModel().clearSelection();
            }
        });
        
        buttonModificar = new Button("Modificar");
        
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBoxCRUD.getChildren().clear();
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(UpdateUsuario.getUPDATE_Usuario()
                            .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                    tableView.getSelectionModel().clearSelection();
                } else {
                    hBoxCRUD.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Usuariolgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.showAndWait();
                }
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.initStyle(StageStyle.DECORATED);
                        alert.setTitle("Usuariologo de Cofirmacion");
                        alert.setHeaderText("Segudo Que Desea Eliminar ");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK){
                            for(Usuario ususario : tableView.getSelectionModel().getSelectedItems()){
                                ControllerUsuario.getCONTROLLER_Usuario().delete(ususario.getIdUsuario());
                                tableView.getSelectionModel().clearSelection();
                            }
                            restarthBoxCRUD();
                            
                            updateTableViewItems();

                        } 
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Usuariolgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdUsuario = new TableColumn<>();
        tableColumnIdUsuario.setText("ID");
        tableColumnIdUsuario.setCellValueFactory(new PropertyValueFactory<>("idUsuario"));
        tableColumnIdUsuario.setMinWidth(80);
        
        tableColumnNombre = new TableColumn<>();
        tableColumnNombre.setText("Nombre");
        tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
        tableColumnNombre.setMinWidth(150);
        
        tableColumnContrasenia = new TableColumn<>();
        tableColumnContrasenia.setText("Contraseña");
        tableColumnContrasenia.setCellValueFactory(new PropertyValueFactory<>("contrasenia"));
        tableColumnContrasenia.setMinWidth(150);
        
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdUsuario, tableColumnNombre, tableColumnContrasenia);
        tableView.setMinSize(380, 400);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                hBoxCRUD.getChildren().clear();
                hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(VerUsuario.getVer_Usuario()
                        .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                }
            }
        });
        
        gridPane.add(tableView, 0, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        hBoxCRUD.getChildren().add(gridPane);
        hBoxCRUD.setAlignment(Pos.CENTER_LEFT);

        return hBoxCRUD;
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerUsuario.getCONTROLLER_Usuario().getArrayList());
    }
    
}    
    /////////////   Creacion de Usuario ///////////////////////
    
    class CreateUsuario {
    private static final CreateUsuario CREATE_Usuario = new CreateUsuario();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonAgregar;
    Alert alert = new Alert(AlertType.INFORMATION);
    

    private CreateUsuario() {
        alert.setTitle("Information");
        alert.setHeaderText(null);
    }

    public static synchronized CreateUsuario getCREATE_Usuario() {
        return CREATE_Usuario;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        
        textFieldNombre = new TextField();
        gridPane.add(textFieldNombre, 1, 3, 3, 1);
        
        labelNombre = new Label("Contraseña: ");
        gridPane.add(labelNombre, 0, 4);
        
        PasswordField passwordFieldContrania = new PasswordField();
        gridPane.add(passwordFieldContrania, 1, 4, 3, 1);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                if(textFieldNombre.getText().trim().isEmpty() || 
                   passwordFieldContrania.getText().trim().isEmpty() ){                    
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
    
                } else {
                   if(!ControllerUsuario.getCONTROLLER_Usuario().buscar(textFieldNombre.getText())){
                        ControllerUsuario.getCONTROLLER_Usuario().add(textFieldNombre.getText(), passwordFieldContrania.getText());
                        CRUDUsuario.getCRUD_Usuario().updateTableViewItems();
                        textFieldNombre.clear();
                        passwordFieldContrania.clear();
                   } else {
                        alert.setContentText("El usuario ya existe");
                        alert.showAndWait();
                   }
                }
            }
 
        });;
        gridPane.add(buttonAgregar, 0, 7);
        
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAlumno.getCRUD_Alumno().restarAnimation(gridPane);
            }
        });
        gridPane.add(buttonCerrar, 1, 7);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(300, 400);
        
        return gridPane;
    }
}
        ////////////////////        ACTUALIZACION DE DIA   /////////////////////
class UpdateUsuario {
    private static final UpdateUsuario UPDATE_Usuario = new UpdateUsuario();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;
    private Alert alert = new Alert(AlertType.INFORMATION);
    
    private UpdateUsuario() {
        alert.setTitle("Information");
        alert.setHeaderText(null);
    }

    public static synchronized UpdateUsuario getUPDATE_Usuario() {
        return UPDATE_Usuario;
    }

    public GridPane getGridPane(Usuario usuario) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Modificar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        
        textFieldNombre = new TextField(usuario.getNombre());
        gridPane.add(textFieldNombre, 1, 3, 3, 1);
        
        labelNombre = new Label("Contraseña: ");
        gridPane.add(labelNombre, 0, 4);
        
        TextField texFieldContrasenia = new TextField(usuario.getContrasenia());
        gridPane.add(texFieldContrasenia, 1, 4, 3,1);
        
        buttonModificar = new Button("Modificar");
        buttonModificar.setDefaultButton(true);
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(textFieldNombre.getText().trim().isEmpty() || 
                   texFieldContrasenia.getText().trim().isEmpty() ){                    
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
    
                } else {
                   if(!ControllerUsuario.getCONTROLLER_Usuario().buscar(textFieldNombre.getText())){
                        ControllerUsuario.getCONTROLLER_Usuario().modify(usuario.getIdUsuario(), 
                        textFieldNombre.getText(), texFieldContrasenia.getText());
                        CRUDUsuario.getCRUD_Usuario().updateTableViewItems();
                   } else {
                        alert.setContentText("El usuario ya existe");
                        alert.showAndWait();
                   }
                }
            }
        });
        
        gridPane.add(buttonModificar, 0, 5);
        
        Button buttonCerrar = new Button("Cerrar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAlumno.getCRUD_Alumno().restarAnimation(gridPane);
            }
        });
        gridPane.add(buttonCerrar, 1, 5);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(400, 400);
        
        return gridPane;
    }
}

class VerUsuario {
    private static final VerUsuario Ver_Usuario = new VerUsuario();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;
    private Alert alert = new Alert(AlertType.INFORMATION);
    
    private VerUsuario() {
    }

    public static VerUsuario getVer_Usuario() {
        return Ver_Usuario;
    }

    public GridPane getGridPane(Usuario usuario) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Ver");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0, 3, 1);
        
        labelNombre = new Label("Nombre: ");
        gridPane.add(labelNombre, 0, 3);
        textFieldNombre = new TextField(usuario.getNombre());
        textFieldNombre.setEditable(false);
        gridPane.add(textFieldNombre, 1, 3, 3, 1);
        
        labelNombre = new Label("Contraseña: ");
        gridPane.add(labelNombre, 0, 4);
        TextField texFieldContrasenia = new TextField(usuario.getContrasenia());
        texFieldContrasenia.setEditable(false);
        gridPane.add(texFieldContrasenia, 1, 4, 3,1);
        
        Button buttonCerrar = new Button("Cerrar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAlumno.getCRUD_Alumno().restarAnimation(gridPane);
            }
        });
        gridPane.add(buttonCerrar, 1, 5);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(400, 400);
        
        return gridPane;
    }
}