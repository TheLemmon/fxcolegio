 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.HashMap;
import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Alumno;
import org.luisgarcia.bean.Asignacion;
import org.luisgarcia.bean.Seccion;
import org.luisgarcia.controller.ControllerActividad;
import org.luisgarcia.controller.ControllerActividadAlumno;
import org.luisgarcia.controller.ControllerAlumno;
import org.luisgarcia.controller.ControllerAsignacion;
import org.luisgarcia.controller.ControllerNotas;
import org.luisgarcia.controller.ControllerSeccion;
import org.luisgarcia.util.ReportGenerator;

/**
 *
 * @author Claudio Canel
 */
public class CRUDAlumno {
    private static final CRUDAlumno CRUD_Alumno = new CRUDAlumno();
    private HBox hBox;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    private Button buttonReporte;
    
    private TableColumn<Alumno, Integer> tableColumnIdAlumno;
    private TableColumn<Alumno, Seccion> tableColumnSeccion;
    private TableColumn<Alumno, String>  tableColumnNombre;
    private TableColumn<Alumno, String> tableColumnDireccion;
    private TableColumn<Alumno, String> tableColumnTelefono;
    private TableColumn<Alumno, String> tableColumnCarnet;
    private TableView<Alumno> tableView;
    private ObservableList observableList;

    private CRUDAlumno() {
    }

    public static CRUDAlumno getCRUD_Alumno() {
        return CRUD_Alumno;
    }

    public HBox gethBox() {
        hBox = new HBox();
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Alumnos");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
        
        gridPane.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 1) {
               restarthBox();
               
                }
            }
        });
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar Alumno");
        textFieldBuscar.textProperty().addListener((newValue) -> {
            updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Actualizar");
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateObservableList();
                tableView.setItems(observableList);
            }
        });
        
        hBoxBuscar.getChildren().addAll(textFieldBuscar, buttonBuscar);
        gridPane.add(hBoxBuscar, 0, 1);
        
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
        buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, Animation.getAnimation().
                        getGridPanAnimation(CreateAlumno.getCREATE_LIBRO().getGridPane()));
                tableView.getSelectionModel().clearSelection();
            }
        });
        
        buttonModificar = new Button("Modificar");

        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if(tableView.getSelectionModel().getSelectedItem() != null){
                    hBox.getChildren().clear();
                    hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(ModificarAlumno.getModificar_Alumno()
                        .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                    tableView.getSelectionModel().clearSelection();
                } else {
                    hBox.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.initStyle(StageStyle.UTILITY);
                    alert.showAndWait();
                }
                
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                        Alert alert = new Alert(AlertType.CONFIRMATION);
                        alert.initStyle(StageStyle.DECORATED);
                        alert.setTitle("Materialogo de Cofirmacion");
                        alert.setHeaderText("Segudo Que Desea Eliminar ");

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.get() == ButtonType.OK){
                            for(Alumno alumno : tableView.getSelectionModel().getSelectedItems()){
                                ControllerAlumno.getCONTROLLER_Alumno().delete(alumno.getIdAlumno());
                                tableView.getSelectionModel().clearSelection();
                            }
                            restarthBox();
                            updateTableViewItems();
                        } 
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Materialgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
        buttonReporte = new Button("Notas");
        buttonReporte.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                 if (tableView.getSelectionModel().getSelectedItem() != null) {
                    HashMap<String, String> hashMap = new HashMap();
                    hashMap.put("Alumno", tableView.getSelectionModel().getSelectedItem().getNombre());
                    ReportGenerator.getREPORT_GENERATOR().generate(hashMap, "Notas.jasper", "Reporte de Notas");
                }
            }
        });
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar, buttonReporte);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdAlumno = new TableColumn<>("ID");
        tableColumnIdAlumno.setCellValueFactory(new PropertyValueFactory<>("idAlumno"));
        tableColumnIdAlumno.setMinWidth(80);
        
        tableColumnSeccion = new TableColumn<>("Grado");
        tableColumnSeccion.setCellValueFactory(new PropertyValueFactory<>("Seccion"));
        tableColumnSeccion.setMinWidth(100);
        
         tableColumnNombre = new TableColumn<>("Nombre");
         tableColumnNombre.setCellValueFactory(new PropertyValueFactory<>("nombre"));
         tableColumnNombre.setMinWidth(120);

        tableColumnDireccion = new TableColumn<>("Direccion");
        tableColumnDireccion.setCellValueFactory(new PropertyValueFactory<>("direccion"));
        tableColumnDireccion.setMinWidth(100);
        
        tableColumnTelefono  = new TableColumn<>("Telefono");
        tableColumnTelefono.setCellValueFactory(new PropertyValueFactory<>("telefono"));
        tableColumnTelefono.setMinWidth(100);
        
        
        tableColumnCarnet = new TableColumn<>("Carnet");
        tableColumnCarnet.setCellValueFactory(new PropertyValueFactory<>("carnet"));
        tableColumnCarnet.setMinWidth(100);
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                hBox.getChildren().clear();
                hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(VerAlumno.getVer_Alumno()
                        .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                }
            }
        });
        tableView.getColumns().addAll(tableColumnIdAlumno, tableColumnCarnet,
                tableColumnNombre, tableColumnSeccion, tableColumnDireccion, 
                tableColumnTelefono);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        tableView.setMinSize(600, 400);
        gridPane.add(tableView, 0, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        hBox.getChildren().add(gridPane);
        hBox.setAlignment(Pos.CENTER_LEFT);
        return hBox;
    }
    
    public void updateTableViewItems() {
        updateObservableList();
        tableView.setItems(observableList);
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerAlumno.getCONTROLLER_Alumno().getArrayList());
    }
    public void updateTableViewItemsbusqueda(String nombre) {
         observableList = FXCollections.observableArrayList(ControllerAlumno.getCONTROLLER_Alumno().buscar(nombre));
         tableView.setItems(observableList);
    }
    
    public void restarthBox() {
        hBox.getChildren().clear();
        hBox.getChildren().add(gridPane);
    }
    
    public void restarAnimation(GridPane grid){
        hBox.getChildren().clear();
        hBox.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimationReverse(grid));
    }
}
class CreateAlumno {
    private static final CreateAlumno CREATE_Alumno =
            new CreateAlumno();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Seccion> comboBoxSeccion;
    private Label labelCarnet;
    private TextField textFieldCarnet;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelDireccion;
    private TextField textFieldDireccion;
    private Button buttonAgregar;
    
    private ObservableList<Seccion> observableListSeccion;

    private CreateAlumno() {
    }

    public static CreateAlumno getCREATE_LIBRO() {
        return CREATE_Alumno;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        
        textTitle = new Text("Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0, 3, 1);
        
        labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 1);
        
        updateObservableListSeccion();
        comboBoxSeccion = new ComboBox<>(observableListSeccion);
        comboBoxSeccion.setMinSize(140, 20);
        gridPane.add(comboBoxSeccion, 1, 1, 2, 1);
  
        labelCarnet = new Label("Carnet:");
        gridPane.add(labelCarnet, 0, 3);
        
        textFieldCarnet = new TextField();
        gridPane.add(textFieldCarnet, 1, 3);
        
        labelNombre = new Label("Nombre:");
        gridPane.add(labelNombre, 0, 4);
        
        textFieldNombre = new TextField();
        gridPane.add(textFieldNombre, 1, 4);
        
        labelDireccion = new Label("Direccion:");
        gridPane.add(labelDireccion, 0, 5);
        
        textFieldDireccion = new TextField();
        gridPane.add(textFieldDireccion, 1, 5);
        
        Label labelTelefono = new Label("Telefono:");
        gridPane.add(labelTelefono, 0, 6);
        
        TextField textFieldTelefono = new TextField();
        gridPane.add(textFieldTelefono, 1, 6);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(comboBoxSeccion.getSelectionModel().isEmpty() || textFieldCarnet.getText().trim().isEmpty() ||
                        textFieldNombre.getText().trim().isEmpty() || textFieldDireccion.getText().trim().isEmpty()|| 
                        textFieldTelefono.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if (ControllerAlumno.getCONTROLLER_Alumno().buscarExistente(textFieldNombre.getText(),
                        textFieldCarnet.getText())){
                             alert.setContentText("El alumno ya existe");
                             alert.showAndWait();
                } else {
                    agregar(comboBoxSeccion.getSelectionModel().getSelectedItem(), 
                        textFieldCarnet.getText().trim(), textFieldNombre.getText().trim(), 
                        textFieldDireccion.getText().trim(), textFieldTelefono.getText().trim());
                        
                    for(Asignacion asignacion : ControllerAsignacion.getCONTROLLER_AsignarMateria().buscar(
                            comboBoxSeccion.getSelectionModel().getSelectedItem().getCodigo())){
                        ControllerNotas.getCONTROLLER_Notas().add(ControllerAlumno
                                .getCONTROLLER_Alumno().buscarUno(textFieldNombre.getText().trim()).getIdAlumno(),
                            asignacion.getMateria().getIdMateria());      
                        
                        ControllerActividadAlumno.getCONTROLLER_ActividadAlumno().add(
                                ControllerActividad.getCONTROLLER_Actividad().getInt(
                                    asignacion.getSeccion().getIdSeccion(), 
                                    asignacion.getProfesor().getIdProfesor(),
                                    asignacion.getProfesor().getIdProfesor()),
                                ControllerAlumno
                                .getCONTROLLER_Alumno().buscarUno(textFieldNombre.getText().trim()).getIdAlumno(),
                                0, ( 100 - ControllerActividad.getCONTROLLER_Actividad().getValorNetoDos(asignacion.getIdAsignacion(), 0)));
                        
                    }
                textFieldCarnet.clear(); textFieldNombre.clear(); textFieldDireccion.clear();
                        textFieldTelefono.clear();
                        
                }
                
            }
        });
        gridPane.add(buttonAgregar, 0, 8);
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAlumno.getCRUD_Alumno().restarAnimation(gridPane);
            }
        });
        
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
    
    private void agregar(Seccion seccion, String carnet, String nombre, String direccion, String telefono) {
        ControllerAlumno.getCONTROLLER_Alumno().add(seccion.getIdSeccion(),
                nombre.trim(), direccion.trim(), telefono.trim(), carnet.trim());
        CRUDAlumno.getCRUD_Alumno().updateTableViewItems();
    }
    
    public void updateObservableListSeccion() {
        observableListSeccion = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_Seccion().getArrayList());
    }
    
    public ObservableList getget(){
        return observableListSeccion;
    }
    
    public void updateTableViewItems() {
        updateObservableListSeccion();
        comboBoxSeccion.setItems(observableListSeccion);
    }

}

class ModificarAlumno {
    private static final ModificarAlumno Modificar_Alumno =
            new ModificarAlumno();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Seccion> comboBoxSeccion;
    private Label labelCarnet;
    private TextField textFieldCarnet;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelDireccion;
    private TextField textFieldDireccion;
    private Button buttonAgregar;
    
    private ObservableList<Seccion> observableListSeccion;

    private ModificarAlumno() {
    }

    public static ModificarAlumno getModificar_Alumno() {
        return Modificar_Alumno;
    }
    
    public GridPane getGridPane(Alumno alumno) {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        
        textTitle = new Text("Modificar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0, 3, 1);
        
        labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 1);
        
        CreateAlumno.getCREATE_LIBRO().updateObservableListSeccion();
        comboBoxSeccion = new ComboBox<>(CreateAlumno.getCREATE_LIBRO().getget());
        comboBoxSeccion.getSelectionModel().select(alumno.getSeccion().getIdSeccion() - 1);
        comboBoxSeccion.setMinSize(140, 20);
        gridPane.add(comboBoxSeccion, 1, 1);
        
        labelCarnet = new Label("Carnet:");
        gridPane.add(labelCarnet, 0, 3);
        
        textFieldCarnet = new TextField(alumno.getCarnet());
        gridPane.add(textFieldCarnet, 1, 3);
        
        labelNombre = new Label("Nombre:");
        gridPane.add(labelNombre, 0, 4);
        
        textFieldNombre = new TextField(alumno.getNombre());
        gridPane.add(textFieldNombre, 1, 4);
        
        labelDireccion = new Label("Direccion:");
        gridPane.add(labelDireccion, 0, 5);
        
        textFieldDireccion = new TextField(alumno.getDireccion());
        gridPane.add(textFieldDireccion, 1, 5);
        
        Label labelTelefono = new Label("Telefono:");
        gridPane.add(labelTelefono, 0, 6);
        
        TextField textFieldTelefono = new TextField(alumno.getTelefono());
        gridPane.add(textFieldTelefono, 1, 6);
        
        Button buttonAceptar = new Button("Aceptar");
        buttonAceptar.setDefaultButton(true);
        buttonAceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(comboBoxSeccion.getSelectionModel().isEmpty() || textFieldCarnet.getText().trim().isEmpty() ||
                        textFieldNombre.getText().trim().isEmpty() || textFieldDireccion.getText().trim().isEmpty()|| 
                        textFieldTelefono.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                } else if (ControllerAlumno.getCONTROLLER_Alumno().buscarExistente(textFieldNombre.getText(),
                        textFieldCarnet.getText())){
                             alert.setContentText("El alumno ya existe");
                             alert.showAndWait();
                } else {
                    ControllerAlumno.getCONTROLLER_Alumno().modify(alumno.getIdAlumno(), 
                    comboBoxSeccion.getSelectionModel().getSelectedItem().getIdSeccion(), 
                    textFieldCarnet.getText(), textFieldNombre.getText(), 
                    textFieldDireccion.getText(), textFieldTelefono.getText()); 
                    CRUDAlumno.getCRUD_Alumno().updateTableViewItems();
                }
            }
        });
        gridPane.add(buttonAceptar, 0, 8);
       
        Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAlumno.getCRUD_Alumno().restarAnimation(gridPane);
                
            }
        });
        gridPane.add(buttonCerrar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
         gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
    }
}

class VerAlumno {
    private static final VerAlumno Ver_Alumno =
            new VerAlumno();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelSeccion;
    private ComboBox<Seccion> comboBoxSeccion;
    private Label labelCarnet;
    private TextField textFieldCarnet;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelDireccion;
    private TextField textFieldDireccion;
    private Button buttonAgregar;
    
    private ObservableList<Seccion> observableListSeccion;

    private VerAlumno() {
    }

    public static VerAlumno getVer_Alumno() {
        return Ver_Alumno;
    }
    
    public GridPane getGridPane(Alumno alumno) {
        gridPane = new GridPane();
        gridPane.setHgap(15);
        gridPane.setVgap(15);
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        
        textTitle = new Text("Ver");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        labelSeccion = new Label("Grado:");
        gridPane.add(labelSeccion, 0, 1);
        
        CreateAlumno.getCREATE_LIBRO().updateObservableListSeccion();
        comboBoxSeccion = new ComboBox<>(CreateAlumno.getCREATE_LIBRO().getget());
        comboBoxSeccion.getSelectionModel().select(alumno.getSeccion().getIdSeccion() - 1);
        comboBoxSeccion.setMinSize(140, 20);
        gridPane.add(comboBoxSeccion, 1, 1);
        
        labelCarnet = new Label("Carnet:");
        gridPane.add(labelCarnet, 0, 3);
        
        textFieldCarnet = new TextField(alumno.getCarnet());
        gridPane.add(textFieldCarnet, 1, 3);
        textFieldCarnet.setEditable(false);
        
        labelNombre = new Label("Nombre:");
        gridPane.add(labelNombre, 0, 4);
        
        textFieldNombre = new TextField(alumno.getNombre());
        gridPane.add(textFieldNombre, 1, 4);
        textFieldNombre.setEditable(false);
        
        labelDireccion = new Label("Direccion:");
        gridPane.add(labelDireccion, 0, 5);
        
        textFieldDireccion = new TextField(alumno.getDireccion());
        gridPane.add(textFieldDireccion, 1, 5);
        textFieldDireccion.setEditable(false);
        
        Label labelTelefono = new Label("Telefono:");
        gridPane.add(labelTelefono, 0, 6);
        
        TextField textFieldTelefono = new TextField(alumno.getTelefono());
        gridPane.add(textFieldTelefono, 1, 6);
        textFieldTelefono.setEditable(false);
        
        Button buttonAceptar = new Button("Aceptar");
        buttonAceptar.setDefaultButton(true);
        buttonAceptar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDAlumno.getCRUD_Alumno().restarAnimation(gridPane);
            }
        });
        gridPane.add(buttonAceptar, 1, 8);
        gridPane.getStyleClass().add("gridPane");
         gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(480, 400);
        return gridPane;
}
}


