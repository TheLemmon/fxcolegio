/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.ui;

import java.util.Optional;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.StageStyle;
import org.luisgarcia.bean.Seccion;
import org.luisgarcia.controller.ControllerSeccion;

/**
 *
 * @author Claudio Canel
 */
public class CRUDSeccion {
    private static final CRUDSeccion CRUD_Seccion = new CRUDSeccion();
    private HBox hBoxCRUD;
    private GridPane gridPane;
    private Text textTitle;
    private HBox hBoxBuscar;
    private TextField textFieldBuscar;
    private Button buttonBuscar;
    private HBox hBoxButtons;
    private Button buttonNuevo;
    private Button buttonModificar;
    private Button buttonEliminar;
    
    private TableColumn<Seccion, Integer> tableColumnIdSeccion;
    private TableColumn<Seccion, String> tableColumnGrado;
    private TableColumn<Seccion, String> tableColumnSeccion;
    private TableColumn<Seccion, String> tableColumnJornada;
    private TableColumn<Seccion, String> tableColumnCodigo;
    private TableView<Seccion> tableView;
    private ObservableList observableList;

    private CRUDSeccion() {
	
    }

    public static CRUDSeccion getCRUD_Seccion() {
        return CRUD_Seccion;
    }
    
    public void restarthBoxCRUD() {
        hBoxCRUD.getChildren().clear();
        hBoxCRUD.getChildren().add(gridPane);
    }
    
    public void updateTableViewItems() {
         updateObservableList();
         tableView.setItems(observableList);
    }
    public void updateTableViewItemsbusqueda(String nombre) {
         observableList = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_Seccion().buscar(nombre));
         tableView.setItems(observableList);
    }

    public HBox gethBoxCRUD() {
        hBoxCRUD = new HBox();
        
        gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        textTitle = new Text("Secciones");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 0, 0);
        
        hBoxBuscar = new HBox(10);
        
        textFieldBuscar = new TextField();
        textFieldBuscar.setPromptText("Buscar grado");
        textFieldBuscar.textProperty().addListener((newValue) -> {
            updateTableViewItemsbusqueda(textFieldBuscar.getText().trim());
        });
        
        buttonBuscar = new Button("Actualizar");
        buttonBuscar.setDefaultButton(true);
        buttonBuscar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                updateTableViewItems();
            }  
        });

        hBoxBuscar.getChildren().addAll(textFieldBuscar,  buttonBuscar);
        hBoxBuscar.setId("hBoxBuscar");
        gridPane.add(hBoxBuscar, 0, 1);
        hBoxButtons = new HBox(10);
        
        buttonNuevo = new Button("Nuevo");
            buttonNuevo.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    hBoxCRUD.getChildren().clear();
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(CreateSeccion.getCREATE_Seccion().getGridPane()));
            }
        });
        
        buttonModificar = new Button("Modificar");
        
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                hBoxCRUD.getChildren().clear();
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().
                            getGridPanAnimation(UpdateSeccion.getUPDATE_Seccion()
                            .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                } else {
                    hBoxCRUD.getChildren().add(gridPane);
                    Alert alert = new Alert(AlertType.INFORMATION);
                    alert.setTitle("Information");
                    alert.setHeaderText("No Se Selecciono un Elemento para Modificar");
                    alert.initStyle(StageStyle.UTILITY);
                    alert.showAndWait();
                }
            }
        });
        
        buttonEliminar = new Button("Eliminar");
        buttonEliminar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (tableView.getSelectionModel().getSelectedItem() != null) {
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.initStyle(StageStyle.DECORATED);
                    alert.setTitle("Gradologo de Cofirmacion");
                    alert.setHeaderText("Segudo Que Desea Eliminar ");

                    Optional<ButtonType> result = alert.showAndWait();
                        
                    if (result.get() == ButtonType.OK){
                        for(Seccion seccion : tableView.getSelectionModel().getSelectedItems()){
                            ControllerSeccion.getCONTROLLER_Seccion().delete(seccion.getIdSeccion());
                        }
                        restarthBoxCRUD();
                        updateTableViewItems();
                }
                    
                } else {
                    Alert alert = new Alert(AlertType.WARNING);
                    alert.setTitle("Gradolgo de Error");
                    alert.setHeaderText("No Se Selecciono un Elemento para Eliminar");
                    alert.showAndWait();
                }
            }
        });
        
        hBoxButtons.getChildren().addAll(buttonNuevo, buttonModificar, 
                buttonEliminar);
        gridPane.add(hBoxButtons, 0, 2);
        
        tableColumnIdSeccion = new TableColumn<>();
        tableColumnIdSeccion.setText("ID");
        tableColumnIdSeccion.setCellValueFactory(new PropertyValueFactory<>("idSeccion"));
        tableColumnIdSeccion.setMinWidth(80);
        
        tableColumnGrado = new TableColumn<>();
        tableColumnGrado.setText("Grado");
        tableColumnGrado.setCellValueFactory(new PropertyValueFactory<>("grado"));
        tableColumnGrado.setMinWidth(150);
        
        tableColumnSeccion = new TableColumn<>();
        tableColumnSeccion.setText("Seccion");
        tableColumnSeccion.setCellValueFactory(new PropertyValueFactory<>("seccion"));
        tableColumnSeccion.setMinWidth(150);
        
        tableColumnJornada = new TableColumn<>();
        tableColumnJornada.setText("Jornada");
        tableColumnJornada.setCellValueFactory(new PropertyValueFactory<>("jornada"));
        tableColumnJornada.setMinWidth(100);
        
               tableColumnCodigo = new TableColumn<>();
        tableColumnCodigo.setText("Codigo");
        tableColumnCodigo.setCellValueFactory(new PropertyValueFactory<>("codigo"));
        tableColumnCodigo.setMinWidth(100);
        
        updateObservableList();
        tableView = new TableView<>(observableList);
        tableView.getColumns().addAll(tableColumnIdSeccion, tableColumnGrado,tableColumnSeccion,
                tableColumnJornada,tableColumnCodigo);
        tableView.setMinSize(580, 400);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override 
            public void handle(MouseEvent event) {
                if (event.isPrimaryButtonDown() && event.getClickCount() == 2) {
                hBoxCRUD.getChildren().clear();
                hBoxCRUD.getChildren().addAll(gridPane, Animation.getAnimation().getGridPanAnimation(VerSeccion.getVer_Seccion()
                        .getGridPane(tableView.getSelectionModel().getSelectedItem())));
                }
            }
        });
        
        gridPane.add(tableView, 0, 3, 2, 1);
        gridPane.setMinSize(700, 600);
        hBoxCRUD.getChildren().add(gridPane);
        hBoxCRUD.setAlignment(Pos.CENTER_LEFT);
        
        
        return hBoxCRUD;
    }
    
    private void updateObservableList() {
        observableList = FXCollections.observableArrayList(ControllerSeccion.getCONTROLLER_Seccion().getArrayList());
    }
}    
    /////////////   Creacion de Seccion ///////////////////////
    
    class CreateSeccion {
    private static final CreateSeccion CREATE_Grado = new CreateSeccion();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonAgregar;

    private CreateSeccion() {
    }

    public static CreateSeccion getCREATE_Seccion() {
        return CREATE_Grado;
    }

    public GridPane getGridPane() {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(30, 30, 30, 30));
        
        textTitle = new Text(" Agregar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 3, 1);
        
        Label nombre = new Label("Grado");
        gridPane.add(nombre, 0, 4);
        textFieldNombre = new TextField();
        gridPane.add(textFieldNombre, 1, 4, 2, 1);
        
        Label Seccion = new Label("Seccion");
        gridPane.add(Seccion, 0, 5);
        TextField textFieldSeccion = new TextField();
        gridPane.add(textFieldSeccion, 1, 5, 2, 1);
        
        Label Jornada = new Label("Jornada");
        gridPane.add(Jornada, 0, 6);
        TextField textFieldJornada = new TextField();
        gridPane.add(textFieldJornada, 1, 6, 2, 1);
        
        Label Codigo = new Label("Codigo");
        gridPane.add(Codigo, 0, 7);
        TextField textFieldCodigo = new TextField();
        gridPane.add(textFieldCodigo, 1, 7, 2, 1);
        
        buttonAgregar = new Button("Agregar");
        buttonAgregar.setDefaultButton(true);
        buttonAgregar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(textFieldNombre.getText().trim().isEmpty() || textFieldSeccion.getText().trim().isEmpty() || 
                        textFieldJornada.getText().trim().isEmpty() || textFieldCodigo.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                   
                } else if(ControllerSeccion.getCONTROLLER_Seccion().getSeccion(textFieldNombre.getText().trim(), 
                        textFieldSeccion.getText().trim(), textFieldJornada.getText().trim(), 
                        textFieldCodigo.getText().trim()) != null){
                        alert.setContentText("La seccion ya existe");
                        alert.showAndWait();
                } else {
                ControllerSeccion.getCONTROLLER_Seccion().add(textFieldNombre.getText().trim(), 
                        textFieldSeccion.getText().trim(), textFieldJornada.getText().trim(), 
                        textFieldCodigo.getText().trim());
                        CRUDSeccion.getCRUD_Seccion().updateTableViewItems();
                        textFieldNombre.clear();
                        textFieldSeccion.clear();
                        textFieldJornada.clear();
                        textFieldCodigo.clear();
                }
                try{
                    CreateAlumno.getCREATE_LIBRO().updateTableViewItems();
                    CreateAsignacion.getCREATE_LIBRO().updateTableViewItems();
                    CRUDActividadAlumno.getCRUD_ActividadAlumno().updateOne();
                } catch(NullPointerException eo){
                    
                }
            
            }
            
        });;
        gridPane.add(buttonAgregar, 1, 9);
        
         Button buttonCerrar = new Button("Cancelar");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDSeccion.getCRUD_Seccion().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 2, 9);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setMinSize(450, 400);
        
        return gridPane;
    }
}
        ////////////////////        ACTUALIZACION DE DIA   /////////////////////
class UpdateSeccion {
    private static final UpdateSeccion UPDATE_Seccion = new UpdateSeccion();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;

    private UpdateSeccion() {
    }

    public static UpdateSeccion getUPDATE_Seccion() {
        return UPDATE_Seccion;
    }

    public GridPane getGridPane(Seccion dia) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Modificar");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 2, 1);
                
        Label nombre = new Label("Grado");
        gridPane.add(nombre, 0, 4);
        textFieldNombre = new TextField(dia.getGrado());
        gridPane.add(textFieldNombre, 1, 4, 2, 1);
        
        Label Seccion = new Label("Seccion");
        gridPane.add(Seccion, 0, 5);
        TextField textFieldSeccion = new TextField(dia.getSeccion());
        gridPane.add(textFieldSeccion, 1, 5, 2, 1);
        
        Label Jornada = new Label("Jornada");
        gridPane.add(Jornada, 0, 6);
        TextField textFieldJornada = new TextField(dia.getJornada());
        gridPane.add(textFieldJornada, 1, 6, 2, 1);
        
        Label Codigo = new Label("Codigo");
        gridPane.add(Codigo, 0, 7);
        TextField textFieldCodigo = new TextField(dia.getCodigo());
        gridPane.add(textFieldCodigo, 1, 7, 2, 1);
        
        buttonModificar = new Button("Modificar");
        buttonModificar.setDefaultButton(true);
        buttonModificar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Information");
                alert.setHeaderText(null);
                if(textFieldNombre.getText().trim().isEmpty() || textFieldSeccion.getText().trim().isEmpty() || 
                        textFieldJornada.getText().trim().isEmpty() || textFieldCodigo.getText().trim().isEmpty()){
                    alert.setContentText("No se ingresaron todos los datos");
                    alert.showAndWait();
                        
                } else if(ControllerSeccion.getCONTROLLER_Seccion().getSeccion(textFieldNombre.getText().trim(), 
                        textFieldSeccion.getText().trim(), textFieldJornada.getText().trim(), 
                        textFieldCodigo.getText().trim()) != null){
                        alert.setContentText("La seccion ya existe");
                        alert.showAndWait();
                } else {
                    ControllerSeccion.getCONTROLLER_Seccion().modify(dia.getIdSeccion(), 
                    textFieldNombre.getText(), textFieldSeccion.getText(), 
                    textFieldJornada.getText(), textFieldCodigo.getText());
                    CRUDSeccion.getCRUD_Seccion().updateTableViewItems();
                }
                try{
                    CreateAlumno.getCREATE_LIBRO().updateTableViewItems();
                    CreateAsignacion.getCREATE_LIBRO().updateTableViewItems();
                } catch(NullPointerException eo){
                    
                }
            }
        });
        gridPane.add(buttonModificar, 1, 9);
        
        Button buttonCerrar = new Button("  Cerrar  ");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDSeccion.getCRUD_Seccion().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 2, 9);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(400, 400);
        
        return gridPane;
    }
}

class VerSeccion {
    private static final VerSeccion Ver_Seccion = new VerSeccion();
    private GridPane gridPane;
    private Text textTitle;
    private Label labelActivo;
    private CheckBox checkBoxActivo;
    private Label labelNombre;
    private TextField textFieldNombre;
    private Label labelClave;
    private PasswordField passwordFieldClave;
    private Button buttonModificar;

    private VerSeccion() {
    }

    public static VerSeccion getVer_Seccion() {
        return Ver_Seccion;
    }

    public GridPane getGridPane(Seccion dia) {
        gridPane = new GridPane();
        gridPane.setVgap(10);
        gridPane.setHgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        
        textTitle = new Text("Ver");
        textTitle.setFont(Font.font("Verdana", FontWeight.BOLD, 20));
        gridPane.add(textTitle, 1, 0, 2, 1);
                
        Label nombre = new Label("Grado");
        gridPane.add(nombre, 0, 4);
        textFieldNombre = new TextField(dia.getGrado());
        textFieldNombre.setEditable(false);
        gridPane.add(textFieldNombre, 1, 4, 2, 1);
        
        Label Seccion = new Label("Seccion");
        gridPane.add(Seccion, 0, 5);
        TextField textFieldSeccion = new TextField(dia.getSeccion());
        textFieldSeccion.setEditable(false);
        gridPane.add(textFieldSeccion, 1, 5, 2, 1);
        
        Label Jornada = new Label("Jornada");
        gridPane.add(Jornada, 0, 6);
        TextField textFieldJornada = new TextField(dia.getJornada());
        textFieldJornada.setEditable(false);
        gridPane.add(textFieldJornada, 1, 6, 2, 1);
        
        Label Codigo = new Label("Codigo");
        gridPane.add(Codigo, 0, 7);
        TextField textFieldCodigo = new TextField(dia.getCodigo());
        textFieldCodigo.setEditable(false);
        gridPane.add(textFieldCodigo, 1, 7, 2, 1);
             
        Button buttonCerrar = new Button("  Cerrar  ");
        buttonCerrar.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                CRUDSeccion.getCRUD_Seccion().restarthBoxCRUD();
            }
        });
        gridPane.add(buttonCerrar, 2, 9);
        gridPane.getStyleClass().add("gridPane");
        gridPane.setMinSize(400, 400);
        
        return gridPane;
    }
}