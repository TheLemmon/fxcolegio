/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Alumno;
import org.luisgarcia.connection.SQLDatabaseConnection;
/**
 *
 * @author Fernando
 */
public class ControllerAlumno {
     
private static final ControllerAlumno CONTROLLER_Alumno = 
            new ControllerAlumno();
    private ArrayList<Alumno> arrayList;

    private ControllerAlumno() {
        arrayList = new ArrayList<>();
    }

    public static ControllerAlumno getCONTROLLER_Alumno() {
        return CONTROLLER_Alumno;
    }
     
    //Create
    
        public void add(int idSeccion, String nombre, String direccion, String telefono, String carnet) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("INSERT INTO Alumno(idSeccion, nombre, carnet, direccion, telefono) "
                + "VALUES(" + idSeccion + ", '" + nombre  +"', '" + carnet + "', '" + direccion + "', '" + telefono + "');");
    }
    
    //Read
    public ArrayList<Alumno> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Alumno");
        try {
            
            while (resultSet.next()) {
                Alumno alumno = new Alumno();
                alumno.setIdAlumno(resultSet.getInt("idAlumno"));
                alumno.setSeccion(ControllerSeccion.getCONTROLLER_Seccion().getSeccion(resultSet.getInt("idSeccion")));
                alumno.setNombre(resultSet.getString("nombre"));
                alumno.setDireccion(resultSet.getString("direccion"));
                alumno.setTelefono(resultSet.getString("telefono"));
                alumno.setCarnet(resultSet.getString("carnet"));
                arrayList.add(alumno);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    //Update
    
    public void modify(int idAlumno, int seccion, String nombre, String carnet, String direccion, String telefono ) {
        String query;
            query = "EXECUTE SP_ActualizarAlumno " + idAlumno + ", " + seccion + ", '" + nombre + "'"
                    + ", '" + carnet +"', '" + direccion + "', '" + telefono +"';";
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete
    
    public void delete(int idAlumno) {
        String query = "DELETE Alumno WHERE idAlumno = " + idAlumno;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }

    //Search
    public boolean buscarExistente(String nombre, String carnet){
        boolean verificar = false;
        for(Alumno alumno : arrayList){
            if(alumno.getNombre().equals(nombre) || alumno.getCarnet().equalsIgnoreCase(carnet)){
                verificar = true;
            }
        }
        return verificar;
    }
    
    public Alumno buscar(int idAlumno){    
        for(Alumno alumno : arrayList){
            if(alumno.getIdAlumno() == idAlumno){
                return alumno;
            }
        }
        return null;
    }
    
    public ArrayList<Alumno> buscar(String nombre){
        ArrayList<Alumno> resultado = new ArrayList<>(); 
        for(Alumno alumno1 : getArrayList()){
             if(alumno1.getCarnet().toUpperCase().contains(nombre.toUpperCase()) 
                     || alumno1.getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     alumno1.getSeccion().getCodigo().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(alumno1);
             }
         }
         return resultado;
    }
    
    public Alumno buscarUno(String nombre){
        for(Alumno alumno : arrayList){
            if(alumno.getNombre().equalsIgnoreCase(nombre)){
                return alumno;
            }
        }
        return null;
    }
}

