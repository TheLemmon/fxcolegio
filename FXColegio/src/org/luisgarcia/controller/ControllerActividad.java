/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Actividad;
import org.luisgarcia.bean.Alumno;
import org.luisgarcia.connection.SQLDatabaseConnection;

/**
 *
 * @author Fernando
 */
public class ControllerActividad {
    private static final ControllerActividad CONTROLLER_Actividad = 
            new ControllerActividad();
    private ArrayList<Actividad> arrayList;

    private ControllerActividad() {
        arrayList = new ArrayList<>();
    }
    public static ControllerActividad getCONTROLLER_Actividad() {
        return CONTROLLER_Actividad;
    }
    
        //Create
    
    public void add(int idActividades, int idBimestre, String descripcion, int valorNeto) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("EXECUTE SP_InsertarActividad"
                + " " + idActividades + ", " + idBimestre + ", '" + descripcion + "', " + valorNeto + " ;");
        
    }
    
    ///  READ
    public ArrayList<Actividad> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Actividad");
        try {
            while (resultSet.next()) {
                Actividad actividades = new Actividad();
                actividades.setIdActividad(resultSet.getInt("idActividad"));
                actividades.setAsignacion(ControllerAsignacion.getCONTROLLER_AsignarMateria().buscarUno(resultSet.getInt("idAsignacion")));
                actividades.setBimestre(ControllerBimestre.getCONTROLLER_Bimestre().getBimestre(resultSet.getInt("idBimestre")));
                actividades.setDescripcion(resultSet.getString("descripcion"));
                actividades.setValorNeto(resultSet.getInt("valorNeto"));
                arrayList.add(actividades);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividad.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
        
    ///////////// update
        
    public void modify(int idActividad, int idAsignacion, int idBimestre, String descripcion, int valorNeto) {
        String query;
            query = "EXECUTE SP_ActualizarActividad " + idActividad + ","
                    + " " + idBimestre + ", " + idAsignacion + ", '" + descripcion + "'"
                    + ", " + valorNeto +";";
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
     //Delete
    
    public void delete(int idActividad) {
        String query = "DELETE Actividad WHERE idActividad = " + idActividad;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
        //Search
    public boolean buscarExistente(String nombre, int idAsignacion, int nota){
        boolean verificar = false;
        for(Actividad actividades : arrayList){
            if(actividades.getDescripcion().equals(nombre)  &&
                    actividades.getAsignacion().getIdAsignacion() == idAsignacion
                    && actividades.getValorNeto() == nota){
                verificar = true;
            }
        }
        return verificar;
    }
    
    public Actividad buscar(int idActividades){
        for(Actividad actividades : arrayList){
            if(actividades.getIdActividad() == idActividades){
                return actividades;
            }
        }
        return null;
    }
    
    public ArrayList<Actividad> buscar(String nombre){
        ArrayList<Actividad> resultado = new ArrayList<>(); 
        for(Actividad actividades1 : getArrayList()){
             if(actividades1.getAsignacion().getMateria().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     actividades1.getAsignacion().getProfesor().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     actividades1.getAsignacion().getSalon().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     actividades1.getAsignacion().getSeccion().getCodigo().toUpperCase().contains(nombre.toUpperCase()) ||
                     actividades1.getDescripcion().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(actividades1);
             }
         }
        return resultado;
    }
    
    public ArrayList<Actividad> searchOne(int idSeccion, int idProfesor, int idMateria, int idBimestre){
        ArrayList<Actividad> resultado = new ArrayList<>();
        
        for(Actividad actividad : getArrayList()){
            if(actividad.getAsignacion().getMateria().getIdMateria() == idMateria &&
                    actividad.getAsignacion().getSeccion().getIdSeccion() == idSeccion &&
                    actividad.getAsignacion().getProfesor().getIdProfesor() == idProfesor &&
                    actividad.getBimestre().getIdBimestre() == idBimestre){
                resultado.add(actividad);
            }
        }
        
        return resultado;
    }
    
    public int getValorNeto(int idAsignacion, int nombre) {
        int valor = 0;
        int max = 100;
        int resultado = 0;
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().
                query("SELECT SUM(valorNeto) AS 'Valor' FROM Actividad WHERE idAsignacion = " + idAsignacion + " AND idBimestre = " + nombre + ";");
        try {
            while (resultSet.next()) {
                valor = resultSet.getInt("Valor");
                if((max - valor) > 0){
                    resultado = max - valor;
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividad.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return resultado;
    }
    
    public int getValorNetoDos(int idAsignacion, int valorActual) {
        int valor = 0;
        int max = 100;
        int resultado = 0;
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().
                query("SELECT SUM(valorNeto) AS 'Valor' FROM Actividad WHERE idAsignacion = " + idAsignacion + ";");
        try {
            while (resultSet.next()) {
                valor = resultSet.getInt("Valor");
                if((max - valor) >= 0){
                    resultado = max - valor + valorActual;
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividad.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return resultado;
    }
    public int getInt(int idSeccion, int idMateria, int idProfesor){  
        int id = 0;
        for(Actividad actividad : getArrayList()){
            if(actividad.getAsignacion().getMateria().getIdMateria() == idMateria &&
                    actividad.getAsignacion().getSeccion().getIdSeccion() == idSeccion &&
                    actividad.getAsignacion().getProfesor().getIdProfesor() == idProfesor){
                id = actividad.getIdActividad();
            }
        }
        
        return id;
    }
    
 
}
