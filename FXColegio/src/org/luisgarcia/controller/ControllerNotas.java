/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Notas;
import org.luisgarcia.connection.SQLDatabaseConnection;
/**
 *
 * @author Fernando
 */
public class ControllerNotas {
     private static final ControllerNotas CONTROLLER_Notas = 
            new ControllerNotas();
     private ArrayList<Notas> arrayList;

    private ControllerNotas() {
        
    }
    public static ControllerNotas getCONTROLLER_Notas() {
        return CONTROLLER_Notas;
    }
    
    
    ///  READ
    
    public void add(int idAlumno, int idMateria){
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("EXECUTE SP_InsertarNotas"
                + " " + idAlumno + ", " + idMateria + " ;");
    }
    
         
    public ArrayList<Notas> getArrayList() {
        arrayList = new ArrayList<>();
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Notas");
        try {
            while (resultSet.next()) {
                Notas notas = new Notas();
                notas.setControlNotas(resultSet.getInt("idNotas"));
                notas.setAlumno(ControllerAlumno.getCONTROLLER_Alumno().buscar(resultSet.getInt("idAlumno")));
                notas.setMateria(ControllerMateria.getCONTROLLER_Materia().getMateria(resultSet.getInt("idMateria")));
                notas.setBimestreI(resultSet.getInt("bimestre1"));
                notas.setBimestreII(resultSet.getInt("bimestre2"));
                notas.setBimestreIII(resultSet.getInt("bimestre3"));
                notas.setBimestreIV(resultSet.getInt("bimestre4"));
                arrayList.add(notas);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerNotas.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    public void modify(int idNotas, int idAlumno, int idMateria, int nota, int nota2, int nota3, int nota4) {
        String query;
            query = "EXECUTE SP_ActualizarNotas " + idNotas + ", " + idAlumno + ", " + idMateria + ""
                    + ", " + nota +", " + nota2 + ", " + nota3 + ", " + nota4 + ";";
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
}
