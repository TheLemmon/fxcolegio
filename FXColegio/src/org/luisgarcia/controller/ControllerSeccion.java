/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Seccion;
import org.luisgarcia.connection.SQLDatabaseConnection;
import org.luisgarcia.util.Archivadora;

/**
 *
 * @author Claudio Canel
 */
public class ControllerSeccion {
    private static final ControllerSeccion CONTROLLER_Seccion = 
            new ControllerSeccion();
    private ArrayList<Seccion> arrayList;

    private ControllerSeccion() {
        arrayList = new ArrayList<>();
    }

    public static ControllerSeccion getCONTROLLER_Seccion() {
        return CONTROLLER_Seccion;
    }
    
    //Create
    public void add(String grado, String secion, String jornada, String codigo) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("INSERT INTO Seccion(grado, nombre, jornada, codigo) VALUES('" + grado + "',"
                + " '" + secion + "', '" + jornada + "', '" + codigo + "');");
        Archivadora.getInstancia().escribir("Agregar Grado" + "\t" + "Gado = " + grado + 
             " Seccion = " + secion + " Jornada = " + jornada + " Codigo = " + codigo, "2015029.obj");
    }
    //Read
    public ArrayList<Seccion> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Seccion");
        try {
            
            while (resultSet.next()) {
                Seccion grado = new Seccion();
                grado.setIdSeccion(resultSet.getInt("idSeccion"));
                grado.setGrado(resultSet.getString("grado"));
                grado.setSeccion(resultSet.getString("nombre"));
                grado.setJornada(resultSet.getString("jornada"));
                grado.setCodigo(resultSet.getString("codigo"));
                arrayList.add(grado);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerSeccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
       //Update
    
    public void modify(int idSeccion, String grado, String seccion, String jornada, String codigo ) {
        String query;
            query = "EXECUTE SP_ActualizarGrado " + idSeccion + ", 'admi" + grado + "'"
                    + ", '" + seccion +"', '" + jornada + "', '" + codigo +"';";
            Archivadora.getInstancia().escribir("Modificar Grado" + "\t" + " ID = " + idSeccion 
                    + "Gado = " + grado +  " Seccion = " + seccion + " Jornada = " + jornada + " Codigo = " + codigo,
                    "2015029.obj");
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete
     public void delete(int idSeccion) {
        String query = "DELETE Seccion WHERE idSeccion = " + idSeccion;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }

     ///search
     public Seccion getSeccion(int idSeccion){
        for(Seccion buscar : arrayList){
            if(idSeccion == buscar.getIdSeccion()){
                return buscar;
            }
        }
        return null;
    }
    
    public Seccion getSeccion(String grado, String nombre, String jornada, String codigo){
        for(Seccion buscar : arrayList){
            if(buscar.getGrado().equalsIgnoreCase(grado) && buscar.getSeccion().equalsIgnoreCase(nombre)
                    && buscar.getJornada().equalsIgnoreCase(jornada) && buscar.getCodigo().equalsIgnoreCase(codigo)){
                return buscar;
            }
        }
        return null;
    }
    
    public ArrayList<Seccion> buscar(String nombre){
        ArrayList<Seccion> resultado = new ArrayList<>(); 
        for(Seccion seccion1 : getArrayList()){
             if(seccion1.getCodigo().toUpperCase().contains(nombre.toUpperCase()) 
                     || seccion1.getGrado().toUpperCase().contains(nombre.toUpperCase())
                    || seccion1.getJornada().toUpperCase().contains(nombre.toUpperCase()) 
                     || seccion1.getSeccion().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(seccion1);
             }
         }
         return resultado;
    }   
    
}
