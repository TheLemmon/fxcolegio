/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Actividad;
import org.luisgarcia.bean.ActividadAlumno;
import org.luisgarcia.connection.SQLDatabaseConnection;
/**
 *
 * @author Fernando
 */
public class ControllerActividadAlumno {
    private static final ControllerActividadAlumno CONTROLLER_ActividadAlumno = 
            new ControllerActividadAlumno();
    private ArrayList<ActividadAlumno> arrayList;

    private ControllerActividadAlumno() {
        arrayList = new ArrayList<>();
    }
    public static ControllerActividadAlumno getCONTROLLER_ActividadAlumno() {
        return CONTROLLER_ActividadAlumno;
    }
    
        //Create
    
    public void add(int idActividad, int idAlumno, double nota, double valorNeto) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("EXECUTE SP_InsertarActividadAlumno"
                + " " + idActividad + ", " + idAlumno + ", " + valorNeto + ", " + nota + " ;");
    }
    ///  READ
    public ArrayList<ActividadAlumno> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM ActividadAlumno");
        try {
            while (resultSet.next()) {
                ActividadAlumno actividadAlumno = new ActividadAlumno();
                actividadAlumno.setIdActividadAlumno(resultSet.getInt("idActividadAlumno"));
                actividadAlumno.setActividad(ControllerActividad.getCONTROLLER_Actividad().buscar(resultSet.getInt("idActividad")));
                actividadAlumno.setAlumno(ControllerAlumno.getCONTROLLER_Alumno().buscar(resultSet.getInt("idAlumno")));
                actividadAlumno.setNota(resultSet.getInt("nota"));
                actividadAlumno.setValorNeto(resultSet.getInt("valorNeto"));
                arrayList.add(actividadAlumno);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    
    public void modify(int idActividadAlumno, int idActividad, int idAlumno, int valorNeto, int nota) {
        String query;
            query = "EXECUTE SP_ActualizarActividadAlumno " + idActividadAlumno + ", " + idActividad + ", " + idAlumno + ""
                    + ", " + valorNeto +", " + nota + ";";
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete
    public void delete(int idActividadAlumno) {
        String query = "DELETE ActividadAlumno WHERE idActividadAlumno = " + idActividadAlumno;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    public boolean buscarExistente(int idAlumno, int idActividad){
        boolean verificar = false;
        for(ActividadAlumno actividades : arrayList){
            if(actividades.getAlumno().getIdAlumno() == idAlumno && 
                    actividades.getActividad().getIdActividad() == idActividad){
                verificar = true;
            }
        }
        return verificar;
    }
    
    public ArrayList<ActividadAlumno> buscar(ArrayList<Actividad> actividad, int idAlumno){
        ArrayList<ActividadAlumno> resultado = new ArrayList<>(); 
        for(ActividadAlumno actividades1 : getArrayList()){
            for(Actividad actividads : actividad){
                if(actividades1.getAlumno().getIdAlumno() == idAlumno && 
                    actividades1.getActividad().getIdActividad() == actividads.getIdActividad()){
                 resultado.add(actividades1);
            }
            
            }
        }
        return resultado;
    }

    public int buscar(int idActividad){
        int idActividadALumno = 0;
        for(ActividadAlumno actividades1 : getArrayList()){
            if(actividades1.getActividad().getIdActividad() == idActividad){
                idActividadALumno = actividades1.getIdActividadAlumno();           
            }
        }
        return idActividadALumno;
    }     
}
