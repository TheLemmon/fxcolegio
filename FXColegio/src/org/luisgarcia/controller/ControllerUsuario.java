/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Usuario;
import org.luisgarcia.connection.SQLDatabaseConnection;

/**
 *
 * @author Claudio Canel
 */
public class ControllerUsuario {
    private static final ControllerUsuario CONTROLLER_Usuario = 
            new ControllerUsuario();
    private ArrayList<Usuario> arrayList;
        private Boolean authenticate = Boolean.FALSE;
            private Usuario authenticateUsuario;

    private ControllerUsuario() {
        arrayList = new ArrayList<>();
    }

    public static ControllerUsuario getCONTROLLER_Usuario() {
        return CONTROLLER_Usuario;
    }
    
    //SEARCH
        public boolean buscar(String nombre){
            boolean comprobar = false;
            for(Usuario usuarios : arrayList){
                if(nombre.equalsIgnoreCase(usuarios.getNombre())){
                    comprobar = true;
                }
            }
            return comprobar;
        }
        
       public Boolean authenticate(String nombre, String clave) {
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Usuario WHERE nombre = '" + nombre + "' AND contrasenia = '" + clave + "';");
        authenticate = Boolean.FALSE;
        if(nombre.equals("admin") && clave.equals("admin")){
            authenticate = Boolean.TRUE;
        } else {
                    try {
            while (resultSet.next()) {
                Usuario usuario = new Usuario();
                usuario.setIdUsuario(resultSet.getInt("idUsuario"));
                usuario.setNombre(resultSet.getString("nombre"));
                usuario.setContrasenia(resultSet.getString("contrasenia"));
                this.authenticateUsuario = usuario;
                authenticate = Boolean.TRUE;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ControllerUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        }

        return authenticate;
    }
    
    public void deauthenticate() {
        this.authenticate = Boolean.FALSE;
        this.authenticateUsuario = null;
    }

    public Usuario getAuthenticateUsuario() {
        return authenticateUsuario;
    }

    public Boolean getAuthenticate() {
        
        return authenticate;
    }
    
    //Create
    
        public void add(String nombre, String contrasenia) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("SP_InsertarUsuario '" + nombre + "', '" + contrasenia + "';");
    }
    
    //Read
    public ArrayList<Usuario> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Usuario");
        try {
            
            while (resultSet.next()) {
                Usuario dia = new Usuario();
                dia.setIdUsuario(resultSet.getInt("idUsuario"));
                dia.setNombre(resultSet.getString("nombre"));
                dia.setContrasenia(resultSet.getString("contrasenia"));
                arrayList.add(dia);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    //Update
    
    public void modify(int idUsuario, String nombre, String contrasenia) {
        String query;
            query = "EXECUTE SP_ActualizarUsuario " + idUsuario + ", '" + nombre + "', '" + contrasenia + "';";

        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete
     public void delete(int idUsuario) {
        String query = "DELETE Usuario WHERE idUsuario = " + idUsuario;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
         
    public ArrayList<Usuario> getBuscar(String nombre){
        ArrayList<Usuario> resultado = new ArrayList<>(); 
        for(Usuario usuario1 : getArrayList()){
             if(usuario1.getNombre().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(usuario1);
             }
         }
         return resultado;
     }
    
}