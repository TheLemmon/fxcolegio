/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Salon;
import org.luisgarcia.connection.SQLDatabaseConnection;

/**
 *
 * @author Claudio Canel
 */
public class ControllerSalon {
    private static final ControllerSalon CONTROLLER_Salon = 
            new ControllerSalon();
    private ArrayList<Salon> arrayList;

    private ControllerSalon() {
        arrayList = new ArrayList<>();
    }

    public static ControllerSalon getCONTROLLER_Salon() {
        return CONTROLLER_Salon;
    }
    
    //Create
    
        public void add(String nombre) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("SP_InsertarSalon '" + nombre + "';");
    }
    
    //Read
    public ArrayList<Salon> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Salon");
        try {
            
            while (resultSet.next()) {
                Salon dia = new Salon();
                dia.setIdSalon(resultSet.getInt("idSalon"));
                dia.setNombre(resultSet.getString("nombre"));
                arrayList.add(dia);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerSalon.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    //Update
    
    public void modify(int idSalon, String nombre) {
        String query;
            query = "EXECUTE SP_ActualizarSalon " + idSalon + ", '" + nombre + "';";

        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete
     public void delete(int idSalon) {
        String query = "DELETE Salon WHERE idSalon = " + idSalon;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
     //Buscar
     public Salon buscar(int idSalon){
         Salon salon = null;
         for(Salon salon1 : arrayList){
             if(salon1.getIdSalon() == idSalon){
                 return salon1;
             }
         }
         return salon;
     }
    public Salon buscarUno(String nombre){
         Salon salon = null;
         for(Salon salon1 : arrayList){
             if(salon1.getNombre().equalsIgnoreCase(nombre)){
                 return salon1;
             }
         }
         return salon;
     }
     
    public ArrayList<Salon> buscar(String nombre){
        ArrayList<Salon> resultado = new ArrayList<>(); 
        for(Salon salon1 : getArrayList()){
             if(salon1.getNombre().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(salon1);
             }
         }
         return resultado;
    }
}