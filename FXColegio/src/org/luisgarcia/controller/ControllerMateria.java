/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Materia;
import org.luisgarcia.connection.SQLDatabaseConnection;

/**
 *
 * @author Luis Garcia
 */
public class ControllerMateria {
    private static final ControllerMateria CONTROLLER_Materia = 
            new ControllerMateria();
    private ArrayList<Materia> arrayList;

    private ControllerMateria() {
        arrayList = new ArrayList<>();
    }

    public static ControllerMateria getCONTROLLER_Materia() {
        return CONTROLLER_Materia;
    }
    
    //Create
    
        public void add(String nombre) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("SP_InsertarMateria '" + nombre + "';");
    }
    
    //Read
    public ArrayList<Materia> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Materia");
        try {
            
            while (resultSet.next()) {
                Materia dia = new Materia();
                dia.setIdMateria(resultSet.getInt("idMateria"));
                dia.setNombre(resultSet.getString("nombre"));
                arrayList.add(dia);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerMateria.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    //Update
    
    public void modify(int idMateria, String nombre) {
        String query;
            query = "EXECUTE SP_ActualizarMateria " + idMateria + ", '" + nombre + "';";

        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete
     public void delete(int idMateria) {
        String query = "DELETE Materia WHERE idMateria = " + idMateria;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
   public Materia getMateria(int idMateria){
       for(Materia materia : arrayList){
           if(idMateria == materia.getIdMateria()){
               return materia;
           }
       }
       return null;
   }
   
    public Materia getMateria(String nombre){
       for(Materia materia : arrayList){
           if(materia.getNombre().equalsIgnoreCase(nombre)){
               return materia;
           }
       }
       return null;
   }
    public ArrayList<Materia> buscar(String nombre){
        ArrayList<Materia> resultado = new ArrayList<>(); 
        for(Materia materia1 : getArrayList()){
             if(materia1.getNombre().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(materia1);
             }
         }
         return resultado;
    }
           
}