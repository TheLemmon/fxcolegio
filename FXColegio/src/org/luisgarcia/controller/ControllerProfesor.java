/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Profesor;
import org.luisgarcia.connection.SQLDatabaseConnection;

/**
 *
 * @author Claudio Canel
 */
public class ControllerProfesor {
    private static final ControllerProfesor CONTROLLER_Profesor = 
            new ControllerProfesor();
    private ArrayList<Profesor> arrayList;

    private ControllerProfesor() {
        arrayList = new ArrayList<>();
    }

    public static ControllerProfesor getCONTROLLER_Profesor() {
        return CONTROLLER_Profesor;
    }
    
    //Create
    
    public void add(String dpi, String nombre, String direccion, String telefono, String correo) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("SP_InsertarProfesor '" + dpi + "', '" + nombre + "', "
                + "'" + direccion + "', '" + telefono + "', '" + correo + "';");
    }
    
    //Read
    public ArrayList<Profesor> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Profesor");
        try {
            
            while (resultSet.next()) {
                Profesor profesor = new Profesor();
                profesor.setIdProfesor(resultSet.getInt("idProfesor"));
                profesor.setDpi(resultSet.getString("dpi"));
                profesor.setNombre(resultSet.getString("nombre"));
                profesor.setTelefono(resultSet.getString("telefono"));
                profesor.setDireccion(resultSet.getString("direccion"));
                profesor.setCorreo(resultSet.getString("correo"));
                arrayList.add(profesor);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerProfesor.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    //Update
    
    public void modify(int idProfesor, String dpi, String nombre, String telefono, String direccion, String correo) {
        String query;
            query = "EXECUTE SP_ActualizarProfesor " + idProfesor + ", '" + dpi + "', "
                    + "'" + nombre + "', '" + direccion + "', '" + telefono + "', "
                    + "'" + correo + "';";

        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete
    public void delete(int idProfesor) {
        String query = "DELETE Profesor WHERE idProfesor = " + idProfesor;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
        
    /// search
    public Profesor getProfesor(int idProfesor){
        for(Profesor profesor : arrayList){
            if(idProfesor == profesor.getIdProfesor()){
                return profesor;
            }
        }
        return null;
    }
    
    public boolean getProfesor(String nombre){
        boolean verificar = false;
        for(Profesor profesor : arrayList){
            if(profesor.getNombre().equalsIgnoreCase(nombre)){
                verificar = true;
            }
        }
        return verificar;
    }
     
    public ArrayList<Profesor> buscar(String nombre){
        ArrayList<Profesor> resultado = new ArrayList<>(); 
        for(Profesor seccion1 : getArrayList()){
             if(seccion1.getCorreo().toUpperCase().contains(nombre.toUpperCase())
                     || seccion1.getDpi().toUpperCase().contains(nombre.toUpperCase())
                    || seccion1.getNombre().toUpperCase().contains(nombre.toUpperCase()) 
                     || seccion1.getTelefono().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(seccion1);
             }
         }
         return resultado;
    } 
}
