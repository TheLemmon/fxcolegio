/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Bimestre;
import org.luisgarcia.connection.SQLDatabaseConnection;

/**
 *
 * @author Luis Garcia
 */
public class ControllerBimestre {
    private static final ControllerBimestre CONTROLLER_Bimestre = 
            new ControllerBimestre();
    private ArrayList<Bimestre> arrayList;

    private ControllerBimestre() {
        arrayList = new ArrayList<>();
    }

    public static ControllerBimestre getCONTROLLER_Bimestre() {
        return CONTROLLER_Bimestre;
    }
    
    //Create
    
        public void add(String nombre) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("SP_InsertarBimestre '" + nombre + "';");
    }
    
    //Read
    public ArrayList<Bimestre> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Bimestre");
        try {
            
            while (resultSet.next()) {
                Bimestre dia = new Bimestre();
                dia.setIdBimestre(resultSet.getInt("idBimestre"));
                dia.setNombre(resultSet.getString("nombre"));
                arrayList.add(dia);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerBimestre.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    //Update
    
    public void modify(int idBimestre, String nombre) {
        String query;
            query = "EXECUTE SP_ActualizarBimestre " + idBimestre + ", '" + nombre + "';";

        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete
     public void delete(int idBimestre) {
        String query = "DELETE Bimestre WHERE idBimestre = " + idBimestre;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
   public Bimestre getBimestre(int idBimestre){
       for(Bimestre materia : getArrayList()){
           if(idBimestre == materia.getIdBimestre()){
               return materia;
           }
       }
       return null;
   }
   
    public Bimestre getBimestre(String nombre){
       for(Bimestre materia : getArrayList()){
           if(materia.getNombre().equalsIgnoreCase(nombre)){
               return materia;
           }
       }
       return null;
   }
    public ArrayList<Bimestre> buscar(String nombre){
        ArrayList<Bimestre> resultado = new ArrayList<>(); 
        for(Bimestre materia1 : getArrayList()){
             if(materia1.getNombre().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(materia1);
             }
         }
         return resultado;
    }
           
}
