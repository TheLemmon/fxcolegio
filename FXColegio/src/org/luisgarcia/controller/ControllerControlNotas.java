/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Notas;
import org.luisgarcia.connection.SQLDatabaseConnection;
/**
 *
 * @author Fernando
 */
public class ControllerControlNotas {
     
private static final ControllerControlNotas CONTROLLER_ControlNotas = 
            new ControllerControlNotas();
    private ArrayList<Notas> arrayList;

    private ControllerControlNotas() {
        arrayList = new ArrayList<>();
    }

    public static ControllerControlNotas getCONTROLLER_ControlNotas() {
        return CONTROLLER_ControlNotas;
    }
    
    //Create
    
    //Read
    public ArrayList<Notas> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM ControlNotas");
        try {
            
            while (resultSet.next()) {
                Notas asignarMateria = new Notas();
                asignarMateria.setControlNotas(resultSet.getInt("idControlNotas"));
                asignarMateria.setIdAlumno(resultSet.getInt("idAlumno"));
                asignarMateria.setBimestreI(resultSet.getInt("bimestreI"));
                asignarMateria.setIdMateria(resultSet.getInt("idMateria"));
                asignarMateria.setBimestreII(resultSet.getInt("bimestreII"));
                asignarMateria.setBimestreIII(resultSet.getInt("bimestreIII"));
                asignarMateria.setBimestreIV(resultSet.getInt("bimestreIV"));
                asignarMateria.setBimestreV(resultSet.getInt("bimestreV"));
                asignarMateria.setExamenFinal(resultSet.getInt("examenFinal"));
                asignarMateria.setPromedio(resultSet.getInt("Promedio"));

                arrayList.add(asignarMateria);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerControlNotas.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    //Update
    
    //Delete

    
    
}



