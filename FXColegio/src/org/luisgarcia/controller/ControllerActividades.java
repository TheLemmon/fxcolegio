/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Actividades;
import org.luisgarcia.connection.SQLDatabaseConnection;

/**
 *
 * @author Fernando
 */
public class ControllerActividades {
    private static final ControllerActividades CONTROLLER_Actividades = 
            new ControllerActividades();
    private ArrayList<Actividades> arrayList;

    private ControllerActividades() {
        arrayList = new ArrayList<>();
    }
    public static ControllerActividades getCONTROLLER_Actividades() {
        return CONTROLLER_Actividades;
    }
    
        //Create
    
    public void add(int idActividades, String descripcion, double valorNeto) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery("EXECUTE SP_InsertarActividad"
                + " " + idActividades + ", '" + descripcion + "', " + valorNeto + " ;");
    }
    
    ///  READ
    public ArrayList<Actividades> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION().query("SELECT * FROM Actividad");
        try {
            while (resultSet.next()) {
                Actividades actividades = new Actividades();
                actividades.setIdActividaes(resultSet.getInt("idActividad"));
                actividades.setAsignacion(ControllerAsignacion.getCONTROLLER_AsignarMateria().buscarUno(resultSet.getInt("idAsignacion")));
                actividades.setDescripcion(resultSet.getString("descripcion"));
                actividades.setValorNeto(resultSet.getDouble("valorNeto"));
                arrayList.add(actividades);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerActividades.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
        
    ///////////// update
        
    public void modify(int idActividad, int idAsignacion, String descripcion, double valorNeto) {
        String query;
            query = "EXECUTE SP_ActualizarActividad " + idActividad + ", " + idAsignacion + ", '" + descripcion + "'"
                    + ", " + valorNeto +";";
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
     //Delete
    
    public void delete(int idActividad) {
        String query = "DELETE Actividad WHERE idActividad = " + idActividad;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
        //Search
    public boolean buscarExistente(String nombre, int idAsignacion){
        boolean verificar = false;
        for(Actividades actividades : arrayList){
            if(actividades.getDescripcion().equals(nombre)  &&
                    actividades.getAsignacion().getIdAsignacion() == idAsignacion){
                verificar = true;
            }
        }
        return verificar;
    }
    
    public Actividades buscar(int idActividades){
        for(Actividades actividades : arrayList){
            if(actividades.getIdActividaes() == idActividades){
                return actividades;
            }
        }
        return null;
    }
    
    public ArrayList<Actividades> buscar(String nombre){
        ArrayList<Actividades> resultado = new ArrayList<>(); 
        for(Actividades actividades1 : getArrayList()){
             if(actividades1.getAsignacion().getMateria().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     actividades1.getAsignacion().getProfesor().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     actividades1.getAsignacion().getSalon().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     actividades1.getAsignacion().getSeccion().getCodigo().toUpperCase().contains(nombre.toUpperCase()) ||
                     actividades1.getDescripcion().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(actividades1);
             }
         }
        return resultado;
    }
}
