/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.controller;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.luisgarcia.bean.Asignacion;
import org.luisgarcia.bean.Materia;
import org.luisgarcia.bean.Profesor;
import org.luisgarcia.connection.SQLDatabaseConnection;
/**
 *
 * @author Fernando
 */
public class ControllerAsignacion {
     
private static final ControllerAsignacion CONTROLLER_AsignarMateria = 
            new ControllerAsignacion();
    private ArrayList<Asignacion> arrayList;
    private ArrayList<Materia> arrayListMateria = new ArrayList<>();
    private ArrayList<Asignacion> arrayListAsignacion = new ArrayList<>();

    private ControllerAsignacion() {
        arrayList = new ArrayList<>();
    }

    public static ControllerAsignacion getCONTROLLER_AsignarMateria() {
        return CONTROLLER_AsignarMateria;
    }
    
    //Create
    
    public void add(int idSeccion, int idMateria, int idProfesor, int idSalon) {
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(
                "SP_InsertarAsignacion " + idSeccion + ", " + idMateria + ", "
                + "" + idProfesor + ", " + idSalon + ";");
    }
    
    //Read
    public ArrayList<Asignacion> getArrayList() {
        arrayList.clear();
        ResultSet resultSet = SQLDatabaseConnection.getSQLDATABASECONNECTION()
                .query("SELECT * FROM Asignacion");
        try {
            
            while (resultSet.next()) {
                Asignacion asignacion = new Asignacion();
                asignacion.setIdAsignacion(resultSet.getInt("idAsignacion"));
                asignacion.setSeccion(ControllerSeccion.getCONTROLLER_Seccion()
                        .getSeccion(resultSet.getInt("idSeccion")));
                asignacion.setMateria(ControllerMateria.getCONTROLLER_Materia()
                        .getMateria(resultSet.getInt("idMateria")));
                asignacion.setProfesor(ControllerProfesor.getCONTROLLER_Profesor()
                        .getProfesor(resultSet.getInt("idProfesor")));
                asignacion.setSalon(ControllerSalon.getCONTROLLER_Salon().buscar
                        (resultSet.getInt("idSalon")));
                arrayList.add(asignacion);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(ControllerAsignacion.class.getName()).log(Level.SEVERE, null, ex);
        }
        SQLDatabaseConnection.getSQLDATABASECONNECTION().disconnect();
        return arrayList;
    }
    
    //Update
    
    public void modify(int idAsignacion, int idSeccion, int idMateria, int idProfesor, int idSalon) {
       String query;
        query = "SP_ActualizarAsignacion " + idAsignacion + ", " + idSeccion + ", " + idMateria + ", "
                + "" + idProfesor + ", " + idSalon + ";";
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    //Delete

    public void delete(int idAsignacion) {
        String query = "DELETE Asignacion WHERE idAsignacion = " + idAsignacion;
        SQLDatabaseConnection.getSQLDATABASECONNECTION().executeQuery(query);
    }
    
    public ArrayList<Asignacion> buscar(String nombre){
        ArrayList<Asignacion> resultado = new ArrayList<>(); 
        for(Asignacion asignacion1 : getArrayList()){
             if(asignacion1.getMateria().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     asignacion1.getProfesor().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     asignacion1.getSalon().getNombre().toUpperCase().contains(nombre.toUpperCase()) ||
                     asignacion1.getSeccion().getCodigo().toUpperCase().contains(nombre.toUpperCase())){
                 resultado.add(asignacion1);
             }
        }
        return resultado;
    }
    public boolean buscar(String materia, String profesor, String salon, String seccion){
        boolean verificar = false;
        for(Asignacion asignacion : arrayList){
            if(asignacion.getMateria().getNombre().equalsIgnoreCase(materia) && 
               asignacion.getProfesor().getNombre().equalsIgnoreCase(profesor) &&
               asignacion.getSalon().getNombre().equalsIgnoreCase(salon) && 
               asignacion.getSeccion().getCodigo().equalsIgnoreCase(seccion)){
               verificar = true;
            }
        }
        return verificar;
    }
    public Asignacion buscarUno(int idAsignacion){
        for(Asignacion asignacion : getArrayList()){
            if(asignacion.getIdAsignacion() == idAsignacion){
                return asignacion;
            }
        }
        return null;
    }
    
    public Asignacion searchOne(String seccion, String materia, String profesor){
        for(Asignacion asignacion : getArrayList()){
            if(asignacion.getSeccion().getCodigo().equalsIgnoreCase(seccion) && 
                    asignacion.getMateria().getNombre().equalsIgnoreCase(materia) &&
                    asignacion.getProfesor().getNombre().equalsIgnoreCase(profesor)){
                return asignacion;
            }
        }
        return null;
    }
    
    public ArrayList<Materia> getArrayListMateria(String codigo){
       arrayListMateria.clear();
        for(Asignacion asignacion : getArrayList()){
            if(asignacion.getSeccion().getCodigo().equalsIgnoreCase(codigo)){
                arrayListMateria.add(asignacion.getMateria());
                
            }
        }
        return arrayListMateria;
    }
    
    public ArrayList<Profesor> getArrayListProfesor(String codigo, String nombre){
        ArrayList<Profesor> arrayListProfesor = new ArrayList<>();
        for(Asignacion asignacion : getArrayList()){
            if(asignacion.getMateria().getNombre().equalsIgnoreCase(nombre) && 
                    asignacion.getSeccion().getCodigo().equalsIgnoreCase(codigo)){
                arrayListProfesor.add(asignacion.getProfesor());
                
            }
        }
        return arrayListProfesor;
    }
}


