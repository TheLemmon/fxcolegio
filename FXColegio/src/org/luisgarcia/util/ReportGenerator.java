/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.util;

import java.io.InputStream;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.luisgarcia.connection.SQLDatabaseConnection;

/**
 *
 * @author Claudio Canel
 */
public class ReportGenerator {
    private static final ReportGenerator REPORT_GENERATOR =
            new ReportGenerator();

    private ReportGenerator() {
    }

    public static ReportGenerator getREPORT_GENERATOR() {
        return REPORT_GENERATOR;
    }
    
    public void generate(Map parameters, String reportFile, String title) {
        try {
            InputStream reporte = getClass().getResourceAsStream("/org/luisgarcia/resource/" + reportFile);
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reporte);
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, SQLDatabaseConnection.getSQLDATABASECONNECTION().getConnection());
            JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
            jasperViewer.setTitle(title);
            jasperViewer.setVisible(true);
        } catch (JRException ex) {
            Logger.getLogger(ReportGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
