package org.luisgarcia.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Archivadora {
	private static final Archivadora INSTANCIA = 
		new Archivadora();
		
	private File file;
	private FileWriter fileWriter;
	private PrintWriter printWriter;
	private InputStream inputStream;
	private InputStreamReader inputStreamReader;
	private BufferedReader bufferedReader;
	private String fecha;
		
	private Archivadora() {
	}
	
	public static Archivadora getInstancia() {
		return INSTANCIA;
	}
	
	public void escribir(String contenido, String nombre) {
		try {
			file = new File(nombre);
			fecha = formatFecha();
			fileWriter = new FileWriter(file, true);
			printWriter = new PrintWriter(fileWriter, true);
			printWriter.println(contenido + "\t\t" + fecha);
			printWriter.close();
		} catch (IOException ex) {
			
		}
	}
	
	public BufferedReader leer(String nombre) {
		try {
			file = new File(nombre);
			inputStream = new FileInputStream(file);
			bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		} catch (IOException ex) {
			
		}
		return bufferedReader;
	}
	
	private String formatFecha() {
		 SimpleDateFormat formateador = new SimpleDateFormat(
		"dd'/'MM'/'yyyy  hh:mm:ss a");
		Date fechaDate = new Date();
		String fecha = formateador.format(fechaDate);
		return fecha;
   } 
	
}