/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.bean;

/**
 *
 * @author Fernando
 */
public class ControlNotas {
    private int controlNotas;
    private int idAlumno;
    private int idMateria;
    private double bimestreI;
    private double bimestreII;
    private double bimestreIII;
    private double bimestreIV;
    private double bimestreV;
    private double examenFinal;
    private double promedio;

    public ControlNotas() {
    }

    public int getControlNotas() {
        return controlNotas;
    }

    public void setControlNotas(int controlNotas) {
        this.controlNotas = controlNotas;
    }
    

    public int getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(int idAlumno) {
        this.idAlumno = idAlumno;
    }

    public int getIdMateria() {
        return idMateria;
    }

    public void setIdMateria(int idMateria) {
        this.idMateria = idMateria;
    }

    public double getBimestreI() {
        return bimestreI;
    }

    public void setBimestreI(double bimestreI) {
        this.bimestreI = bimestreI;
    }

    public double getBimestreII() {
        return bimestreII;
    }

    public void setBimestreII(double bimestreII) {
        this.bimestreII = bimestreII;
    }

    public double getBimestreIII() {
        return bimestreIII;
    }

    public void setBimestreIII(double bimestreIII) {
        this.bimestreIII = bimestreIII;
    }

    public double getBimestreIV() {
        return bimestreIV;
    }

    public void setBimestreIV(double bimestreIV) {
        this.bimestreIV = bimestreIV;
    }

    public double getBimestreV() {
        return bimestreV;
    }

    public void setBimestreV(double bimestreV) {
        this.bimestreV = bimestreV;
    }

    public double getExamenFinal() {
        return examenFinal;
    }

    public void setExamenFinal(double examenFinal) {
        this.examenFinal = examenFinal;
    }

    public double getPromedio() {
        return promedio;
    }

    public void setPromedio(double promedio) {
        this.promedio = promedio;
    }
    
    
}
