/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.bean;

/**
 *
 * @author Fernando
 */
public class Notas {
    private int controlNotas;
    private Alumno alumno;
    private Materia materia;
    private int bimestreI;
    private int bimestreII;
    private int bimestreIII;
    private int bimestreIV;

    public Notas() {
    }

    public int getControlNotas() {
        return controlNotas;
    }

    public void setControlNotas(int controlNotas) {
        this.controlNotas = controlNotas;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public Materia getMateria() {
        return materia;
    }

    public void setMateria(Materia materia) {
        this.materia = materia;
    }
    
    

    public int getBimestreI() {
        return bimestreI;
    }

    public void setBimestreI(int bimestreI) {
        this.bimestreI = bimestreI;
    }

    public int getBimestreII() {
        return bimestreII;
    }

    public void setBimestreII(int bimestreII) {
        this.bimestreII = bimestreII;
    }

    public int getBimestreIII() {
        return bimestreIII;
    }

    public void setBimestreIII(int bimestreIII) {
        this.bimestreIII = bimestreIII;
    }

    public int getBimestreIV() {
        return bimestreIV;
    }

    public void setBimestreIV(int bimestreIV) {
        this.bimestreIV = bimestreIV;
    }

    
    
}
