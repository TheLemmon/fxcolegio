/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.bean;

/**
 *
 * @author Fernando
 */
public class ActividadAlumno {
    private int idActividadAlumno;
    private Actividad actividad;
    private Alumno alumno;
    private int valorNeto;
    private int nota;

    public ActividadAlumno() {
    }

    public int getIdActividadAlumno() {
        return idActividadAlumno;
    }

    public void setIdActividadAlumno(int idActividadAlumno) {
        this.idActividadAlumno = idActividadAlumno;
    }

    public Actividad getActividad() {
        return actividad;
    }

    public void setActividad(Actividad actividad) {
        this.actividad = actividad;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }
    
    public int getNota() {
        return nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }
    
    public int getValorNeto() {
        return valorNeto;
    }
        
    public void setValorNeto(int valorNeto) {
        this.valorNeto = valorNeto;
    }

}