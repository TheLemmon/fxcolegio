/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.luisgarcia.bean;

/**
 *
 * @author Fernando
 */
public class Actividades {
    private int idActividaes;
    private Asignacion asignacion;
    private String descripcion;
    private Double valorNeto;

    public Actividades() {
    }

    public int getIdActividaes() {
        return idActividaes;
    }

    public void setIdActividaes(int idActividaes) {
        this.idActividaes = idActividaes;
    }

    public Asignacion getAsignacion() {
        return asignacion;
    }

    public void setAsignacion(Asignacion asignacion) {
        this.asignacion = asignacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getValorNeto() {
        return valorNeto;
    }

    public void setValorNeto(Double valorNeto) {
        this.valorNeto = valorNeto;
    }
    
    @Override
    public String toString(){
        return this.descripcion + ", " + asignacion.getMateria().getNombre();
    }
}
